/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

#define _SCL_SECURE_NO_WARNINGS

// namespace header
#include "XPIWITMainObject.h"

// qt header
#include <QtCore/QCoreApplication>
#include <QtCore/QTimer>

// system header
#include <iostream>
#include <string>

// the namespaces used
using namespace XPIWIT;

// the main function
int main(int argc, char *argv[])
{
    std::cout << "Starting XPIWIT" << std::endl;

    // create the application
    QCoreApplication app(argc, argv);

    // create the XPIWIT main object
    XPIWITMainObject *xpMainObject = new XPIWITMainObject( &app );

    // handle event loop stuff //

    // finish the event loop when done
    QObject::connect(xpMainObject, SIGNAL( finished() ), &app, SLOT( quit() ) );

    // run xpiwit
    QTimer::singleShot( 0, xpMainObject, SLOT( run() ) );

    // close application
    int returnCode = app.exec();
    bool failureOccurred = xpMainObject->HasFailureOccured();

    if( failureOccurred || returnCode != 0 )
	{
        std::cout << " - ERROR OCCURRED - " << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << " - SUCCESS - " << std::endl;
    return EXIT_SUCCESS;
}
