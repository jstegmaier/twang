/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

#ifndef XPIWITMAINOBJECT_H
#define XPIWITMAINOBJECT_H

// namespace header
#include "../CMD/CMDArguments.h"
#include "../XML/AbstractPipeline.h"

// qt header
#include <QtCore/QObject>

class CMDPipelineArguments;


namespace XPIWIT
{
/**
 *	@class XPIWITMainObject
 *	Main class of the XPIWIT Core project
 */
class XPIWITMainObject : public QObject
{
    Q_OBJECT
    public:

        XPIWITMainObject( QObject *parent = 0 );

        ~XPIWITMainObject();

        void Init();

        void ProcessInputData();

		void ParseXmlPipeline();

		void WriteFilterList();

        void ProcessPipeline();

		void WriteGlobalLogFile( QString id, QString timeDelta, CMDPipelineArguments *);
		void WriteCombinedMetaData();

        void Finalize();

        // return true for failure
        bool HasFailureOccured() { return !mNoFailureOccurred; }

    signals:
        void finished();

    public slots:
        void run();

    private:
        bool mProcessPipeline;
		bool mWriteFilterList;
		bool mNoFailureOccurred;


        CMDArguments *mCmdArguments;
        AbstractPipeline abstractPipeline;
};

} // namespace XPIWIT

#endif  // XPIWITMAINOBJECT_H
