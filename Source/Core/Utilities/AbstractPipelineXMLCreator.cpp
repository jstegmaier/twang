/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// file header
#include "AbstractPipelineXMLCreator.h"


// project header
#include "../XML/AbstractFilter.h"
#include "../XML/AbstractInput.h"
#include "../XML/AbstractPipeline.h"
#include "../XML/AbstractPipelineTools.h"


// qt header
#include <QXmlStreamWriter>
#include <QFile>
#include <QBuffer>


namespace XPIWIT
{

// the default constructor
AbstractPipelineXMLCreator::AbstractPipelineXMLCreator()
{
    mWriteDescription = true;
	mAbstractPipeline = NULL;
}


// function to get the XML
QByteArray AbstractPipelineXMLCreator::GetXML()
{
    QBuffer xmlBuffer;
	CreateXMLFromAbstractPipeline( &xmlBuffer, mAbstractPipeline );

    return xmlBuffer.buffer();
}


// function to write the xml
void AbstractPipelineXMLCreator::WriteXML( QString file )
{
    QFile xmlFile(file);

    if( xmlFile.open(QIODevice::WriteOnly) )
	{
		CreateXMLFromAbstractPipeline( &xmlFile, mAbstractPipeline );
        xmlFile.close();
    }
	else
	{
        // error
    }
}


// function to create the whole filter list of all available filters
void AbstractPipelineXMLCreator::CreateXMLFromAbstractPipeline(QIODevice* device, AbstractPipeline* abstractPipeline)
{
	QHash< QString, AbstractFilter* >* pipelineItemSet = abstractPipeline->GetPipelineItemSet();

	QList<QString> abstractFilterKeys = pipelineItemSet->keys();
	
    QXmlStreamWriter xmlWriter;
    xmlWriter.setAutoFormatting( true );
    xmlWriter.setDevice( device );
    xmlWriter.writeStartDocument();

	xmlWriter.writeStartElement("xpiwit");
    xmlWriter.writeStartElement("pipeline");

	int readerId = 0;

    // every filter
    //int k = 0;
    QString itemId;
	foreach(QString currentKey, abstractFilterKeys )
	{
		AbstractFilter* currentFilter = pipelineItemSet->value(currentKey);

		int filterId = QString(currentFilter->GetId()[currentFilter->GetId().length()-1]).toInt();

		itemId.sprintf("%04d", filterId);

		
		
       // processObjectSettings = i_ProcessObjectBase->GetProcessObjectSettings();
        //processObjectType = i_ProcessObjectBase->GetType();

        xmlWriter.writeStartElement("item");
        xmlWriter.writeAttribute( "item_id", "item_" + itemId );

        xmlWriter.writeStartElement("name");
		QString name = currentFilter->GetName();
        xmlWriter.writeCharacters( name );
        xmlWriter.writeEndElement();

		bool isReader = false;
		if (name == QString("ImageReader"))
		{
			isReader = true;
		}

        if( mWriteDescription )
		{
            xmlWriter.writeStartElement("description");
            xmlWriter.writeCharacters(currentFilter->GetDesription());
            xmlWriter.writeEndElement();
        }
		
        //inputs
        xmlWriter.writeStartElement("input");
        for(int i = 0; i < currentFilter->GetNumberImageIn(); i++)
		{
            xmlWriter.writeEmptyElement( "image" );

			if (isReader == true)
			{
				xmlWriter.writeAttribute( "item_id_ref", "cmd" );
				xmlWriter.writeAttribute( "number_of_output", QString().number(readerId) );
				readerId++;
			}
			else
			{
				itemId.sprintf("item_%04d", currentFilter->GetImageInputs()[i].mIdRef.toInt() );
				xmlWriter.writeAttribute( "item_id_ref", itemId );
				xmlWriter.writeAttribute( "number_of_output", QString::number(currentFilter->GetImageInputs()[i].mNumberRef ) );
				xmlWriter.writeAttribute( "type_number", QString::number(currentFilter->GetImageInputs()[i].mDataType) );
			}
        }
        for(int i = 0; i < currentFilter->GetNumberMetaIn(); i++)
		{
            xmlWriter.writeEmptyElement( "meta" );
			itemId.sprintf("item_%04d", currentFilter->GetMetaInputs()[i].mIdRef.toInt() );
			xmlWriter.writeAttribute( "item_id_ref", itemId );
			xmlWriter.writeAttribute( "number_of_output", QString::number(currentFilter->GetMetaInputs()[i].mNumberRef ) );
			xmlWriter.writeAttribute( "type_number", QString::number(currentFilter->GetMetaInputs()[i].mDataType) );
        }
        xmlWriter.writeEndElement();    // close inputs

		/*
        //outputs
        xmlWriter.writeStartElement( "output" );
        xmlWriter.writeAttribute( "number_images", QString::number( processObjectType->GetNumberImageOutputs() ) );
        xmlWriter.writeAttribute( "number_meta", QString::number( processObjectType->GetNumberMetaOutputs() ) );
        for(int i = 0; i < processObjectType->GetNumberImageOutputs(); i++)
		{
            xmlWriter.writeEmptyElement( "image" );
            xmlWriter.writeAttribute( "number", QString::number( i + 1 ) );
			xmlWriter.writeAttribute( "type_number", QString::number( processObjectType->GetImageOutputTypes().at(i) ));
        }
        for(int i = 0; i < processObjectType->GetNumberMetaOutputs(); i++)
		{
            xmlWriter.writeEmptyElement("meta");
            xmlWriter.writeAttribute( "number", QString::number( i + 1 ) );
			xmlWriter.writeAttribute( "type_number", processObjectType->GetMetaOutputTypes().at(i) );
        }

        xmlWriter.writeEndElement();
		*/

        // arguments
        xmlWriter.writeStartElement( "arguments" );
		for(int i = 0; i < currentFilter->GetNumberParameter(); i++)
		{
            xmlWriter.writeEmptyElement( "parameter" );
			xmlWriter.writeAttribute( "key", currentFilter->GetParameter(i).at(0) );
			xmlWriter.writeAttribute( "value", currentFilter->GetParameter(i).at(1) );
            //if( mWriteDescription )
			//	xmlWriter.writeAttribute( "description", currentFilter->GetParameterDescriptions()[i] );
        }
        xmlWriter.writeEndElement(); // close arguments

        xmlWriter.writeEndElement(); // close item
		
    }

    xmlWriter.writeEndElement();
	xmlWriter.writeEndElement();
    xmlWriter.writeEndDocument();
}

} //namespace XPIWIT
