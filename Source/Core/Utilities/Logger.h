/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

#ifndef LOGGER_H
#define LOGGER_H

// qt header
#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QFile>
#include <sstream>


namespace XPIWIT
{

class Logger : public QObject
{
    public:
        // singleton methods
        static Logger *GetInstance( QObject* parent = 0 );
        static void DeleteInstance();

        // logger methods //

        // init for use of file and console
        void InitLogFile( bool fileLogging, QString rootDirectory, QString fileName );
        void SetNewLogFile( QString rootDirectory, QString fileName );
        void ReleaseLogFile();
        void SetUseSubfolder( bool useSubfolder ) { m_UseSubfolder = useSubfolder; }

        // writer for default log
        void Write( QString );
        void WriteLine( QString );
        void WriteException( std::exception err );
        void WriteSpacer();

        // special writer methods
        void WriteToConsole( QString );
        void WriteToAll( QString );

        // global log writer
        void InitGlobalLog( QString rootDirectory, QString fileName, QString separator = ";" );
        void WriteGlobalLine( QStringList , bool hasImageOutput , QStringList );

    private:
        static Logger* m_Instance;

        Logger( QObject* parent = 0 );
        ~Logger();

        QString m_StdLogPath;
        QString m_GlobalLogPath;

        std::streambuf *coutbuf;
        std::streambuf *cerrbuf;
        std::streambuf *filebuf;

        std::ostream coutStream;

        QString m_CSVSeparator;
        QFile m_GlobalLogFile;

        bool m_UseSubfolder;
        bool m_LogFileExists;
        bool m_GlobalLogExists;
        bool m_GlobalLogHeaderExists;

        void WriteGlobalLogHeader( bool hasImageOutput , QStringList metaFiles );
};

} // Namespace

#endif // LOGGER_H
