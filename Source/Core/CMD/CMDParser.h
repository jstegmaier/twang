/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

#ifndef CMDPARSER_H
#define CMDPARSER_H

// namespace header
#include "CMDArguments.h"

// qt header
#include <QtCore/QList>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QRegExp>


namespace XPIWIT
{

/**
 * @class CMDParser
 * @brief The CMDParser class processes the data from the cmd input.
 *        It can handle command line argumends as well as piped arguments.
 */
class CMDParser
{
public:
    CMDParser();
    ~CMDParser();

    CMDArguments *InitArguments();

    bool mWriteFilterList;
	bool mProcessPipeline;

private:
    CMDArguments *mCmdArguments;

    QStringList mEnableKey;
    QStringList mDisableKey;
    QStringList mSubFolderKeys;
    QStringList mOutputFormatKeys;

    QStringList PipedArguments();
    void ProcessArguments( QStringList arguments );

    QStringList ProcessParameter( QStringList arguments, QString argument );
    QStringList ProcessValues( QStringList arguments, int index );
    void ProcessIO( QStringList arguments, QString argument);
    bool ProcessFlagParameter( QStringList arguments , bool defaultFlag = true );

};

} //namespace XPIWIT

#endif // CMDPARSER_H

