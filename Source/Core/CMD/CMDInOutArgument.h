/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

#ifndef CMDINOUTARGUMENT_H
#define CMDINOUTARGUMENT_H

// qt header
#include <QtCore/QString>
#include <QtCore/QStringList>


namespace XPIWIT
{

class CMDInOutArgument
{
public:
    CMDInOutArgument();
    void InitInput( QStringList );
    void InitOutput( QStringList );

    QString mInputId;       // number to map the id
    QString mPath;          // path of the input
    QStringList mImages;    // path of the images
    QString mPrefix;        // prefix for the output
    QString mDimension;     // dimension of the image

    bool mIsInput;
    bool mIsOutput;
    void SetIsInput()   { mIsInput = true; mIsOutput = false; }
    void SetIsOutput()  { mIsInput = false; mIsOutput = true; }

    bool mIsImage;
    bool mIsCsv;
    void SetIsImage()   { mIsImage = true; mIsCsv = false; }
    void SetIsCsv()     { mIsImage = false; mIsCsv = true; }

    bool mIsDefault;
    void SetIsDefault( bool isDefault = true ) { mIsDefault = isDefault; }

    bool mIsSingleFile;
    bool mIsDir;
    bool mIsList;
    void SetIsSingleFile()  { mIsSingleFile = true; mIsDir = false; mIsList = false; }
    void SetIsDir()         { mIsSingleFile = false; mIsDir = true; mIsList = false; }
    void SetIsList()        { mIsSingleFile = false; mIsDir = false; mIsList = true; }

private:
    QStringList mInputStringList;

    void ProcessPath();
    void ProcessList();
    void ProcessDir();
    void ProcessImage();
};

} //namespace XPIWIT

#endif // CMDINOUTARGUMENT_H

