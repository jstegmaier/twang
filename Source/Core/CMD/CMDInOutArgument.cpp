/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// file header
#include "CMDInOutArgument.h"

// qt header
#include <QtCore/QFile>
#include <QtCore/QDir>


namespace XPIWIT
{

// the default constructor
CMDInOutArgument::CMDInOutArgument()
{
    mIsInput = false;
    mIsOutput = false;
    mIsImage = false;
    mIsCsv = false;
    mIsDefault = false;
    mIsSingleFile = false;
    mIsDir = false;
    mIsList = false;
}


// function to init the input
void CMDInOutArgument::InitInput( QStringList inputLine )
{
    mInputStringList = inputLine;

    // format: id, path, dimension/csv
    if( inputLine.length() < 3 )
        return;

	// check default marker
    mInputId = mInputStringList.at( 0 );
    if( mInputId.endsWith("*") )
	{
        SetIsDefault();
        mInputId = mInputId.replace( QString("*"), QString("") );
    }

    mPath = mInputStringList.at( 1 );
    mDimension = mInputStringList.at( 2 );

    SetIsInput();
    if( mDimension.compare( "csv", Qt::CaseInsensitive ) == 0 )
        SetIsCsv();
    else
        SetIsImage();

    ProcessPath();
    SetIsInput();
}


// function to init the output
void CMDInOutArgument::InitOutput( QStringList inputLine )
{
    mInputStringList = inputLine;

    // format: path, prefix
    if( inputLine.length() != 1 && inputLine.length() != 2 )
        return;

    mPath = mInputStringList.at( 0 );
	mPrefix = "";
	if( inputLine.length() == 2 )
		mPrefix = mInputStringList.at( 1 );

    // if ends with txt -> List
    if( mPath.endsWith(".txt") )
	{
        ProcessList();
        return;
    }

    if( mPath.endsWith("/") )
	{
        SetIsDir();
    }

    SetIsOutput();
}


// function to process either a path or a file
void CMDInOutArgument::ProcessPath()
{
    // if ends with txt -> List
    if( mPath.endsWith(".txt") )
	{
        ProcessList();
        return;
    }

    // if ends with / -> Dir
    if( mPath.endsWith("/") )
	{
        ProcessDir();
        return;
    }

    // otherwise file
    ProcessImage();
}


// process a whole list of images
void CMDInOutArgument::ProcessList()
{
    SetIsList();

    // open file
    QFile listFile( mPath );
    if( !listFile.open( QIODevice::ReadOnly | QIODevice::Text ) )
        return;

    int lineLength = 0;
    while( lineLength >= 0 )
	{
        char buf[1024];
        lineLength = listFile.readLine( buf, sizeof(buf) );
        if (lineLength != -1)
		{
            mImages.append( QString( buf ).replace("\n", "") );
        }
    }

    listFile.close();
}


// process a complete directory
void CMDInOutArgument::ProcessDir()
{
    SetIsDir();

    QDir inputDir( mPath );
    QStringList allFiles = inputDir.entryList( QDir::Files | QDir::Readable |
                                               QDir::NoDotAndDotDot | QDir::NoSymLinks );
    QStringList clearedFiles;
    if( mIsImage )
	{
        foreach (const QString &str, allFiles)
		{
            // exclude common file extensions that are not images
            if ( str.contains(".txt")	|| str.contains(".csv")  ||
                 str.contains(".m")		|| str.contains(".prjz") ||
                 str.contains(".log")	|| str.contains(".xml")  )
                continue;
            clearedFiles += str;
        }
    }

    if( mIsCsv )
	{
        foreach (const QString &str, allFiles)
		{
            // only csv files
            if ( str.contains(".csv") )
                clearedFiles += str;
        }
    }


    for( int i = 0; i < clearedFiles.length(); i++ )
	{
        mImages.append( mPath + clearedFiles.at( i ) );
    }
}


// function to process the image
void CMDInOutArgument::ProcessImage()
{
    SetIsSingleFile();
    mImages.append( mPath );
}

} //namespace XPIWIT
