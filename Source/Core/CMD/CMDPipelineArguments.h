/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

#ifndef CMDPIPELINEARGUMENTS_H
#define CMDPIPELINEARGUMENTS_H

// qt header
#include <QtCore/QList>
#include <QtCore/QString>
#include <QtCore/QStringList>


namespace XPIWIT
{

/**
 * @class CMDPipelineArguments
 * @brief The CMDPipelineArguments class represents all releveant data from the cmd for a pipeline
 */
class CMDPipelineArguments
{
public:
    CMDPipelineArguments()
	{
        mHasImageOutput = false;
        mDefaultIndex = 0;
        mMetaId = 0;
    }

    int mMetaId;

    QStringList mInputMetaId;       // number of the meta input
    QStringList mInputMeta;         // full path of the meta input

    int mDefaultIndex;				// define the default input for write result image name
    QStringList mInputImageId;      // number of the input image
    QStringList mInputImagePaths;   // full path of the input images
    QStringList mInputDimension;    // dimension of the input image

    QString mStdOutputPath;         // out path for all outputs
    QString mPrefix;

    bool mHasImageOutput;           // list mode
    QString mImageOutputPath;       // list mode full image path and name

    QString mXmlPath;               // full path of the xml
    int mSeed;                      // seed
    bool mLockFileEnabled;			// flag to set lockfile
	bool mSkipProcessingIfOutputExists;// if set, processing will be skipped if the output already exits 

    bool mUseSubFolder;				// write result to subfolder
    QStringList mSubFolderFormat;	// format of the foldername

    bool mFileLoggingEnabled;

    QStringList mOutputFileFormat;	// format of the filenames

    // Meta Data Handling
    bool mUseMetaDataHeader;
    QString mMetaDataSeparator;
    QString mMetaDataDelimitor;

private:
};

} //namespace XPIWIT

#endif // CMDPIPELINEARGUMENTS_H

