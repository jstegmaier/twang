/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

#ifndef CMDARGUMENTS_H
#define CMDARGUMENTS_H

// namespace header
#include "CMDInOutArgument.h"
#include "CMDPipelineArguments.h"

// qt header
#include <QtCore/QList>
#include <QtCore/QString>
#include <QtCore/QStringList>


namespace XPIWIT
{

/**
 * @class CMDArguments
 * @brief The CMDArguments class represents a cmd/piped input as it was delivered.
 */
class CMDArguments
{
public:
    CMDArguments();

    bool mWriteFilterList;
    QString mFilterListPath;

    QString mXmlPath;

    bool mLockFileEnabled;
    bool mFileLoggingEnabled;
    bool mUseSubFolder;

    int mSeed;

    QStringList mSubFolderFormat;
    QStringList mOutputFileFormat;

    // input
    QList< CMDInOutArgument *> mInputArguments;

    // output
    CMDInOutArgument *mOutputArgument;
	bool mSkipProcessingIfOutputExists;

    // meta data
    bool mUseMetaDataHeader;
    QString mMetaDataSeparator;
    QString mMetaDataDelimitor;


    int mNumberOfImages;
    bool CheckInputs();

    CMDPipelineArguments *GetPipelineArguments( int );
    int GetNumberOfImages();

    void WriteToLog();
    QString BoolToString( bool );

	void CreateInputFolder();

private:

};

} //namespace XPIWIT

#endif // CMDARGUMENTS_H

