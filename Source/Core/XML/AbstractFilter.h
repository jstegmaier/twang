/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

#ifndef ABSTRACTFILTER_H
#define ABSTRACTFILTER_H

// namespace header
#include "AbstractInput.h"
#include "../CMD/CMDPipelineArguments.h"

// qt header
#include <QtCore/QPair>
#include <QtCore/QList>
#include <QtCore/QString>
#include <QVector>


namespace XPIWIT
{

class AbstractFilter
{
public:
    AbstractFilter() { mIsReader = false; }
    
    AbstractFilter(AbstractFilter& filter)
    {
        SetAbstractFilter(filter);
    }
    
    void SetAbstractFilter(AbstractFilter& filter)
    {
        mId = filter.GetId();                              // id of the filter from xml
        mName = filter.GetName();                                      // name of the filter
        mDescription = filter.GetDesription();								// description of filter
        mIsReader = filter.IsReader();                                     // is true for reader items with cmd_00 input
        mImageTypes = filter.GetTypes();							// list of types for templating
        mImageTypeIds = filter.GetTypeIds();							// list of types for templating
        mImageInputs = filter.GetImageInputs();               // image inputs
        mMetaInputs = filter.GetMetaInputs();                // meta inputs
        mParameter = filter.GetParameters();						// parameter of the filter
        mParameterDescriptions = filter.GetParameterDescriptions();				// description of the parameter at corresponding index
        mNumberImageIn = filter.GetNumberImageIn();
        mNumberImageOut = filter.GetNumberImageOut();
        mNumberMetaIn = filter.GetNumberMetaIn();
        mNumberMetaOut = filter.GetNumberMetaOut();
        
        mKeepOutput = filter.GetKeepOutput();
    }
    
    // ID
    void SetId( QString id ) { mId = id; }
    QString GetId() { return mId; }

    // Name
    void SetName( QString name ) {
        mName = name;
        if( name.toLower().compare("imagereader") == 0 ) mIsReader = true;
        if( name.toLower().compare("metareader") == 0 ) mIsReader = true;
    }

    QString GetName(){ return mName; }

    // is reader
    bool IsReader() { return mIsReader; }
    void SetIsReader( bool isReader ) { mIsReader = isReader; }

	// types
	//void SetType( int num, QString type ) { SetType( QPair< int, QString >(num, type) ); }
	void SetType( QString type ) { mImageTypes.append( type ); }
	QStringList GetTypes() { return mImageTypes; }
	void SetTypeId( int typeId ) { mImageTypeIds.append( typeId ); }
	QList< int > GetTypeIds() { return mImageTypeIds; }

    // Image Input
	void SetImageInput( int inputId, QString idRef, int numberRef, int dataType ) { mImageInputs[inputId].mIdRef = idRef; mImageInputs[inputId].mNumberRef = numberRef; mImageInputs[inputId].mDataType = dataType; }
    void SetImageInput( QString idRef, int numberRef, int dataType ) { SetImageInput( AbstractInput( idRef, numberRef, dataType ) ); }
	void SetImageInput( AbstractInput input ) { mImageInputs.append( input ); 	}
    QList< AbstractInput > GetImageInputs() { return mImageInputs; }


    // Meta Input
	void SetMetaInput( int inputId, QString idRef, int numberRef, int dataType ) { mMetaInputs[inputId].mIdRef = idRef; mMetaInputs[inputId].mNumberRef = numberRef; mMetaInputs[inputId].mDataType = dataType; }
	void SetMetaInput( QString idRef, int numberRef, int dataType ) {
        SetMetaInput( AbstractInput( idRef, numberRef, dataType ) ); }
    void SetMetaInput( AbstractInput input ) { mMetaInputs.append( input ); }
	QList< AbstractInput > GetMetaInputs() { return mMetaInputs; }

    const QList< QString > GetRequiredIds()
	{
        QList< QString > ids;
        foreach( AbstractInput input, mImageInputs ) {ids.append( input.mIdRef );}
        foreach( AbstractInput input, mMetaInputs ) {ids.append( input.mIdRef );}
        return ids;
    }

    // Parameter
    void SetParameter( QString key, QString value ) {
		QVector<QString> parameter; 
		parameter.append(key);  
		parameter.append(value);
		SetParameter(parameter); 
	}
    void SetParameter( QVector<QString> parameter)
    {
		mParameter.append(parameter); 
	}
    
	void SetParameter( int index, QString value )
    {
		mParameter[index][1] = value;
	}
    int GetNumberParameter() { return mParameter.length(); }
	QVector<QString> GetParameter( int number ) { return mParameter.at( number ); }
    QList< QVector<QString> > GetParameters() { return mParameter; }

    void SetKeepOutput( int num ) { mKeepOutput.append( num ); }
	int GetKeepOutput( int num ) { return mKeepOutput.count( num ); }
    QList<int> GetKeepOutput() { return mKeepOutput; }

	// Descriptions ATTENTION: Parameter descriptions don't automatically stay in sync with parameters! For now you have to manage that yourself!
	void SetDescription(QString des){mDescription = des;}
	QString GetDesription(){return mDescription;}
	void SetParDescriptions(QList<QString> des){mParameterDescriptions = des;}
	void AppendParDescription(QString des){ mParameterDescriptions.append(des);}
	QList<QString> GetParameterDescriptions(){return mParameterDescriptions;}

	// In/out numbers
	int GetNumberImageIn() {return mNumberImageIn;}
	int GetNumberImageOut() {return mNumberImageOut;}
	int GetNumberMetaIn() {return mNumberMetaIn;}
	int GetNumberMetaOut() {return mNumberMetaOut;}
	void SetNumberImageIn(int number) {mNumberImageIn = number;}
	void SetNumberImageOut(int number) {mNumberImageOut = number;}
	void SetNumberMetaIn(int number) {mNumberMetaIn = number;}
	void SetNumberMetaOut(int number) {mNumberMetaOut = number;}

//public slots:
	//void slotSetParameter(QString value, int idx){SetParameter(idx, value);}

private:
    QString mId;                                        // id of the filter from xml
    QString mName;                                      // name of the filter
	QString mDescription;								// description of filter
    bool mIsReader;                                     // is true for reader items with cmd_00 input
	QStringList mImageTypes;							// list of types for templating
	QList< int > mImageTypeIds;							// list of types for templating
    QList< AbstractInput > mImageInputs;               // image inputs
    QList< AbstractInput > mMetaInputs;                // meta inputs
    QList< QVector<QString> > mParameter;						// parameter of the filter
	QList<QString> mParameterDescriptions;				// description of the parameter at corresponding index
	int mNumberImageIn;
	int mNumberImageOut;
	int mNumberMetaIn;
	int mNumberMetaOut;

    QList< int > mKeepOutput;
};

} // namespace XPIWIT

#endif // ABSTRACTFILTER_H
