/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

#ifndef XMLREADER_H
#define XMLREADER_H

// namespace header
#include "AbstractPipeline.h"
#include "AbstractFilter.h"
#include "AbstractInput.h"

// qt header
#include <QtCore/QFile>
#include <QtCore/QXmlStreamReader>
#include <QtCore/QStringList>


namespace XPIWIT
{

/**
 * @class XMLReader
 * Reads the content of a XML file and creates an SimplifiedPipeline with all filter settings.
 */
class XMLReader
{
public:
    XMLReader();
	~XMLReader();

    void readXML( QString file );

    AbstractPipeline getAbstractPipeline() { return mAbstractPipeline; }
	QList<AbstractFilter*> getFilterList() { return mFilterList; }

private:
    QXmlStreamReader xml;
    AbstractPipeline mAbstractPipeline;
	QList<AbstractFilter*> mFilterList;

    // xml reader sub methods
    AbstractFilter* readItem();

    QString ErrorMessage( QString methodName = "" );

    QStringList mIdCheckList;
    bool CheckIdAndAddToList( QString );
    QString GetAttribute( QString, bool throwException = false );

};

} // namespace XPIWIT

#endif // XMLREADER_H
