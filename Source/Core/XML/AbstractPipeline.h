/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

#ifndef ABSTRACTPIPELINE_H
#define ABSTRACTPIPELINE_H

// namespace header
#include "../Utilities/Logger.h"
#include "AbstractFilter.h"

// qt header
#include <QtCore/QString>
#include <QtCore/QHash>
#include <QtCore/QList>


namespace XPIWIT 
{

/**
 * @class AbstractPipeline
 * Holds a list of AbstractFilter
 */
class AbstractPipeline
{
public:
    AbstractPipeline();

    void AddItem( AbstractFilter* inputItem );
	void RemoveItem( AbstractFilter* item );
	void RemoveItemById( int id );

    int GetFilterNumberById( QString filterId );
    int GetNumFilter();
    AbstractFilter* GetFilter( int num );
	//const AbstractFilter* GetFilterPointer( int num );

    QHash< QString, AbstractFilter* >* GetPipelineItemSet();
    void SetPipelineSequence( QList< QString > pipelineSequence );

    void Clear();
    void WriteToLog();

private:
    QHash< QString, AbstractFilter* >* mPipelineItemSet;
    QList< QString > mPipelineSequence;
};

} //namespace XPIWIT

#endif // ABSTRACTPIPELINE_H
