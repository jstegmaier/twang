/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

#ifndef XPIWITGUI

#ifndef XMLCREATOR_H
#define XMLCREATOR_H

// qt header
#include <QtCore/QByteArray>
#include <QtCore/QIODevice>


namespace XPIWIT
{

/**
 * @class XMLCreator
 * Produces a list of all available Filter and its default values
 */
class XMLCreator
{
public:
    /**
     * The constructor.
     */
    XMLCreator();

    /**
      * return the xml
      */
    QByteArray GetXML();

    /**
      * writes the xml
      */
    void WriteXML( QString file );

    /**
     * set or unset the write description flag
     */
    void SetWriteDescription( bool writeDescription ) { mWriteDescription = writeDescription; }

private:

    /**
     * create the XML Document
     */
    void CreateXMLFilterList( QIODevice *dev );

    /**
     * defines if description is written or not
     */
    bool mWriteDescription;

};

} //namespace XPIWIT

#endif // XMLCREATOR_H

#endif
