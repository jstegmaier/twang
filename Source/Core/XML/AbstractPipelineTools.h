/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

#ifndef ABSTRACTPIPELINETOOLS
#define ABSTRACTPIPELINETOOLS

// namespace header
#include "AbstractPipeline.h"

// qt header
#include <QtCore/QString>


namespace XPIWIT {

/**
 * @class AbstractPipelineTools
 * Sort function and validation of the abstract pipeline
 */
class AbstractPipelineTools
{
public:
    AbstractPipelineTools( AbstractPipeline *abstractPipeline );

    void SetAbstractPipeline( AbstractPipeline *abstractPipeline );
    AbstractPipeline *GetAbstractPipeline();

    void OrderPipeline();
    void SetReleaseFlags();

private:
    AbstractPipeline *mAbstractPipeline;
    QHash< QString, AbstractFilter* > *mPipelineItemSet;
    QList< QString > mPipelineSequence;

    void CreateListOfFilterIds( QList< QPair< QString, QStringList > > * );
    int FindFirstFilterWithAvailableInputs( QList< QPair< QString, QStringList > > * );
    bool IdsAvailable( QStringList );

    void SetKeepOuputFlags(QString filterId, int imageNum );

};

} //namespace XPIWIT

#endif // SIMPLIFIEDPIPELINE_H
