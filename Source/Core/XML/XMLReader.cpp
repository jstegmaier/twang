/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// file header
#include "XMLReader.h"


namespace XPIWIT
{
// example website: http://developer.nokia.com/Community/Wiki/QXmlStreamReader_to_parse_XML_in_Qt

// the default constructor
XMLReader::XMLReader()
{
//	mFilterList = new QList<AbstractFilter>;
}


// the default destructor
XMLReader::~XMLReader()
{
//	delete(mFilterList);
}


// function to read t he XML file
void XMLReader::readXML( QString file )
{
    bool pipelineTagFound = false;
	bool filterListTagFound = false;
    bool itemTagFound = false;

    // just in case
    xml.clear();
    mAbstractPipeline.Clear();

    CheckIdAndAddToList( "cmd" );   // id reserved for cmd inputs

    // open file
    QFile xmlFile(file);
    if( !xmlFile.open( QIODevice::ReadOnly | QIODevice::Text ) )
        throw QString("Error: can't open xml file! Filename: " + file);

    xml.setDevice( &xmlFile );

    bool readPipeline = false;
	bool readFilterList = false;
	// check if filter list
	if( file.endsWith("myfilters.xml")){ readFilterList = true;}

    QXmlStreamReader::TokenType token = xml.readNext();

    while ( !xml.atEnd() && !xml.hasError() )
	{
        // read token
        token = xml.readNext();

        // continue if header ( xml version and encoding )
        if(token == QXmlStreamReader::StartDocument)
		{
            continue;
        }
        
		// continue if comment
        if(token == QXmlStreamReader::Comment)
		{
            continue;
        }

        if(token == QXmlStreamReader::StartElement)
		{
			// Pipeline
            if( xml.name() == "pipeline" )
			{
                readPipeline = true;
                pipelineTagFound = true;
                continue;
            }

            if(readPipeline)
			{
                if( xml.name() == "item" )
				{
                    itemTagFound = true;
                    AbstractFilter* curItem = readItem();
                    mAbstractPipeline.AddItem( curItem );
                    continue;
                }
            }
			// Filter list
			if( xml.name() == "filterlist" && readFilterList)
			{
				filterListTagFound = true;
				continue;
			}
			if( xml.name() == "item" && filterListTagFound)
			{
				itemTagFound = true;
                AbstractFilter* curItem = readItem();
				mFilterList.append( curItem );
				continue;
			}
        }
    }

    if( (!pipelineTagFound || !itemTagFound) && !readFilterList )
        throw ErrorMessage("readXML -- No pipeline tag found or no items in xml.");    // throw error message
	
	if( readFilterList && !filterListTagFound)
		throw ErrorMessage("readXML -- could not read filter list \"myfilters.txt\".");		// throw error message

    if ( xml.hasError() )
        throw ErrorMessage("readXML");    // throw error message
}


// function to read a single item
AbstractFilter* XMLReader::readItem()
{
    AbstractFilter* curItem = new AbstractFilter();
	// flag because input and output have image sub tags
    bool readInput = false;

    while( !(xml.tokenType() == QXmlStreamReader::EndElement && xml.name() == "item") )
	{
        if ( xml.hasError() )
            throw ErrorMessage("readItem");    // throw error message

        if(xml.tokenType() == QXmlStreamReader::StartElement)
		{
            if( xml.name() == "item" )
			{
                QString curId = GetAttribute( "item_id", true );
                CheckIdAndAddToList( curId );
                curItem->SetId( curId );
            }

            if( xml.name() == "name" )
			{
                xml.readNext();
                QString curName = xml.text().toString();
                curItem->SetName( curName );
            }

			if (xml.name() == "description"){
				QString curDes = xml.readElementText();
				curItem->SetDescription( curDes );
			}

            if( xml.name() == "input" ){
                readInput = true;
				curItem->SetNumberImageIn(xml.attributes().value("number_images").toInt());
				curItem->SetNumberMetaIn(xml.attributes().value("number_meta").toInt());
            }

            if( xml.name() == "output" )
			{
                readInput = false;
				curItem->SetNumberImageOut(xml.attributes().value("number_images").toInt());
				curItem->SetNumberMetaOut(xml.attributes().value("number_meta").toInt());
            }
			
			if( xml.name() == "type" )
			{
                QString type = GetAttribute( "type_name" );
				curItem->SetType( type );
				int typeId = GetAttribute( "type_number" ).toInt();
				curItem->SetTypeId( typeId );
            }

            if( readInput )
			{
                if( xml.name() == "image" )
				{
                    QString curIdRef = GetAttribute( "item_id_ref" );
                    int curNumRef = GetAttribute( "number_of_output" ).toInt();
                    int curType = GetAttribute( "type_number" ).toInt();
                    curItem->SetImageInput( curIdRef, curNumRef, curType );
                }

                if( xml.name() == "meta" )
				{
                    QString curIdRef = GetAttribute( "item_id_ref" );
                    int curNumRef = GetAttribute( "number_of_output" ).toInt();
                    curItem->SetMetaInput( curIdRef, curNumRef, 0 );
                }
            }

            // in arguments
            if( xml.name() == "parameter" )
			{
                QString curKey = xml.attributes().value("key").toString();
                QString curVal = xml.attributes().value("value").toString();
				QString curDes = xml.attributes().value("description").toString();
                curItem->SetParameter( curKey, curVal );
				if( !curDes.isEmpty() ) {
					curItem->AppendParDescription(curDes);
				}
            }
        }
        xml.readNext();
    }

    return curItem;
}


// function to validate the existing ids
bool XMLReader::CheckIdAndAddToList( QString id )
{
    if( mIdCheckList.contains( id ) )
	{
        // Error: id is already in use
        throw QString( "Error: XMLReader: Id duplication of id: " + id );
        return false;
    }
	else
	{
        mIdCheckList.append( id );
        return true;
    }
}


// function to get an xml attribute
QString XMLReader::GetAttribute( QString name , bool throwException )
{
    if( xml.attributes().hasAttribute( name ) )
        return xml.attributes().value( name ).toString();

    if( throwException )
        throw ErrorMessage("readXML -- required attribute: " + name + " not set ");    // throw error message

    return QString("");
}


// function to write an error message
QString XMLReader::ErrorMessage( QString methodName )
{
    // create the error message
    return QString( "XMLReader::" + methodName + " Error in XML Processing " +
                    xml.errorString() +
                    "\nat Line: " + QString::number(xml.lineNumber()) );
}

} // namespace XPIWIT
