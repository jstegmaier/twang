/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// file header
#include "AbstractPipeline.h"

// namespace header
#include "AbstractInput.h"


namespace XPIWIT
{

// default constructor
AbstractPipeline::AbstractPipeline()
{
	mPipelineItemSet = NULL;
}


// add an item to the abstract pipeline
void AbstractPipeline::AddItem(AbstractFilter* inputItem)
{
	if( mPipelineItemSet == NULL )
		mPipelineItemSet = new QHash< QString, AbstractFilter* >;

    mPipelineItemSet->insert( inputItem->GetId(), inputItem );
	mPipelineSequence.append( inputItem->GetId() );
}

void AbstractPipeline::RemoveItem(AbstractFilter* item)
{
    delete item; // POTENTIAL CRASH LOCATION?
	mPipelineItemSet->remove(item->GetId());
	mPipelineSequence.removeAll(item->GetId());
}
void AbstractPipeline::RemoveItemById(int id)
{
    delete mPipelineItemSet->value(QString::number(id));
	mPipelineItemSet->remove(QString::number(id));	
}

int AbstractPipeline::GetFilterNumberById(QString filterId)
{
    return mPipelineSequence.indexOf( filterId );
}


// get the number of filters
int AbstractPipeline::GetNumFilter()
{
    return mPipelineSequence.length();
}


// write the pipeline structure to the log file
void AbstractPipeline::WriteToLog()
{
    Logger *log = Logger::GetInstance();
    log->WriteSpacer();
    log->WriteLine( "Pipeline sequence:" );
    for( int i = 0; i < mPipelineSequence.length(); i++ )
	{
        QString id = mPipelineSequence.at( i );
        AbstractFilter* curItem = mPipelineItemSet->value( id );
        log->WriteLine( "Item id: " + id );
        log->WriteLine( " - Filter name: " + curItem->GetName() );
    }
    log->WriteSpacer();
}


// get a filter by its number
AbstractFilter* AbstractPipeline::GetFilter( int num )
{
    if( mPipelineItemSet->contains( mPipelineSequence.at(num) ))
        return mPipelineItemSet->value( mPipelineSequence.at(num) );

    Logger::GetInstance()->WriteLine( "SimplifiedPipeline::GetFilter - filter id not found: " + mPipelineSequence.at(num) );
    return NULL;
}


// returns a pointer to a filter object
/*const AbstractFilter* AbstractPipeline::GetFilterPointer( int num )
{
	int myindex = mPipelineSequence.indexOf(QString::number(num));
    if( mPipelineItemSet->contains( mPipelineSequence.at(myindex) ))
        return &(mPipelineItemSet->value( mPipelineSequence.at(myindex) ));

    Logger::GetInstance()->WriteLine( "SimplifiedPipeline::GetFilter - filter id not found: " + mPipelineSequence.at(num) );
    return NULL;
}*/


// get the abstract pipeline
QHash<QString, AbstractFilter*> *AbstractPipeline::GetPipelineItemSet()
{
    return mPipelineItemSet;
}


// set the pipeline sequence
void AbstractPipeline::SetPipelineSequence(QList<QString> pipelineSequence)
{
    mPipelineSequence = pipelineSequence;
}


// clear the current pipeline
void AbstractPipeline::Clear()
{
	if( mPipelineItemSet != NULL )
		mPipelineItemSet->clear();
    mPipelineSequence.clear();
}

} //namespace XPIWIT
