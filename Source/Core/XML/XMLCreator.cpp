/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

#ifndef XPIWITGUI

// file header
#include "XMLCreator.h"


// project header
#include "../../Filter/Base/Management/ProcessObjectBase.h"
#include "../../Filter/Base/Management/ProcessObjectManager.h"
#include "../../Filter/Base/Management/ProcessObjectManager.h"
#include "../../Filter/Base/Management/ProcessObjectSettings.h"
#include "../../Filter/Base/Management/ProcessObjectType.h"

// qt header
#include <QXmlStreamWriter>
#include <QFile>
#include <QBuffer>


namespace XPIWIT
{

// the default constructor
XMLCreator::XMLCreator()
{
    mWriteDescription = true;
}


// function to get the XML
QByteArray XMLCreator::GetXML()
{
    QBuffer xmlBuffer;
    CreateXMLFilterList( &xmlBuffer );

    return xmlBuffer.buffer();
}


// function to write the xml
void XMLCreator::WriteXML( QString file )
{
    QFile xmlFile(file);

    if( xmlFile.open(QIODevice::WriteOnly) )
	{
        CreateXMLFilterList( &xmlFile );
        xmlFile.close();
    }
	else
	{
        // error
    }
}


// function to create the whole filter list of all available filters
void XMLCreator::CreateXMLFilterList(QIODevice *dev)
{
    QList<ProcessObjectBase *> processObjects = ProcessObjectManager::GetInstance()->GetProcessObjectList();			// List of all ProcessObjectWidgets
    ProcessObjectSettings *processObjectSettings;
    ProcessObjectType *processObjectType;

    QXmlStreamWriter xmlWriter;
    xmlWriter.setAutoFormatting( true );
    xmlWriter.setDevice( dev );
    xmlWriter.writeStartDocument();

    xmlWriter.writeStartElement("filterlist");

    // every filter
    int k = 0;
    QString itemId;
    foreach( ProcessObjectBase* i_ProcessObjectBase, processObjects )
	{
		if( i_ProcessObjectBase == NULL )
			continue;

        k++;
        itemId.sprintf("%04d", k);
        processObjectSettings = i_ProcessObjectBase->GetProcessObjectSettings();
        processObjectType = i_ProcessObjectBase->GetType();

        xmlWriter.writeStartElement("item");
        xmlWriter.writeAttribute( "item_id", "item" + itemId );

        xmlWriter.writeStartElement("name");
		QString name = i_ProcessObjectBase->GetName();
        xmlWriter.writeCharacters( name );
        xmlWriter.writeEndElement();

        if( mWriteDescription ){
            xmlWriter.writeStartElement("description");
            xmlWriter.writeCharacters(i_ProcessObjectBase->GetDescription());
            xmlWriter.writeEndElement();
        }

		// types
		xmlWriter.writeStartElement("image_types");
		for(int i = 0; i < processObjectType->GetNumberTypes(); i++)
		{
			xmlWriter.writeStartElement("type");
			xmlWriter.writeAttribute( "type_number", QString::number( i + 1 ) );
			xmlWriter.writeAttribute( "type_name", "float" );
			xmlWriter.writeAttribute( "description", "" );
			xmlWriter.writeEndElement();    
		}
		xmlWriter.writeEndElement();    

        //inputs
        xmlWriter.writeStartElement("input");
        xmlWriter.writeAttribute( "number_images", QString::number( processObjectType->GetNumberImageInputs() ) );
        xmlWriter.writeAttribute( "number_meta", QString::number( processObjectType->GetNumberMetaInputs() ) );
        for(int i = 0; i < processObjectType->GetNumberImageInputs(); i++)
		{
            xmlWriter.writeEmptyElement( "image" );
			xmlWriter.writeAttribute( "item_id_ref", "" );
			xmlWriter.writeAttribute( "number_of_output", QString::number( i + 1 ) );
			xmlWriter.writeAttribute( "type_number", QString::number( processObjectType->GetImageInputTypes().at(i) ));
        }
        for(int i = 0; i < processObjectType->GetNumberMetaInputs(); i++)
		{
            xmlWriter.writeEmptyElement( "meta" );
            xmlWriter.writeAttribute( "item_id_ref", itemId );
			xmlWriter.writeAttribute( "number_of_output", QString::number( i + 1 ) );
			xmlWriter.writeAttribute( "type_number", processObjectType->GetMetaInputTypes().at(i) );
        }
        xmlWriter.writeEndElement();    // close inputs

        //outputs
        xmlWriter.writeStartElement( "output" );
        xmlWriter.writeAttribute( "number_images", QString::number( processObjectType->GetNumberImageOutputs() ) );
        xmlWriter.writeAttribute( "number_meta", QString::number( processObjectType->GetNumberMetaOutputs() ) );
        for(int i = 0; i < processObjectType->GetNumberImageOutputs(); i++)
		{
            xmlWriter.writeEmptyElement( "image" );
            xmlWriter.writeAttribute( "number", QString::number( i + 1 ) );
			xmlWriter.writeAttribute( "type_number", QString::number( processObjectType->GetImageOutputTypes().at(i) ));
        }
        for(int i = 0; i < processObjectType->GetNumberMetaOutputs(); i++)
		{
            xmlWriter.writeEmptyElement("meta");
            xmlWriter.writeAttribute( "number", QString::number( i + 1 ) );
			xmlWriter.writeAttribute( "type_number", processObjectType->GetMetaOutputTypes().at(i) );
        }

        xmlWriter.writeEndElement();

        // arguments
        xmlWriter.writeStartElement( "arguments" );
        for(int i = 0; i < processObjectSettings->GetNumSettings(); i++)
		{
            xmlWriter.writeEmptyElement( "parameter" );
            xmlWriter.writeAttribute( "key", processObjectSettings->GetSettingName( i ) );
            xmlWriter.writeAttribute( "value", processObjectSettings->GetSettingValue( i ) );
            if( mWriteDescription )
                xmlWriter.writeAttribute( "description", processObjectSettings->GetSettingDescription( i ) );
        }
        xmlWriter.writeEndElement(); // close arguments

        xmlWriter.writeEndElement(); // close item
    }

    xmlWriter.writeEndElement();
    xmlWriter.writeEndDocument();
}

} //namespace XPIWIT

#endif
