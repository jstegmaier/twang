/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

#ifndef __XPIWIT_UNCERTAINTYGUIDEDWATERSHEDSEGMENTATIONFILTER_H
#define __XPIWIT_UNCERTAINTYGUIDEDWATERSHEDSEGMENTATIONFILTER_H

// include required headers
#include "../Base/Management/ITKDefinitions.h"
#include "itkExtractRegionPropsImageFilter.h"
#include "itkMacro.h"
#include <string>
#include "../Base/MetaData/MetaDataFilter.h"

using namespace XPIWIT;

namespace itk
{

/**
 * @class UncertaintyGuidedWatershedSegmentationFilter
 * Fast uncertainty based watershed for seed based splitting of connected objects.
 */
template <class TImageType>
class ITK_EXPORT UncertaintyGuidedWatershedSegmentationFilter : public ImageToImageFilter<TImageType, TImageType>
{
    public:
        // Extract dimension from input and output image.
        itkStaticConstMacro(ImageDimension, unsigned int, TImageType::ImageDimension);
        typedef UncertaintyGuidedWatershedSegmentationFilter Self;
        typedef ImageToImageFilter<TImageType,TImageType> Superclass;
        typedef SmartPointer<Self> Pointer;
        typedef SmartPointer<const Self> ConstPointer;
        itkNewMacro(Self);

        // definition of variable type abbreviations
        typedef typename TImageType::PixelType PixelType;
        typedef SeedPoint<TImageType> SeedPointType;
        typedef RegionProps RegionPropsType;
        typedef typename TImageType::RegionType InputImageRegionType;
        typedef typename TImageType::RegionType OutputImageRegionType;

        /**
         * The default constructor.
         */
        UncertaintyGuidedWatershedSegmentationFilter();

        /**
         * The destructor.
         */
        virtual ~UncertaintyGuidedWatershedSegmentationFilter();

        /**
         * Set and get methods for the member variables.
         */
        itkGetMacro( FullyConnected, bool);
        itkSetMacro( FullyConnected, bool);
        itkSetMacro( UncertaintyCombinationFunction, int );
		itkGetMacro( UncertaintyCombinationFunction, int );
        itkSetMacro( UncertaintyThreshold, float );
        itkGetMacro( UncertaintyThreshold, float );

        void SetRegionProps(MetaDataFilter* metaFilter) { m_RegionProps = metaFilter; }
		void SetFuzzySetParameters(MetaDataFilter* metaFilter) { m_FuzzySetParameters = metaFilter; }
		void SetSeedImage(const TImageType* image)               { m_SeedImage = image; }
		void SetLabelImage(const TImageType* image)               { m_LabelImage = image; }

    protected:

        /**
         * Functions for data generation.
         * @param outputRegionForThread the region to be processed.
         * @param threadId the id of the current thread.
         */
        void ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId);

        /**
         * functions for prepareing and post-processing the threaded generation.
         */
        void BeforeThreadedGenerateData();
        void AfterThreadedGenerateData();

        float m_UncertaintyThreshold;						// minimum standard deviation of the intensity values of an extracted region
		bool m_FullyConnected;
		int m_UncertaintyCombinationFunction;

		MetaDataFilter* m_RegionProps;					// region props input
        MetaDataFilter* m_FuzzySetParameters;              // key points input meta data

		QList< QList< QList<float> > > m_RegionPropsPerThread;

		const TImageType* m_SeedImage;
		const TImageType* m_LabelImage;
};

} // namespace itk

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkUncertaintyGuidedWatershedSegmentationFilter.hxx"
#endif

#endif
