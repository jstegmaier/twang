/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// include required headers
#include "itkSliceBySliceSignedMaurerDistanceMapImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkImageRegionConstIterator.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include <iostream>
#include <fstream>
#include <string>
#include <limits>
#include <QString>
#include <QStringList>
#include <QDateTime>
#include "../../Core/Utilities/Logger.h"

// namespace itk
namespace itk
{

// the default constructor
template <class TImageType, class TOutputImage>
SliceBySliceSignedMaurerDistanceMapImageFilter<TImageType, TOutputImage>::SliceBySliceSignedMaurerDistanceMapImageFilter()
{
	m_UseImageSpacing = 1;
	m_UseSquaredDistance = 1;
	m_InsideIsPositive = 0;
	m_BackgroundValue = 0;
}


// the destructor
template <class TImageType, class TOutputImage>
SliceBySliceSignedMaurerDistanceMapImageFilter<TImageType, TOutputImage>::~SliceBySliceSignedMaurerDistanceMapImageFilter()
{
}


// before threaded generate data
template <class TImageType, class TOutputImage>
void SliceBySliceSignedMaurerDistanceMapImageFilter<TImageType, TOutputImage>::BeforeThreadedGenerateData()
{
    // Allocate output
    typename TOutputImage::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();
}


// the thread generate data
template <class TImageType, class TOutputImage>
void SliceBySliceSignedMaurerDistanceMapImageFilter<TImageType, TOutputImage>::ThreadedGenerateData(const OutputImageRegionType& outputRegionForThread, ThreadIdType threadId)
{
    // get input and output pointers
    typename TOutputImage::Pointer output = this->GetOutput();
    typename TImageType::ConstPointer input  = this->GetInput();
	
	// initialize the regions to process
	typename TImageType::RegionType currentRegion;
	typename TImageType::IndexType currentIndex;
	typename TImageType::SizeType currentSize;

	for (int j=0; j<TImageType::ImageDimension; ++j)
	{
		currentIndex[j] = outputRegionForThread.GetIndex(j);
		currentSize[j] = outputRegionForThread.GetSize(j);
	}
	currentSize[2] = 1;
	currentRegion.SetIndex( currentIndex );
	currentRegion.SetSize( currentSize );
	
	// create a temporary image used to extract slices
	typename TImageType::Pointer tmpImage = TImageType::New();
	tmpImage->SetRegions(currentRegion);
	tmpImage->SetSpacing( input->GetSpacing() );
	tmpImage->Allocate();
	tmpImage->FillBuffer(0);

	// iterate over all slices and perform watershed separately for each slice
	for (int i=0; i<outputRegionForThread.GetSize(2); ++i)
	{
		// set the current index
		currentIndex[2] = outputRegionForThread.GetIndex(2)+i;
		currentRegion.SetIndex( currentIndex );
		currentRegion.SetSize( currentSize );
		
		// extract the slice to perform the distance map calculation on
		ImageRegionConstIterator<TImageType> inputIterator( input, currentRegion );
		ImageRegionIterator<TImageType> sliceIterator( tmpImage, tmpImage->GetLargestPossibleRegion() );
		inputIterator.GoToBegin();
		sliceIterator.GoToBegin();

		while(inputIterator.IsAtEnd() == false)
		{
			// copy the input value
			sliceIterator.Set( inputIterator.Value() );

			// increment the iterators
			++inputIterator;
			++sliceIterator;
		}

		// setup the distance map filter
		typedef itk::SignedMaurerDistanceMapImageFilter<TImageType, TImageType> DistanceMapFilterType;
		typename DistanceMapFilterType::Pointer distanceMapFilter = DistanceMapFilterType::New();
		distanceMapFilter->SetInput( tmpImage );
		distanceMapFilter->SetReleaseDataFlag( true );
		distanceMapFilter->SetInsideIsPositive( m_InsideIsPositive );
		distanceMapFilter->SetSquaredDistance( m_UseSquaredDistance );
		distanceMapFilter->SetBackgroundValue( m_BackgroundValue );
		distanceMapFilter->SetUseImageSpacing( m_UseImageSpacing );
		distanceMapFilter->SetNumberOfThreads( 1 );
		itkTryCatch( distanceMapFilter->Update(), "Exception Caught: Updating distance map calculation of the slice-based watershed filter." );

		// initialize the seed region iterator
		ImageRegionIterator<TOutputImage> distanceMapIterator( distanceMapFilter->GetOutput(), distanceMapFilter->GetOutput()->GetLargestPossibleRegion() );
		ImageRegionIterator<TOutputImage> outputIterator( output, currentRegion );

		distanceMapIterator.GoToBegin();
		outputIterator.GoToBegin();
		while (outputIterator.IsAtEnd() == false)
		{
			outputIterator.Set(distanceMapIterator.Value());

			++outputIterator;
			++distanceMapIterator;
		}
	}
}


// after threaded generate data
template <class TImageType, class TOutputImage>
void SliceBySliceSignedMaurerDistanceMapImageFilter<TImageType, TOutputImage>::AfterThreadedGenerateData()
{
    // maybe peform post processing here?
}

} // end namespace itk
