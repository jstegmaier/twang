/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

#ifndef __XPIWIT_EXTRACTREGIONPROPSIMAGEFILTER_H
#define __XPIWIT_EXTRACTREGIONPROPSIMAGEFILTER_H

// include required headers
#include "../Base/Management/ITKDefinitions.h"
#include "itkBinaryImageToLabelMapFilter.h"
#include "itkLabelGeometryImageFilter.h"
#include "itkLabelMapToLabelImageFilter.h"
#include "itkMatrix.h"
#include <string>
#include <QString>
#include "../Base/MetaData/MetaDataFilter.h"

namespace itk
{

/**
 * @class RegionProps container class for region properties.
 * used to store information such as index, scale, intensity, ...
 */
class RegionProps
{
    public:
        /**
         * The constructor to instantly set all contained variables.
         * scheme: id, size, xpos, ypos, zpos, xsize, ysize, zsize, weightedx, weightedy, weightedz, minaxis, medaxis, majaxis, eccentricity, elongation, orientation, integratedintensity, ...
         *         sigma, seedIntensity, seedScale, minIntensityForeground, maxIntensityForeground, minIntensityBackground, maxIntensityBackground, volumeForeground, volumeBackground, foregroundIntensity, backgroundIntensity, fg/bg ratio
         */
        RegionProps(const unsigned int id, const unsigned int volume,
                    const unsigned int x, const unsigned int y, const unsigned int z,
                    const int width, const int height, const int depth,
                    const float weightedX, const float weightedY, const float weightedZ,
                    const float minorAxis, const float mediumAxis, const float majorAxis,
                    const float eccentricity, const float elongation, const float orientation,
                    const float integratedIntensity, const float regionSigma,
                    const float seedIntensity, const float seedScale,
                    const float minIntensityForeground, const float maxIntensityForeground, const float minIntensityBackground, const float maxIntensityBackground,
                    const int volumeForeground, const int volumeBackground,
                    const float intensityForeground, const float intensityBackground, const float foregroundBackgroundRatio, const float integratedGradientMagnitude)
        {
            m_ID = id;
            m_Volume = volume;
            m_X = x;
            m_Y = y;
            m_Z = z;
            m_Width = width;
            m_Height = height;
            m_Depth = depth;
            m_WeightedX = weightedX;
            m_WeightedY = weightedY;
            m_WeightedZ = weightedZ;
            m_MinorAxis = minorAxis;
            m_MediumAxis = mediumAxis;
            m_MajorAxis = majorAxis;
            m_Eccentricity = eccentricity;
            m_Elongation = elongation;
            m_Orientation = orientation;
            m_IntegratedIntensity = integratedIntensity;
            m_SeedIntensity = seedIntensity;
            m_SeedScale = seedScale;
            m_RegionSigma = regionSigma;
            m_MinIntensityForeground = minIntensityForeground;
            m_MaxIntensityForeground = maxIntensityForeground;
            m_MinIntensityBackground = minIntensityBackground;
            m_MaxIntensityBackground = maxIntensityBackground;
            m_VolumeForeground = volumeForeground;
            m_VolumeBackground = volumeBackground;
            m_IntensityForeground = intensityForeground;
            m_IntensityBackground = intensityBackground;
            m_ForegroundBackgroundRatio = foregroundBackgroundRatio;
            m_IntegratedGradientMagnitude = integratedGradientMagnitude;
        }

        ~RegionProps() {}

        itkGetMacro( ID, unsigned int);
        itkGetMacro( Volume, unsigned int);
        itkGetMacro( X, unsigned int);
        itkGetMacro( Y, unsigned int);
        itkGetMacro( Z, unsigned int);
        itkGetMacro( Width, int);
        itkGetMacro( Height, int);
        itkGetMacro( Depth, int);
        itkGetMacro( WeightedX, float);
        itkGetMacro( WeightedY, float);
        itkGetMacro( WeightedZ, float);
        itkGetMacro( IntegratedIntensity, float);
        itkGetMacro( MinorAxis, float);
        itkGetMacro( MediumAxis, float);
        itkGetMacro( MajorAxis, float);
        itkGetMacro( Eccentricity, float);
        itkGetMacro( Elongation, float);
        itkGetMacro( Orientation, float);
        itkGetMacro( RegionSigma, float);
        itkGetMacro( SeedScale, float );
        itkGetMacro( MinIntensityForeground, float );
        itkGetMacro( MaxIntensityForeground, float );
        itkGetMacro( SeedIntensity, float );
        itkGetMacro( MinIntensityBackground, float );
        itkGetMacro( MaxIntensityBackground, float );
        itkGetMacro( VolumeForeground, unsigned int );
        itkGetMacro( VolumeBackground, unsigned int );
        itkGetMacro( IntensityForeground, float );
        itkGetMacro( IntensityBackground, float );
        itkGetMacro( ForegroundBackgroundRatio, float );
        itkGetMacro( IntegratedGradientMagnitude, float );

    private:

        unsigned int m_ID;
        unsigned int m_Volume;
        unsigned int m_X;
        unsigned int m_Y;
        unsigned int m_Z;
        int m_Width;
        int m_Height;
        int m_Depth;
        float m_WeightedX;
        float m_WeightedY;
        float m_WeightedZ;
        float m_IntegratedIntensity;
        float m_MinorAxis;
        float m_MediumAxis;
        float m_MajorAxis;
        float m_Eccentricity;
        float m_Elongation;
        float m_Orientation;
        float m_RegionSigma;
        float m_SeedIntensity;
        float m_SeedScale;
        float m_MinIntensityForeground;
        float m_MaxIntensityForeground;
        float m_MinIntensityBackground;
        float m_MaxIntensityBackground;
        unsigned int m_VolumeForeground;
        unsigned int m_VolumeBackground;
        float m_IntensityForeground;
        float m_IntensityBackground;
        float m_ForegroundBackgroundRatio;
        float m_IntegratedGradientMagnitude;
};


/**
 * @class ExtractRegionPropsImageFilter
 * Used to extract properties of binary connected regions within an image. Input image can be either
 * binary or a label image. In case of a binary image, a label image is automatically generated.
 */
template <class TInputImageType>
class ITK_EXPORT ExtractRegionPropsImageFilter : public ImageToImageFilter<TInputImageType, TInputImageType>
{
    public:
        // Extract dimension from input and output image.
        itkStaticConstMacro(ImageDimension, unsigned int, TInputImageType::ImageDimension);
        typedef ExtractRegionPropsImageFilter Self;
        typedef ImageToImageFilter<TInputImageType, TInputImageType> Superclass;
        typedef SmartPointer<Self> Pointer;
        typedef SmartPointer<const Self> ConstPointer;

        itkNewMacro(Self);
        typedef TInputImageType InputImageType;

        /**
         * The constructor.
         */
        ExtractRegionPropsImageFilter();

        /**
         * The destructor.
         */
        virtual ~ExtractRegionPropsImageFilter();

        /**
         * set and get function for the member variables
         */
        itkGetMacro( FileName, std::string);
        itkSetMacro( FileName, std::string);
        itkGetMacro( BinaryInput, bool);
        itkSetMacro( BinaryInput, bool);
        itkGetMacro( LogResults, bool);
        itkSetMacro( LogResults, bool);
        itkSetMacro( IntensityImage, typename InputImageType::Pointer );
        itkGetMacro( IntensityImage, typename InputImageType::Pointer );
        itkSetMacro( CentroidOffset, int );
        itkGetMacro( CentroidOffset, int );
        itkSetMacro( FullyConnected, bool );
        itkGetMacro( FullyConnected, bool );

        void SetMetaObject( XPIWIT::MetaDataFilter* metaObject ){ m_MetaObject = metaObject; }

    protected:
        // typedef abbreviations for some filters
        typedef typename itk::BinaryImageToLabelMapFilter<InputImageType> BinaryImageToLabelMapFilterType;
        typedef typename itk::LabelMapToLabelImageFilter<LabelMap< LabelObject< SizeValueType, TInputImageType::ImageDimension > >, InputImageType> LabelMapToLabelImageFilterType;
        typedef typename itk::LabelGeometryImageFilter<InputImageType, InputImageType> LabelGeometryImageFilterType;

        /**
         * Functions for data generation.
         */
        void GenerateData();

        XPIWIT::MetaDataFilter *m_MetaObject;						// meta data object to store results
        std::string m_FileName;								// the file to store the extracted information to
        bool m_BinaryInput;									// flag to switch between binary or labeled input
        typename InputImageType::Pointer m_IntensityImage;	// intensity image. If provided mean intensity and the like can also be extracted
        int m_CentroidOffset;								// offset that is added to extracted centroids, e.g. useful if converting to matlab representation
        bool m_FullyConnected;								// determines the connectivity of the regions to be identified.
        bool m_LogResults;									// if true the regions are additionally written to the cout
};

} // namespace itk

#ifndef ITK_MANUAL_INSTANTIATION
#include "itkExtractRegionPropsImageFilter.txx"
#endif

#endif
