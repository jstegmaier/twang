/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkResampleImageFilter.h"
#include "itkScaleTransform.h"
#include "itkIdentityTransform.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
ResizeFilterWidget<TInputImage>::ResizeFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = "ResizeFilter";
	this->mDescription = "Resize the filter according to the specified size or scaling factors.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Width", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The new width for the image." );
    processObjectSettings->AddSetting( "Height", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The new height for the image." );
    processObjectSettings->AddSetting( "Depth", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The new depth for the image." );
    processObjectSettings->AddSetting( "ScaleX", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The scaling factor for the x dimension of the image." );
    processObjectSettings->AddSetting( "ScaleY", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The scaling factor for the y dimension of the image." );
    processObjectSettings->AddSetting( "ScaleZ", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The scaling factor for the z dimension of the image." );
    processObjectSettings->AddSetting( "UseScaleFactors", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, the scaling factors are used for resizing." );
	processObjectSettings->AddSetting( "InterpolationType", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "0: NearestNeighbor, 1: LinearInterpolation" );


    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage>
ResizeFilterWidget<TInputImage>::~ResizeFilterWidget()
{
}


// the update function
template< class TInputImage>
void ResizeFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get the parameter values
    float newWidth = processObjectSettings->GetSettingValue( "Width" ).toDouble();
    float newHeight = processObjectSettings->GetSettingValue( "Height" ).toDouble();
    float newDepth = processObjectSettings->GetSettingValue( "Depth" ).toDouble();
    float scaleX = processObjectSettings->GetSettingValue( "ScaleX" ).toDouble();
    float scaleY = processObjectSettings->GetSettingValue( "ScaleY" ).toDouble();
    float scaleZ = processObjectSettings->GetSettingValue( "ScaleZ" ).toDouble();
    bool useScaleFactors = processObjectSettings->GetSettingValue( "UseScaleFactors" ).toInt() > 0;
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const int interpolationType = processObjectSettings->GetSettingValue( "InterpolationType" ).toInt();

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

    // set image sizes
    typename TInputImage::PointType origin = inputImage->GetOrigin();
    typename TInputImage::SizeType inputSize = inputImage->GetLargestPossibleRegion().GetSize();
    typename TInputImage::SizeType outputSize = inputImage->GetLargestPossibleRegion().GetSize();
    typename TInputImage::SpacingType inputSpacing = inputImage->GetSpacing();
    typename TInputImage::SpacingType outputSpacing = inputImage->GetSpacing();

    // initialize the transformation for rescaling to the desired size.
	typedef itk::IdentityTransform<double, TInputImage::ImageDimension> IdentityTransformType;
	typename IdentityTransformType::Pointer scaleTransform = IdentityTransformType::New();
	itk::FixedArray<double, 3> scale;
    itk::FixedArray<double, 3> newSize;

    scale[0] = scaleX; scale[1] = scaleY; scale[2] = scaleZ;
    newSize[0] = newWidth; newSize[1] = newHeight; newSize[2] = newDepth;

    if (useScaleFactors == false)
    {
        for (int i=0; i<TInputImage::ImageDimension; ++i)
            outputSize[i] = (double)newSize[i];
    }
    else
    {
        for (int i=0; i<TInputImage::ImageDimension; ++i)
            outputSize[i] = (double)inputSize[i] * scale[i];
    }

    // set output spacing and the origin
    Logger::GetInstance()->WriteLine( QString("- Changed image spacing to [" ));
	for (int i=0; i<TInputImage::ImageDimension; ++i)
    {
        outputSpacing[i] = (double)inputSpacing[i] * (double)inputSize[i] / (double)outputSize[i];
        origin[i] = origin[i] / scale[i];

		Logger::GetInstance()->WriteLine( QString().number(outputSpacing[i]) + QString(", ") );
    }
	Logger::GetInstance()->WriteLine( QString("]") );

    // resample the image using the specified transform
    typedef itk::ResampleImageFilter<TInputImage, TInputImage, double> ResampleImageFilterType;
    typename ResampleImageFilterType::Pointer rescaleFilter = ResampleImageFilterType::New();
    rescaleFilter->SetInput( inputImage );
    rescaleFilter->SetOutputSpacing( outputSpacing );
    rescaleFilter->SetOutputOrigin( origin );
    
	if (interpolationType == 0)
	{
		typedef itk::NearestNeighborInterpolateImageFunction<TInputImage, double > InterpolatorType;
		typename InterpolatorType::Pointer interpolator = InterpolatorType::New();
		rescaleFilter->SetInterpolator( interpolator );
	}
	else if (interpolationType == 1)
	{
		 typedef itk::LinearInterpolateImageFunction<TInputImage, double > InterpolatorType;
		typename InterpolatorType::Pointer interpolator = InterpolatorType::New();
		rescaleFilter->SetInterpolator( interpolator );
	}

	rescaleFilter->SetSize( outputSize );
    rescaleFilter->UpdateLargestPossibleRegion();

    try
    {
        rescaleFilter->Update();
    }
    catch( itk::ExceptionObject & err )
    {
        Logger::GetInstance()->WriteLine( "ExceptionObject caught !" );
        Logger::GetInstance()->WriteException( err );
        return;
    }

	ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( rescaleFilter->GetOutput() );
    mOutputImages.append( outputImage );

    // update the process widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

} // namespace XPIWIT
