/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

#ifdef XPIWIT_USE_GPU

// project header
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"
#include "../../ITKCustom/itkSliceBySliceWatershedImageFilter.h"
#include "../../CUDA/watershed/watershedPBC.h"

// itk header
#include "GPUMorphologicalWatershedFilterWidget.h"
#include "itkConstNeighborhoodIterator.h"
#include "itkImageRegionIterator.h"
#include "itkMorphologicalWatershedImageFilter.h"
#include "itkPasteImageFilter.h"
#include "itkExtractImageFilter.h"

// system header
#include <stdint.h>
#include <iostream>
#include <fstream>

namespace XPIWIT
{

// the default constructor
template< class TInputImage >
GPUMorphologicalWatershedFilterWidget<TInputImage>::GPUMorphologicalWatershedFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = "GPUMorphologicalWatershedFilter";
    this->mDescription = "Morphological Watershed Filter. ";
    this->mDescription += "Performs watershed segmentation of the input image.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);

    this->mObjectType->SetNumberImageInputs(1);
    this->mObjectType->AppendImageInputType(1);

	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);

    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings to the filter
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Level", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Initial level of the watershed." );
	processObjectSettings->AddSetting( "MarkWatershedLine", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, watershed lines are highlighted by zero values." );
    processObjectSettings->AddSetting( "Segment3D", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Use a 3D watershed segmentation." );
	processObjectSettings->AddSetting( "FullyConnected", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled 8-neighborhood (2D) or 27-neighborhood (3D) is used." );
	processObjectSettings->AddSetting( "DebugOutput", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled some debug output is printed." );
	processObjectSettings->AddSetting( "MinSlice", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "If positive, only the slices larger than this number are processed." );
	processObjectSettings->AddSetting( "MaxSlice", "100000", ProcessObjectSetting::SETTINGVALUETYPE_INT, "If positive, only the slices smaller than this number are processed." );
	
    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
GPUMorphologicalWatershedFilterWidget<TInputImage>::~GPUMorphologicalWatershedFilterWidget()
{
}


// update the filter
template< class TInputImage>
void GPUMorphologicalWatershedFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const bool segment3D = processObjectSettings->GetSettingValue( "Segment3D" ).toInt() > 0;
	const bool markWatershedLine = processObjectSettings->GetSettingValue( "MarkWatershedLine" ).toInt() > 0;
	const bool fullyConnected = processObjectSettings->GetSettingValue( "FullyConnected" ).toInt() > 0;
	const bool debugOutput = processObjectSettings->GetSettingValue( "DebugOutput" ).toInt() > 0;
	const float level = processObjectSettings->GetSettingValue( "Level" ).toFloat();
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	int64 minSlice = processObjectSettings->GetSettingValue( "MinSlice" ).toInt();
	int64 maxSlice = processObjectSettings->GetSettingValue( "MaxSlice" ).toInt();

	// pointer to the input image	
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	typename TInputImage::Pointer maskImage = mInputImages.at(1)->template GetImage<TInputImage>();

	///////////////////// PLACE YOUR CODE HERE /////////////////////////////
	// --> Result image should be written into the output wrapper, i.e. replace "inputImage" in line 82 by the processed image
	

	// typedefs for the watershed
	typedef itk::Image< unsigned short, TInputImage::ImageDimension > WatershedInputImageType;
	typedef itk::Image< int, TInputImage::ImageDimension > SegmentationImageType;
	typedef itk::IntensityWindowingImageFilter< TInputImage, WatershedInputImageType > RescaleIntensityWatershedInputFilterType;
	//typedef itk::MorphologicalWatershedImageFilter< WatershedInputImageType, SegmentationImageType > WatershedFilterType;
	//typedef itk::SliceBySliceWatershedImageFilter<WatershedInputImageType, SegmentationImageType> SliceBySliceWatershedImageFilterType;


	//read image dimensions
	TInputImage::RegionType region = inputImage->GetLargestPossibleRegion();
	TInputImage::SizeType imSize = region.GetSize();
	int64 Nim = 1;
	int64 imgDims[dimsImage];
	for(int ii = 0; ii < TInputImage::ImageDimension; ii++)
	{
		imgDims[ii] = imSize[ii];
		Nim *= imSize[ii];
	}

	// rescale the intensity range to 0-65535
	typename RescaleIntensityWatershedInputFilterType::Pointer rescaleInputImage = RescaleIntensityWatershedInputFilterType::New();
	rescaleInputImage->SetInput( inputImage );
	rescaleInputImage->SetWindowMinimum( 0.0 );
	rescaleInputImage->SetWindowMaximum( 1.0 );
	rescaleInputImage->SetOutputMinimum( 0 );
	rescaleInputImage->SetOutputMaximum( 65535 );
	itkTryCatch( rescaleInputImage->Update(), "Exception Caught: Rescale intensity input image for watershed segmentation." );
	
	
	//invert intensity since my watershed code looks for maxima
	typedef itk::InvertIntensityImageFilter <WatershedInputImageType> InvertIntensityImageFilterType;
	InvertIntensityImageFilterType::Pointer invertIntensityFilter = InvertIntensityImageFilterType::New();
	invertIntensityFilter->SetInput(rescaleInputImage->GetOutput());
	invertIntensityFilter->SetMaximum(65535);//inverts intensity of pixels by subtracting pixel value to a maximum value. The maximum value can be set with SetMaximum and defaults the maximum of input pixel type
	itkTryCatch( invertIntensityFilter->Update(), "Exception Caught: invert intensity input image for watershed segmentation." );

	
	//hMaxima filter
	//TODO amatf run it on the GPU
	//TODO amatf this is running Hmaxima in 3D (not slices by slice, so results are different)
	typedef itk::HMaximaImageFilter<WatershedInputImageType, WatershedInputImageType >  HmaximaFilterType;
	HmaximaFilterType::Pointer HmaximaFilter = HmaximaFilterType::New();
	HmaximaFilter->SetInput(  invertIntensityFilter->GetOutput() );
	HmaximaFilter->SetHeight( level );
	HmaximaFilter->SetFullyConnected( fullyConnected );
	itkTryCatch( HmaximaFilter->Update(), "Exception Caught: Hmaxima input image for watershed segmentation." );
	

	//declare GPU watershed object
	int devCUDA = 0;//TODO amatf: add this as a parameter in the XML file
	int conn3D = 4;//default for 2D
	

	
	//get pointer to image data within ITK class
	unsigned short int *img = HmaximaFilter->GetOutput()->GetBufferPointer();

	//setup watershed parameters depending on input parameters
	if( TInputImage::ImageDimension == 2) //2D image
	{
		imgDims[2] = 1;//our watershed code treats everything as 3D
		if( fullyConnected == true )
			conn3D = 8;

	}else{//3D image stack		
		if (segment3D == false)//perform slie by slice
		{
			if( fullyConnected == true )
				conn3D = 8;
			else
				conn3D = 4;
		}else{//full 3D watershed
			if( fullyConnected == true )
				conn3D = 26;
			else
				conn3D = 6;
		}
	}

	//restrict slices that are only background
	int64 imIdx = minSlice * imgDims[0] * imgDims[1];
	unsigned short int bb = img[imIdx];
	while( img[imIdx] == bb )
		imIdx++;
	minSlice = floor (float(imIdx) / float(imgDims[0] * imgDims[1]));

	imIdx = std::min(Nim, maxSlice * imgDims[0] * imgDims[1]) - 1;
	bb = img[imIdx];
	while( img[imIdx] == bb )
		imIdx--;
	maxSlice = imgDims[2] - floor (float(Nim - 1 - imIdx) / float(imgDims[0] * imgDims[1]));

	//add these slices to mask
	TInputImage::PixelType *mask = maskImage->GetBufferPointer();
	memset(mask, 0, sizeof(TInputImage::PixelType) * minSlice * imgDims[0] * imgDims[1]);
	memset(&(mask[maxSlice * imgDims[0] * imgDims[1]]), 0, sizeof(TInputImage::PixelType) * (imgDims[2] - maxSlice) * imgDims[0] * imgDims[1]);
	

	//call watershed	
	watershedPBC_CUDA<unsigned short> wPBC(img, imgDims, conn3D, devCUDA);
	int err = wPBC.waterhsedWithSortedElementsNoTau(0, mask);
	if( err > 0 )
		return;	

	//TODO amatf It seems I got a hold on how interface with XPIWIT AND ITK correctly
	//However the Watershed in ITK is pretty involve (see section 6.3 from documentation sent by Johannes)
	//So I'll do some simpler filters and the come back to here (I also need the slice by slice implementation, renumbering labels and look for minima)

	//collect output
	typename SegmentationImageType::Pointer tmpImage; //to store the results temporarily
	tmpImage = SegmentationImageType::New();

	typename SegmentationImageType::IndexType start;	
	typename SegmentationImageType::SizeType size;	
	for(int ii = 0; ii < TInputImage::ImageDimension; ii++)
	{
		size[ii] = imgDims[ii];	
		start[ii] = 0;
	}
	typename SegmentationImageType::RegionType region2(start, size);
	tmpImage->SetRegions(region2);
	tmpImage->Allocate();

	
	//copy result from CUDA
	int64 Nf = wPBC.numElementsForeground();
	imgLabelTypeCUDA* Lf = new imgLabelTypeCUDA[Nf];
	wPBC.getForegroundLabelsFromGPU(Lf);

	int *L = tmpImage->GetBufferPointer();//get pointer to image data within ITK class
	memset(L,0,wPBC.numElements() * sizeof(int));	
	imgLabelTypeCUDA maxL = 0;
	for(size_t ii = 0; ii < Nf; ii++)
	{
		L[ wPBC.foregroundVec[ii] ] = Lf[ii];
		maxL = std::max(maxL, Lf[ii]);
	}
	delete[] Lf;
	

	//parse labels to consecutive labels
	if( TInputImage::ImageDimension > 2 && segment3D == false)
	{
		int64 count = 0;
		short unsigned int *mapLabel = new short unsigned int[maxL + 1];//TODO amatf: use an efficient Hash table (VS2010 std::unordered_map is not)
		memset(mapLabel, 0, sizeof(short unsigned int) * (maxL + 1));
		for(int64 slice = 0; slice < imgDims[2]; slice++ )
		{
			short unsigned int numLabels = 0, label;
			for(int idx = 0; idx < imgDims[0] * imgDims[1]; idx++, count++)
			{
				if( L[count] != 0 )//L[count] = 0 -> background
				{
					label = mapLabel[ L[count] ];
					if( label == 0 )
					{
						numLabels++;
						label = numLabels;
						mapLabel[ L[count] ] = label;
					}				
					L[count] = label;
				}
			}
		}
		delete[] mapLabel;		
	}else{
		
		int *mapLabel = new int[maxL + 1];//TODO amatf: use an efficient Hash table (VS2010 std::unordered_map is not)
		memset(mapLabel, 0, sizeof(int) * (maxL + 1));
		int numLabels = 0, label;
		for(int64 count = 0; count <  wPBC.numElements(); count++ )
		{
			if( L[count] != 0 )//L[count] = 0 -> background
			{
				label = mapLabel[ L[count] ];
				if( label == 0 )
				{
					numLabels++;
					label = numLabels;
					mapLabel[ L[count] ] = label;
				}
				L[count] = label;			
			}
		}
		delete[] mapLabel;
	}	

	/*
	// rescale the watershed result to the internal format
	typedef itk::IntensityWindowingImageFilter< SegmentationImageType, TInputImage > RescaleIntensityFilterType;
	typename RescaleIntensityFilterType::Pointer rescaleIntensityFilter = RescaleIntensityFilterType::New();
	rescaleIntensityFilter->SetInput( tmpImage );
	rescaleIntensityFilter->SetWindowMinimum( 0 );
	rescaleIntensityFilter->SetWindowMaximum( 65535 );
	rescaleIntensityFilter->SetOutputMinimum( 0.0 );
	rescaleIntensityFilter->SetOutputMaximum( 1.0 );
	rescaleIntensityFilter->SetNumberOfThreads( maxThreads );
	itkTryCatch( rescaleIntensityFilter->Update(), "Exception Caught: Rescale watershed intensity to [0, 1] range." );
	*/
	///////////////////// PLACE YOUR CODE HERE /////////////////////////////

    // set the output image
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetRescaleFlag( false );
    outputWrapper->SetImage<SegmentationImageType>( tmpImage ); 
    mOutputImages.append( outputWrapper );

    // update the process update widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

} // namespace XPIWIT

#endif