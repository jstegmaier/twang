/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// project header
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkGrayscaleMorphologicalClosingImageFilter.h"
#include "itkConstNeighborhoodIterator.h"
#include "itkImageRegionIterator.h"


// XPIWIT namespace
namespace XPIWIT
{

// the default constructor
template< class TInputImage >
MorphologicalClosingFilterWidget<TInputImage>::MorphologicalClosingFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = "MorphologicalClosingFilter";
    this->mDescription = "Morphological Closing Filter. ";
    this->mDescription += "closes the image using erosion of the dilated input image.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);

    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);

    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);

    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings to the filter
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Radius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Radius of the filter kernel." );
	processObjectSettings->AddSetting( "MinRadius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Minimum radius of the filter kernel (UseRadiusRange has to be enabled for this parameter to work)." );
	processObjectSettings->AddSetting( "MaxRadius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Maximum radius of the filter kernel (UseRadiusRange has to be enabled for this parameter to work)." );
    processObjectSettings->AddSetting( "FilterMask3D", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Use a 3D kernel." );
	processObjectSettings->AddSetting( "UseRadiusRange", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Iteratively apply different radii to close the image (Used e.g. for viscous watershed)." );
	processObjectSettings->AddSetting( "SafeBorder", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Toggles border padding." );
	processObjectSettings->AddSetting( "Algorithm", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The algorithm to use: BASIC = 0, HISTO = 1, ANCHOR = 2, VHGW = 3." );
	
    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
MorphologicalClosingFilterWidget<TInputImage>::~MorphologicalClosingFilterWidget()
{
}


// update the filter
template< class TInputImage >
void MorphologicalClosingFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int radius = processObjectSettings->GetSettingValue( "Radius" ).toInt();
    const int filterMask3D = processObjectSettings->GetSettingValue( "FilterMask3D" ).toInt() > 0;
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const bool useRadiusRange = processObjectSettings->GetSettingValue( "UseRadiusRange" ).toInt() > 0;
	const bool safeBorder = processObjectSettings->GetSettingValue( "SafeBorder" ).toInt() > 0;
	float minRadius = processObjectSettings->GetSettingValue( "MinRadius" ).toFloat();
	float maxRadius = processObjectSettings->GetSettingValue( "MaxRadius" ).toFloat();
	const unsigned int algorithm = processObjectSettings->GetSettingValue( "Algorithm" ).toInt();

	// get images
    typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

	// if single radius should be used, set min and max accordingly
	if (useRadiusRange == false)
	{
		minRadius = radius;
		maxRadius = radius;
	}

	// typedefs for the filter and the structure element
    typedef itk::BinaryBallStructuringElement< typename TInputImage::PixelType, TInputImage::ImageDimension > StructuringElementType;
	typedef itk::GrayscaleMorphologicalClosingImageFilter<TInputImage, TInputImage, StructuringElementType> ClosingImageFilterType;
	
	typename TInputImage::SpacingType spacing = inputImage->GetSpacing();
	for (int j=minRadius; j<maxRadius; ++j)
	{
		// create the ball structuring element
		typename TInputImage::SizeType radius;
		radius[0] = int(0.5 + (float)j / spacing[0]);
		radius[1] = int(0.5 + (float)j / spacing[1]);
		radius[2] = int(0.5 + (float)j / spacing[2]);

		if (filterMask3D == false)
			radius[2] = 0;

		StructuringElementType structuringElement;
		structuringElement.SetRadius( radius );
		structuringElement.CreateStructuringElement();

		// create closing filter
		typename ClosingImageFilterType::Pointer closingImageFilter = ClosingImageFilterType::New();
		closingImageFilter->SetReleaseDataFlag( true );
		closingImageFilter->SetKernel( structuringElement );
		closingImageFilter->SetInput( inputImage );
		closingImageFilter->SetNumberOfThreads( maxThreads );
		closingImageFilter->SetSafeBorder( safeBorder );
		closingImageFilter->SetAlgorithm(algorithm);

		itkTryCatch( closingImageFilter->Update(), "Exception Caught: Updating the morphological closing operator." );
		inputImage = closingImageFilter->GetOutput();
	}
	
	inputImage->SetReleaseDataFlag( false );

    // set the output image
    ImageWrapper *outputWrapper = new ImageWrapper();
    outputWrapper->SetImage<TInputImage>( inputImage );
    mOutputImages.append( outputWrapper );

    // update the process update widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

} // namespace XPIWIT
