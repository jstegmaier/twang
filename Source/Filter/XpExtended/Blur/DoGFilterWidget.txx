/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// project header
#include "../../../Core/Utilities/Logger.h"
using namespace itk;


// XPIWIT namespace
namespace XPIWIT 
{

// the default constructor
template <class TInputImage>
DoGFilterWidget<TInputImage>::DoGFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = "DoGFilter";
    this->mDescription = "Difference of Gaussian Filter. ";
    this->mDescription += "The input image will be processed with the first gaussian kernel and subtracted by the result of the filtering with the second gaussian kernel.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Sigma1", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Sigma value of the smaller gaussian kernel" );
    processObjectSettings->AddSetting( "Sigma2", "5.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Sigma value of the bigger gaussian kernel");
    processObjectSettings->AddSetting( "MaximumError", "0.01", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Maximum error of the gaussian function approximation" );
    processObjectSettings->AddSetting( "MaximumKernelWidth", "32", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Maximum radius of the kernel in pixel" );
    processObjectSettings->AddSetting( "UseImageSpacing", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Use the real spacing for the gaussian kernel creation" );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template <class TInputImage>
DoGFilterWidget<TInputImage>::~DoGFilterWidget()
{
}


// the update function
template <class TInputImage>
void DoGFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    const bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
    const float sigma1 = processObjectSettings->GetSettingValue( "Sigma1" ).toDouble();
    const float sigma2 = processObjectSettings->GetSettingValue( "Sigma2" ).toDouble();

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

    typedef itk::ImageDuplicator< TInputImage > DuplicatorType;
    typename DuplicatorType::Pointer duplicator = DuplicatorType::New();
    duplicator->SetInputImage( inputImage );
    duplicator->Update();

    // create the DoG filter and adjust settings
    typedef itk::RecursiveGaussianImageFilter<TInputImage, TInputImage> GaussianFilter;
    typename GaussianFilter::Pointer filterS1 = GaussianFilter::New();
    filterS1->SetInput( duplicator->GetOutput() );
    filterS1->SetReleaseDataFlag( releaseDataFlag );
    filterS1->SetSigma( sigma1 );
    filterS1->SetNormalizeAcrossScale(true);
    filterS1->SetDirection(0);
    filterS1->SetNumberOfThreads( maxThreads );

    typename GaussianFilter::Pointer filterS2 = GaussianFilter::New();
    filterS2->SetInput( filterS1->GetOutput() );
    filterS2->SetReleaseDataFlag( releaseDataFlag );
    filterS2->SetSigma( sigma1 );
    filterS2->SetNormalizeAcrossScale(true);
    filterS2->SetDirection(1);
    filterS2->SetNumberOfThreads( maxThreads );

	typename GaussianFilter::Pointer filterS3 = GaussianFilter::New();
	if( TInputImage::ImageDimension > 2 ){
		filterS3->SetInput( filterS2->GetOutput() );
		filterS3->SetReleaseDataFlag( releaseDataFlag );
		filterS3->SetSigma( sigma1 );
		filterS3->SetNormalizeAcrossScale(true);
		filterS3->SetDirection(2);
		filterS3->SetNumberOfThreads( maxThreads );
		filterS3->Update();
	}

    typename GaussianFilter::Pointer filterL1 = GaussianFilter::New();
    filterL1->SetInput( duplicator->GetOutput() );
    filterL1->SetReleaseDataFlag( releaseDataFlag );
    filterL1->SetSigma( sigma2 );
    filterL1->SetNormalizeAcrossScale(true);
    filterL1->SetDirection(0);
    filterL1->SetNumberOfThreads( maxThreads );

    typename GaussianFilter::Pointer filterL2 = GaussianFilter::New();
    filterL2->SetInput( filterL1->GetOutput() );
    filterL2->SetReleaseDataFlag( releaseDataFlag );
    filterL2->SetSigma( sigma2 );
    filterL2->SetNormalizeAcrossScale(true);
    filterL2->SetDirection(1);
    filterL2->SetNumberOfThreads( maxThreads );

	typename GaussianFilter::Pointer filterL3 = GaussianFilter::New();
	if( TInputImage::ImageDimension > 2 ){
		filterL3->SetInput( filterL2->GetOutput() );
		filterL3->SetReleaseDataFlag( releaseDataFlag );
		filterL3->SetSigma( sigma2 );
		filterL3->SetNormalizeAcrossScale(true);
		filterL3->SetDirection(2);
		filterL3->SetNumberOfThreads( maxThreads );
		filterL3->Update();
	}

    typedef itk::SubtractImageFilter< TInputImage, TInputImage, TInputImage> SubtractType;
	typename SubtractType::Pointer diffFilter = SubtractType::New ();
	if( TInputImage::ImageDimension > 2 ){
		diffFilter->SetReleaseDataFlag( releaseDataFlag );
		diffFilter->SetInput1( filterS3->GetOutput() );
		diffFilter->SetInput2( filterL3->GetOutput() );
		diffFilter->SetNumberOfThreads( maxThreads );
	}else{

		diffFilter->SetReleaseDataFlag( releaseDataFlag );
		diffFilter->SetInput1( filterS2->GetOutput() );
		diffFilter->SetInput2( filterL2->GetOutput() );
		diffFilter->SetNumberOfThreads( maxThreads );
	}


    // update the pipeline by calling update for the most downstream filter
    try
    {
        diffFilter->Update();
    }
    catch( itk::ExceptionObject & err )
    {
        Logger::GetInstance()->WriteLine("ExceptionObject caught !" );
        Logger::GetInstance()->WriteException( err );
        return;
    }

	// save output
	ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( diffFilter->GetOutput() );
    mOutputImages.append( outputImage );

    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

} // namespace XPIWIT
