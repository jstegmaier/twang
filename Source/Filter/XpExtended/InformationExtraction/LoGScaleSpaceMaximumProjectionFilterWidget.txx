/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// project header
#include "../../../Core/Utilities/Logger.h"

#include "../../ITKCustom/itkExtractRegionPropsImageFilter.h"
#include "../../ITKCustom/itkExtractKeyPointsImageFilter.h"
#include "../../ThirdParty/itkMaximumWithInfoImageFilter.h"

// itk header
#include "itkMaximumImageFilter.h"
#include "itkMinimumImageFilter.h"
#include "itkMesh.h"
#include "itkInvertIntensityImageFilter.h"
#include "itkNeighborhoodIterator.h"
#include "itkAddImageFilter.h"
#include "itkStreamingImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkLaplacianRecursiveGaussianImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkIntensityWindowingImageFilter.h"

// qt header
#include <QtCore/QDir>


namespace XPIWIT
{

// the default constructor
template< class TImageType1, class TImageType2 >
LoGScaleSpaceMaximumProjectionFilterWidget<TImageType1, TImageType2>::LoGScaleSpaceMaximumProjectionFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = "LoGScaleSpaceMaximumProjectionFilter";
    this->mDescription = "Creates the maximum projection of multiple laplacian of gaussian filter results.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(2);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(2);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->AppendImageOutputType(2);

    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Step", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Step width from min to max sigma." );
    processObjectSettings->AddSetting( "MinSigma", "8.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Minimum sigma value." );
    processObjectSettings->AddSetting( "MaxSigma", "12.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Maximum sigma value." );
    processObjectSettings->AddSetting( "NormalizeAcrossScales", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Normalize scales." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TImageType1, class TImageType2 >
LoGScaleSpaceMaximumProjectionFilterWidget<TImageType1, TImageType2>::~LoGScaleSpaceMaximumProjectionFilterWidget()
{
}


// the update function
template< class TImageType1, class TImageType2 >
void LoGScaleSpaceMaximumProjectionFilterWidget<TImageType1, TImageType2>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // define internal integer data type
    typedef itk::Image<PixelUShort, TImageType1::ImageDimension> InternalImageTypeUShort;
    typedef itk::Image<PixelShort, TImageType1::ImageDimension> InternalImageTypeShort;
    typedef itk::Image<PixelUChar, TImageType1::ImageDimension> InternalImageTypeUChar;

    // typedefs for the filters
    typedef itk::LaplacianRecursiveGaussianImageFilter<TImageType1, TImageType1> LoGFilter;
    typedef itk::RescaleIntensityImageFilter<TImageType1> RescaleIntensityFilter;
    typedef itk::IntensityWindowingImageFilter<InternalImageTypeUShort, TImageType1> IntensityExternalToInternalType;
    typedef itk::IntensityWindowingImageFilter<TImageType1, InternalImageTypeUShort> IntensityInternalToExternalType;

    // get the dialog settings for the scale space creation
    float minSigma	= processObjectSettings->GetSettingValue( "MinSigma" ).toDouble();
    float maxSigma	= processObjectSettings->GetSettingValue( "MaxSigma" ).toDouble();
    float step		= processObjectSettings->GetSettingValue( "Step" ).toDouble();
    bool normalizeAcrossScales = processObjectSettings->GetSettingValue( "NormalizeAcrossScales" ).toInt() > 0;
    bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
    unsigned int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    QString outPath = processObjectSettings->GetSettingValue( "OutPath" );
    bool writeResult = processObjectSettings->GetSettingValue( "WriteResult" ).toInt() > 0;

	typename TImageType1::Pointer inputImage = mInputImages.at(0)->template GetImage<TImageType1>();
	typename TImageType1::SpacingType spacing = inputImage->GetSpacing();
//	bool oldReleaseDataFlag = inputImage->GetReleaseDataFlag();
//	inputImage->SetReleaseDataFlag( false );

    // initialize the maximum scale matrix
    typename TImageType2::Pointer scaleWithMaximumValue = TImageType2::New();
    scaleWithMaximumValue->SetRegions( inputImage->GetRequestedRegion() );
    scaleWithMaximumValue->SetSpacing( inputImage->GetSpacing() );
    scaleWithMaximumValue->Allocate();
    scaleWithMaximumValue->FillBuffer( minSigma );
    scaleWithMaximumValue->SetReleaseDataFlag( true );

    // invert the intensity of the scale space projection image
    typename itk::InvertIntensityImageFilter<TImageType1>::Pointer invertIntensity = itk::InvertIntensityImageFilter<TImageType1>::New();
    invertIntensity->SetInput( inputImage );
    invertIntensity->SetReleaseDataFlag( false );
    invertIntensity->SetNumberOfThreads( maxThreads );
    invertIntensity->SetMaximum( 1.0 );
	itkTryCatch( invertIntensity->Update(), "LoGScaleSlaceMaximumProjectionFilterWidget --> Invert intensity filter." );
	
    // initialize the log scale space maximum projection image
    typename TImageType1::Pointer mLoGScaleSpaceMaximumProjection = TImageType1::New();
    mLoGScaleSpaceMaximumProjection->SetRegions( inputImage->GetLargestPossibleRegion() );
    mLoGScaleSpaceMaximumProjection->SetSpacing( inputImage->GetSpacing() );
    mLoGScaleSpaceMaximumProjection->Allocate();
    mLoGScaleSpaceMaximumProjection->FillBuffer( 0 );
    mLoGScaleSpaceMaximumProjection->SetReleaseDataFlag( true );

    // create scale space representations for the selected standard deviations and perform maximum projection
    for (float i=minSigma; i<=maxSigma; i+=step)
    {
        // set release flag of the cached input if in the last loop
        if (i == maxSigma)
            invertIntensity->SetReleaseDataFlag( true );

         // filter the input image with the given standard deviation
        typedef itk::LaplacianRecursiveGaussianImageFilter<TImageType1, TImageType1> LoGFilterType;
        typename LoGFilterType::Pointer filter = LoGFilterType::New();
        filter->SetReleaseDataFlag( !writeResult );
        filter->SetNormalizeAcrossScale( normalizeAcrossScales );
        filter->SetNumberOfThreads( maxThreads );
        filter->SetSigma( i );
        filter->SetInput( invertIntensity->GetOutput() );

        // update the LoG Filtering for the current scale
        itkTryCatch( filter->Update(), "LoGScaleSlaceMaximumProjectionFilterWidget --> updating log filter" );

        // initialize the scale space maximum projection and the corresponding scale with maximum image to the first filtered version
        typedef itk::MaximumWithInfoImageFilter< TImageType1, TImageType1, TImageType2, TImageType1, TImageType2 > MaximumWithInfoImageFilterType;
        typename MaximumWithInfoImageFilterType::Pointer maximumWithInfoImageFilter = MaximumWithInfoImageFilterType::New();

        // setup the minimum with info filter with additionally to the binary minimum operation also stores the pixel that contained the maximum
        maximumWithInfoImageFilter->SetInput1( mLoGScaleSpaceMaximumProjection );
        maximumWithInfoImageFilter->SetInput2( filter->GetOutput() );
        maximumWithInfoImageFilter->SetInput3( scaleWithMaximumValue );
        maximumWithInfoImageFilter->SetNumberOfThreads( maxThreads );
        maximumWithInfoImageFilter->SetInfoValue( i );

        // try updating the filter
        itkTryCatch( maximumWithInfoImageFilter->Update(), "LoGScaleSlaceMaximumProjectionFilterWidget --> performing maximum with info image filter" );

        // set the filter result
        mLoGScaleSpaceMaximumProjection = maximumWithInfoImageFilter->GetOutput1();
        scaleWithMaximumValue = maximumWithInfoImageFilter->GetOutput2();
    }

    // convert the maximum projection to the internal image format
    mLoGScaleSpaceMaximumProjection->SetReleaseDataFlag( true );

    // update the intensity conversion
    //itkTryCatch( rescaleIntensity2->Update(), "LoGScaleSpaceMaximumProjection --> Final intensity conversion." );

	ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TImageType1>( mLoGScaleSpaceMaximumProjection );
    mOutputImages.append( outputImage );

	ImageWrapper *outputImage2 = new ImageWrapper();
    outputImage2->SetImage<TImageType2>( scaleWithMaximumValue );
    mOutputImages.append( outputImage2 );

    // update the process object
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

} // namespace XPIWIT