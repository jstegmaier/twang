/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkBoxMeanImageFilter.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
ExtractSeedBasedIntensityWindowFilterWidget<TInputImage>::ExtractSeedBasedIntensityWindowFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = "ExtractSeedBasedIntensityWindowFilter";
	this->mDescription = "Extracts the mean intensity values of an image from provided seed locations.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);

    this->mObjectType->SetNumberMetaInputs(1);
	this->mObjectType->AppendMetaInputType("KeyPoints");

	this->mObjectType->SetNumberMetaOutputs(1);
	this->mObjectType->AppendMetaOutputType("KeyPoints");
	

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Radius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The radius of the window to use for the mean intensity extraction." );
    processObjectSettings->AddSetting( "UseImageSpacing", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, the radius is scaled with respect to the image spacing." );
	processObjectSettings->AddSetting( "Threshold", "-1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "If larger than zero, used as a threshold, ie only seeds with larger values are saved." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
ExtractSeedBasedIntensityWindowFilterWidget<TInputImage>::~ExtractSeedBasedIntensityWindowFilterWidget()
{
}


// the update function
template< class TInputImage >
void ExtractSeedBasedIntensityWindowFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	this->mMetaOutputs.at(0)->mIsMultiDimensional = true;

	// get the input image
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	
    // get filter parameters
    const unsigned int windowRadius = processObjectSettings->GetSettingValue( "Radius" ).toInt();
	const float threshold = processObjectSettings->GetSettingValue( "Threshold" ).toFloat();
    const bool useImageSpacing = processObjectSettings->GetSettingValue( "UseImageSpacing" ).toInt() > 0;
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();

	// iterate over all seed points and 
	typedef typename itk::BoxMeanImageFilter<TInputImage> BoxMeanFilterType;
	typedef typename BoxMeanFilterType::RadiusType RadiusType;

	// setup the radius for the box filter	
	RadiusType radius;
	for (int i=0; i<TInputImage::ImageDimension; ++i)
	{
		if (useImageSpacing == true)
			radius[i] = int(0.5 + (float)windowRadius / inputImage->GetSpacing()[i]);
		else
			radius[i] = windowRadius;

		Logger::GetInstance()->WriteLine( "- Using radius " + QString().number(radius[i]) + QString(" for dimension ") + QString().number(i) );
	}

	// setup the box filter
	typename BoxMeanFilterType::Pointer boxMeanFilter = BoxMeanFilterType::New();
	boxMeanFilter->SetInput( inputImage );
	boxMeanFilter->SetNumberOfThreads( maxThreads );
	boxMeanFilter->SetRadius( radius );
	itkTryCatch( boxMeanFilter->Update(), "ExtractSeedBasedIntensityWindowFilterWidget: Update box filter" ); 

	// set the output meta information
	this->mMetaOutputs.at(0)->mTitle = this->mMetaInputs.at(0)->mTitle;
	this->mMetaOutputs.at(0)->mTitle << "meanIntensity";
	this->mMetaOutputs.at(0)->mType = this->mMetaInputs.at(0)->mType;
    this->mMetaOutputs.at(0)->mType << "float";

	// loop trough all seed points and extract the box filter value
	const int numKeyPoints = this->mMetaInputs.at(0)->mData.length();
	for(int i=0; i<numKeyPoints; i++)
	{
		QList<float> line = this->mMetaInputs.at(0)->mData.at(i);

		typename TInputImage::IndexType index;
		for (int j=0; j<TInputImage::ImageDimension; ++j)
			index[j] = int( 0.5+line.at(j+2)/inputImage->GetSpacing()[j] );

		// write keypoint
		line.append( boxMeanFilter->GetOutput()->GetPixel( index ) );

		if (line.at(line.length()-1) >= threshold || threshold < 0)
			this->mMetaOutputs.at(0)->mData.append( line );
	}

	ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( boxMeanFilter->GetOutput() );
    mOutputImages.append( outputImage );

    // update the base class
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

} // namespace XPIWIT
