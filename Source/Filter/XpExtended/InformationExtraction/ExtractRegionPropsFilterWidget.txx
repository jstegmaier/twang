/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkIntensityWindowingImageFilter.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkLabelGeometryImageFilter.h"
#include "itkLabelStatisticsImageFilter.h"
#include "itkBinaryImageToLabelMapFilter.h"
#include "itkLabelMapToLabelImageFilter.h"
#include "itkImageRegionConstIterator.h"


namespace XPIWIT
{

// the default constructor
template <class TInputImage>
ExtractRegionPropsFilterWidget<TInputImage>::ExtractRegionPropsFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = "ExtractRegionPropsFilter";
    this->mDescription = "Extracts the region properties of the image";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(0);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(1);
	this->mObjectType->AppendMetaOutputType("RegionProps");

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "BinaryInput", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If input is binary, a label image is generated before extracting the region properties." );
	processObjectSettings->AddSetting( "Threshold", "-1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "If input is binary, recreate a binary image to get rid of casting failures (-1 no recalculation)." );
    processObjectSettings->AddSetting( "FullyConnected", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Determines the connectivity model. Use FullyConnected = 1 for 8/26-neighborhood or to 0 for 4/6 neighborhood." );
	processObjectSettings->AddSetting( "GeometryMode", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Switch between geometry and statistics mode" );
	processObjectSettings->AddSetting( "MinimumVolume", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "All regions with smaller volume are suppressed. E.g. helpful for noise suppression in the EDM-based seed detection." );
	processObjectSettings->AddSetting( "MaximumVolume", "-1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "All regions with larger volume are suppressed. E.g. helpful for background suppression." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template <class TInputImage>
ExtractRegionPropsFilterWidget<TInputImage>::~ExtractRegionPropsFilterWidget()
{
}

// the update function
template <class TInputImage>
void ExtractRegionPropsFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
	const bool binaryInput = processObjectSettings->GetSettingValue( "BinaryInput" ).toInt() > 0;
    const bool fullyConnected = processObjectSettings->GetSettingValue( "FullyConnected" ).toInt() > 0;
	const bool geometryMode = processObjectSettings->GetSettingValue( "GeometryMode" ).toInt() > 0;
	const double threshold = processObjectSettings->GetSettingValue( "Threshold" ).toFloat();
	const unsigned int minimumVolume = processObjectSettings->GetSettingValue( "MinimumVolume" ).toInt();
	const unsigned int maximumVolume = processObjectSettings->GetSettingValue( "MaximumVolume" ).toInt();

	// define region props type and get the input image
	typedef typename itk::Image< PixelUShort, TInputImage::ImageDimension > RegionPropsImageType;
	typename RegionPropsImageType::Pointer inputImage = mInputImages.at(0)->template GetImage< RegionPropsImageType >();

	// get image info
	typename RegionPropsImageType::PointType origin = inputImage->GetOrigin();
    typename RegionPropsImageType::SizeType inputSize = inputImage->GetLargestPossibleRegion().GetSize();
    typename RegionPropsImageType::SizeType outputSize = inputImage->GetLargestPossibleRegion().GetSize();
    typename RegionPropsImageType::SpacingType inputSpacing = inputImage->GetSpacing();
    typename RegionPropsImageType::SpacingType outputSpacing = inputImage->GetSpacing();

    // initialize the geometry extraction filter
	typedef typename itk::LabelGeometryImageFilter<RegionPropsImageType, RegionPropsImageType> LabelGeometryImageFilterType;
    typename LabelGeometryImageFilterType::Pointer labelGeometryFilter =  LabelGeometryImageFilterType::New();
    labelGeometryFilter->SetCalculateOrientedBoundingBox( false );
    labelGeometryFilter->SetCalculateOrientedIntensityRegions( false );
    labelGeometryFilter->SetCalculateOrientedLabelRegions( false );
    labelGeometryFilter->SetCalculatePixelIndices( false );
    labelGeometryFilter->SetReleaseDataFlag( true );
    //labelGeometryFilter->SetIntensityInput( m_IntensityImage );

    if ( binaryInput )
    {
        // convert the binary image into a label map
		typedef typename itk::BinaryImageToLabelMapFilter<RegionPropsImageType> BinaryImageToLabelMapFilterType;
        typename BinaryImageToLabelMapFilterType::Pointer binaryImageToLabelMapFilter = BinaryImageToLabelMapFilterType::New();
        binaryImageToLabelMapFilter->SetFullyConnected( fullyConnected );
		binaryImageToLabelMapFilter->SetInputForegroundValue( 1 );
        binaryImageToLabelMapFilter->SetInput( inputImage );
        binaryImageToLabelMapFilter->SetReleaseDataFlag( false );
        itkTryCatch( binaryImageToLabelMapFilter->Update(), "ExtractRegionProps --> BinaryImageToLabelMapFilter" );

        // convert the label map into a label image
		typedef typename itk::LabelMapToLabelImageFilter< typename BinaryImageToLabelMapFilterType::OutputImageType, RegionPropsImageType> LabelMapToLabelImageFilterType;
        typename LabelMapToLabelImageFilterType::Pointer labelMapToLabelImageFilter = LabelMapToLabelImageFilterType::New();
        labelMapToLabelImageFilter->SetInput( binaryImageToLabelMapFilter->GetOutput() );
        labelMapToLabelImageFilter->SetReleaseDataFlag( true );
        itkTryCatch( labelMapToLabelImageFilter->Update(), "ExtractRegionProps --> LabelMapToLabelImage" );

        // set the extraction filter input
        labelGeometryFilter->SetInput( labelMapToLabelImageFilter->GetOutput() );
    }
    else
    {
        // directly get the information from the prelabeled input
        labelGeometryFilter->SetInput( inputImage );
    }

    // update the geometry extraction filter
    itkTryCatch( labelGeometryFilter->Update(), "ExtractRegionPropsImageFilter, extract geometry" );
    Logger::GetInstance()->WriteLine( "+ ExtractRegionPropsImageFilter: Found " + QString::number(labelGeometryFilter->GetNumberOfLabels()) + " connected regions in the binary image." );

	// Create the meta data Object
	MetaDataFilter* metaOutput = this->mMetaOutputs.at(0);
    metaOutput->mIsMultiDimensional = true;
	metaOutput->mPostfix = "RegionProps";

	QStringList metaDescription;                    QStringList metaType;
    metaDescription << "id";						metaType << "int";
	metaDescription << "size";						metaType << "int";
	metaDescription << "xpos";						metaType << "int";	// centroid
	metaDescription << "ypos";						metaType << "int";
	metaDescription << "zpos";						metaType << "int";
	metaDescription << "xsize";						metaType << "int";	// bounding box
	metaDescription << "ysize";						metaType << "int";
	metaDescription << "zsize";						metaType << "int";
	metaDescription << "aabbIndexX";				metaType << "int";	// weighted centroid
	metaDescription << "aabbIndexY";				metaType << "int";
	metaDescription << "aabbIndexZ";				metaType << "int";
	metaDescription << "integrated intensity";		metaType << "int";
	metaDescription << "major axis length";			metaType << "int";
	metaDescription << "minor axis length";			metaType << "int";
	metaDescription << "eccentricity";				metaType << "float";
	metaDescription << "elongation";				metaType << "float";
	metaDescription << "orientation";				metaType << "float";

	// write to meta object
	metaOutput->mTitle = metaDescription;
	metaOutput->mType = metaType;


	// get the labels
    typename LabelGeometryImageFilterType::LabelsType allLabels = labelGeometryFilter->GetLabels();
    typename LabelGeometryImageFilterType::LabelsType::iterator allLabelsIt;

	// iterate trough the labels and extract nucleus information
    for( allLabelsIt = allLabels.begin(); allLabelsIt != allLabels.end(); allLabelsIt++ )
    {
        typename LabelGeometryImageFilterType::LabelPixelType labelValue = *allLabelsIt;

		// check if volume is larger than the specified minimum volume
		if (labelGeometryFilter->GetVolume(labelValue) < minimumVolume)
			continue;

		if (maximumVolume > 0 && labelGeometryFilter->GetVolume(labelValue) > maximumVolume)
			continue;

		QList<float> currentData;
		currentData << labelValue;								// id
		currentData << labelGeometryFilter->GetVolume(labelValue);					// size
		currentData << labelGeometryFilter->GetCentroid(labelValue)[0];				// xpos			// centroid
		currentData << labelGeometryFilter->GetCentroid(labelValue)[1];				// ypos
		if( TInputImage::ImageDimension >= 3 )
			currentData << labelGeometryFilter->GetCentroid(labelValue)[2];			// zpos
		else
			currentData << 0;
		
		currentData << labelGeometryFilter->GetBoundingBoxSize(labelValue)[0];		// xsize		// bounding box
		currentData << labelGeometryFilter->GetBoundingBoxSize(labelValue)[1];		// ysize
		if( TInputImage::ImageDimension >= 3 )
			currentData << labelGeometryFilter->GetBoundingBoxSize(labelValue)[2];	// zsize
		else
			currentData << 1;

		currentData << labelGeometryFilter->GetBoundingBox(labelValue)[0];		// x index of the bounding box
		currentData << labelGeometryFilter->GetBoundingBox(labelValue)[2];		// y index of the bounding box
		if( TInputImage::ImageDimension >= 3 )
			currentData << labelGeometryFilter->GetBoundingBox(labelValue)[4];	// z index of the bounding box
		else
			currentData << 1;

		currentData << labelGeometryFilter->GetIntegratedIntensity(labelValue);		// integrated intensity
			// avarege: labelGeometryFilter->GetIntegratedIntensity(labelValue) / labelGeometryFilter->GetVolume(labelValue)
		//currentData << labelGeometryFilter->GetAxesLength(labelValue) );				// axes length
		currentData << labelGeometryFilter->GetMajorAxisLength(labelValue);			// major axis length
		currentData << labelGeometryFilter->GetMinorAxisLength(labelValue);			// minor axis length
		currentData << labelGeometryFilter->GetEccentricity(labelValue);				// eccentricity
		currentData << labelGeometryFilter->GetElongation(labelValue);				// elongation
		currentData << labelGeometryFilter->GetOrientation(labelValue);				// orientation

		metaOutput->mData.append( currentData );
    }

	// update the process object widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
	}

} // namespace XPIWIT

