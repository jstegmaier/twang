/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// project header
#include "../../../Core/Utilities/Logger.h"
#include "../../ITKCustom/itkSeedBasedTwangSegmentationFilter.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
TwangSegmentationWidget<TInputImage>::TwangSegmentationWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = "TwangSegmentationFilter";
	this->mDescription = "Applys the TWANG segmentation method on the supplied image as described by Stegmaier et al. Requires seed points as meta information.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(1);
	this->mObjectType->AppendMetaInputType("KeyPoints");
    this->mObjectType->SetNumberMetaOutputs(1);
	this->mObjectType->AppendMetaOutputType("RegionProps");
	
    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "MinimumRegionSigma", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The minimum std. dev. within a cropped image region. Ignored in the current implementation." );
    processObjectSettings->AddSetting( "Segment3D", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Used to perform the segmentation either directly in 3D or to merge 2D segmentation results instead." );
    processObjectSettings->AddSetting( "LabelOutput", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If true, the filter directly produces a labeled output image with a unique id for each detected blob." );
    processObjectSettings->AddSetting( "RandomLabels", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If checked, random labels are used. Note that random labels might not be unique." );
    processObjectSettings->AddSetting( "WriteRegionProps", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If checked, the region props of extracted blobs are exported to a cvs file." );
    processObjectSettings->AddSetting( "MinimumWeightedGradientNormalDotProduct", "0.6", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Used to threshold the weighted dot product image." );
    processObjectSettings->AddSetting( "WeightingKernelSizeMultiplicator", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Used to scale the plateau region of the weighting kernel. If set to 1 the seed radius is used for the plateau radius." );
    processObjectSettings->AddSetting( "WeightingKernelStdDev", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Multiplier for the weighting kernel standard deviation." );
    processObjectSettings->AddSetting( "GradientImageStdDev", "1.5", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The standard deviation of the Gaussian smoothing for smoother gradient directions." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
TwangSegmentationWidget<TInputImage>::~TwangSegmentationWidget()
{
}


// the update function
template< class TInputImage >
void TwangSegmentationWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	this->mMetaOutputs.at(0)->mIsMultiDimensional = true;

    // get filter parameters
    float minimumRegionSigma = processObjectSettings->GetSettingValue( "MinimumRegionSigma" ).toDouble();
    float minimumWeightedGradientNormalDotProduct = processObjectSettings->GetSettingValue( "MinimumWeightedGradientNormalDotProduct" ).toDouble();
    float weightingKernelSizeMultiplicator = processObjectSettings->GetSettingValue( "WeightingKernelSizeMultiplicator" ).toDouble();
    float weightingKernelStdDev = processObjectSettings->GetSettingValue( "WeightingKernelStdDev" ).toDouble();
    float gradientImageStdDev = processObjectSettings->GetSettingValue( "GradientImageStdDev" ).toDouble();
    bool segment3D = processObjectSettings->GetSettingValue( "Segment3D" ).toInt() > 0;
    bool labelOutput = processObjectSettings->GetSettingValue( "LabelOutput" ).toInt() > 0;
    bool randomLabels = processObjectSettings->GetSettingValue( "RandomLabels" ).toInt() > 0;
    bool writeRegionProps = processObjectSettings->GetSettingValue( "WriteRegionProps" ).toInt() > 0;
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();

    // setup the filters
    typedef itk::SeedBasedTwangSegmentationFilter<TInputImage> SeedBasedTwangSegmentationFilterType;
    typename itk::SeedBasedTwangSegmentationFilter<TInputImage>::Pointer twangSegmentationFilter = itk::SeedBasedTwangSegmentationFilter<TInputImage>::New();

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

    // initialize level sets segmentation
    twangSegmentationFilter->SetInput( inputImage );
    twangSegmentationFilter->SetSegment3D( segment3D );
    twangSegmentationFilter->SetNumberOfThreads( maxThreads );
    twangSegmentationFilter->SetLabelOutput( labelOutput );
    twangSegmentationFilter->SetMinimumRegionSigma( minimumRegionSigma );
    twangSegmentationFilter->SetMinimumWeightedGradientNormalDotProduct( minimumWeightedGradientNormalDotProduct );
    twangSegmentationFilter->SetWeightingKernelSizeMultiplicator( weightingKernelSizeMultiplicator );
    twangSegmentationFilter->SetWeightingKernelStdDev( weightingKernelStdDev );
    twangSegmentationFilter->SetGradientImageStdDev( gradientImageStdDev );
    twangSegmentationFilter->SetWriteRegionProps( writeRegionProps );
    twangSegmentationFilter->SetRandomLabels( randomLabels );
    twangSegmentationFilter->SetInputMetaFilter( this->mMetaInputs.at(0) );
    twangSegmentationFilter->SetOutputMetaFilter( this->mMetaOutputs.at(0) );

    // perform and set the output
    itkTryCatch( twangSegmentationFilter->Update(), "Twang Segmentation Called in TwangSegmentationWidget." );
    
	ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( twangSegmentationFilter->GetOutput() );
	outputImage->SetRescaleFlag( false );
    mOutputImages.append( outputImage );

    // update the base class
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

} // namespace XPIWIT
