/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkGeodesicActiveContourLevelSetImageFilter.h"
#include "itkBinaryThresholdImageFilter.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage>
LevelSetSegmentationWidget<TInputImage>::LevelSetSegmentationWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = "LevelSetSegmentationFilter";
	this->mDescription = "Perform geodesic active contours level sets segmentation as described by Caselles et al. Requires seed points as a meta input.";

	// set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(2);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);

	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "PropagationScaling", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The PropagationScaling parameter can be used to switch from propagation outwards (POSITIVE scaling parameter) versus propagating inwards (NEGATIVE scaling parameter)." );
    processObjectSettings->AddSetting( "CurvatureScaling", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "In general, the larger the CurvatureScaling, the smoother the resulting contour." );
    processObjectSettings->AddSetting( "AdvectionScaling", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Set the scaling of the advection field." );
    processObjectSettings->AddSetting( "NumIterations", "140", ProcessObjectSetting::SETTINGVALUETYPE_INT, "The maximum number to perform the level sets extension." );
	processObjectSettings->AddSetting( "MaximumRMSError", "0.02", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The maximum rms error used as convergence threshold." );
    processObjectSettings->AddSetting( "Segment3D", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Perform segmentation in 3D or segment 2D slices with subsequent fusion of the slice segmentation results." );
	processObjectSettings->AddSetting( "InsideIsPositive", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Controls which side of 0 is chosen for the actual segmentation. Default: negative values." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage>
LevelSetSegmentationWidget<TInputImage>::~LevelSetSegmentationWidget()
{
}


// the update function
template< class TInputImage >
void LevelSetSegmentationWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
	ProcessObjectBase::StartTimer();
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

	// get filter parameters
	const float maximumRMSError = processObjectSettings->GetSettingValue( "MaximumRMSError" ).toFloat();
    const float propagationScaling = processObjectSettings->GetSettingValue( "PropagationScaling" ).toFloat();
    const float curvatureScaling = processObjectSettings->GetSettingValue( "CurvatureScaling" ).toFloat();
    const float advectionScaling = processObjectSettings->GetSettingValue( "AdvectionScaling" ).toFloat();
    const int numIterations = processObjectSettings->GetSettingValue( "NumIterations" ).toInt();
    const bool segment3D = processObjectSettings->GetSettingValue( "Segment3D" ).toInt() > 0;
	const bool insideIsPositive = processObjectSettings->GetSettingValue( "InsideIsPositive" ).toInt() > 0;
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	typename TInputImage::Pointer edgeMapImage = mInputImages.at(1)->template GetImage<TInputImage>();
	
	typedef itk::GeodesicActiveContourLevelSetImageFilter<TInputImage, TInputImage> LevelSetsImageFilterType;
	typename LevelSetsImageFilterType::Pointer levelSetsFilter = LevelSetsImageFilterType::New();

	levelSetsFilter->SetInput( inputImage );
	levelSetsFilter->SetFeatureImage( edgeMapImage );
    levelSetsFilter->SetPropagationScaling( propagationScaling );
    levelSetsFilter->SetAdvectionScaling( advectionScaling );
    levelSetsFilter->SetCurvatureScaling( curvatureScaling );
	levelSetsFilter->SetMaximumRMSError( maximumRMSError );
    levelSetsFilter->SetNumberOfIterations( numIterations );

	typedef itk::BinaryThresholdImageFilter<TInputImage, TInputImage> ThresholdingFilterType;
	typename ThresholdingFilterType::Pointer thresholder = ThresholdingFilterType::New();
	thresholder->SetInput( levelSetsFilter->GetOutput() );
	thresholder->SetLowerThreshold( -65535 );
	thresholder->SetUpperThreshold( 0.0 );
	if (insideIsPositive == true)
	{	
		thresholder->SetLowerThreshold( 0.0 );
		thresholder->SetUpperThreshold( 65535 );
	}

	thresholder->SetOutsideValue( 0 );
	thresholder->SetInsideValue( 65535 );
	
    // perform and set the output
    itkTryCatch( thresholder->Update(), "LevelSet Segmentation Called in LevelSetSegmentationWidget." );

    ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( thresholder->GetOutput() );
    mOutputImages.append( outputImage );

    // update the base class
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

} // namespace XPIWIT
