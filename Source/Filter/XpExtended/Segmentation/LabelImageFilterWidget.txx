/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkThresholdImageFilter.h"
#include "itkOtsuThresholdImageFilter.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
LabelImageFilterWidget<TInputImage>::LabelImageFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = "LabelImageFilter";
    this->mDescription = "Label disjoint regions of a binary image with a unique id.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
LabelImageFilterWidget<TInputImage>::~LabelImageFilterWidget()
{
}


// the update function
template< class TInputImage >
void LabelImageFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    bool useOtsu = processObjectSettings->GetSettingValue( "UseOtsu" ).toInt() > 0;
    const float lowerThreshold = processObjectSettings->GetSettingValue( "LowerThreshold" ).toDouble();
    bool releaseDataFlag = processObjectSettings->GetSettingValue( "ReleaseDataFlag" ).toInt() > 0;
    bool inPlace = processObjectSettings->GetSettingValue( "InPlace" ).toInt() > 0;

    // define internal integer data type
    typedef typename itk::Image<PixelUShort, TInputImage::ImageDimension> InternalImageType;
	typename InternalImageType::Pointer inputImage = mInputImages.at(0)->template GetImage<InternalImageType>();
    

    typedef typename itk::BinaryImageToLabelMapFilter<InternalImageType> BinaryImageToLabelMapFilterType;
    typename BinaryImageToLabelMapFilterType::Pointer binaryImageToLabelMapFilter = BinaryImageToLabelMapFilterType::New();
    binaryImageToLabelMapFilter->SetInput( inputImage );
    binaryImageToLabelMapFilter->FullyConnectedOn();
    binaryImageToLabelMapFilter->SetNumberOfThreads( maxThreads );
    binaryImageToLabelMapFilter->Update();

    typedef typename itk::LabelMapToLabelImageFilter<typename BinaryImageToLabelMapFilterType::OutputImageType, InternalImageType> LabelMapToLabelImageFilterType;
    typename LabelMapToLabelImageFilterType::Pointer labelMapToLabelImageFilter = LabelMapToLabelImageFilterType::New();
    labelMapToLabelImageFilter->SetInput(binaryImageToLabelMapFilter->GetOutput());
    labelMapToLabelImageFilter->SetReleaseDataFlag( releaseDataFlag );
    labelMapToLabelImageFilter->SetNumberOfThreads(maxThreads);
    labelMapToLabelImageFilter->Update();

    // set the output
	ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<InternalImageType>( labelMapToLabelImageFilter->GetOutput() );
    mOutputImages.append( outputImage );

    // update the process widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

} // namespace XPIWIT
