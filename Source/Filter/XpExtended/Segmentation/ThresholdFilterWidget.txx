/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkThresholdImageFilter.h"
#include "itkOtsuThresholdImageFilter.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
ThresholdFilterWidget<TInputImage>::ThresholdFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = "ThresholdFilter";
	this->mDescription = "Applys a global binary threshold on the input image either based on fixed thresholds or on Otsu's method.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
    this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "LowerThreshold", "0.03", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The lower threshold. Values lower than this are considered as background." );
    processObjectSettings->AddSetting( "UpperThreshold", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The upper threshold. Values larger than this are considered as background." );
    processObjectSettings->AddSetting( "OutsideValue", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The outside value, i.e., the background intensity." );
    processObjectSettings->AddSetting( "UseOtsu", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If true, the fixed threshold is ignored and Otsu's method is used instead." );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
ThresholdFilterWidget<TInputImage>::~ThresholdFilterWidget()
{
}


// the update function
template< class TInputImage>
void ThresholdFilterWidget<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    bool useOtsu = processObjectSettings->GetSettingValue( "UseOtsu" ).toInt() > 0;
    const float lowerThreshold = processObjectSettings->GetSettingValue( "LowerThreshold" ).toDouble();

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	typename TInputImage::Pointer outputImage;

    // typedefs for the threshold filters
    typedef itk::ThresholdImageFilter<TInputImage> ThresholdFilter;
    typedef itk::OtsuThresholdImageFilter<TInputImage, TInputImage> OtsuThresholdFilter;

    if (useOtsu == false && lowerThreshold >= 0)
    {
        // adjust filter settings
        typename ThresholdFilter::Pointer filter = ThresholdFilter::New();
        filter->SetInput( inputImage );
        filter->SetLower( processObjectSettings->GetSettingValue( "LowerThreshold" ).toDouble() );
        filter->SetUpper( processObjectSettings->GetSettingValue( "UpperThreshold" ).toDouble() );
        filter->SetOutsideValue( processObjectSettings->GetSettingValue( "OutsideValue" ).toDouble() );
        filter->SetReleaseDataFlag( true );
        filter->SetNumberOfThreads( maxThreads );

        itkTryCatch( filter->Update(), "Error: Updating ThresholdFilterWidget failed!" );
        outputImage = filter->GetOutput();
    }
    else if (useOtsu == false && lowerThreshold < 0)
    {
        // extract the mean of the input image which is used as seed threshold if -1 is specified
        outputImage = TInputImage::New();
        outputImage->SetRegions( inputImage->GetLargestPossibleRegion() );
        outputImage->Allocate();
        outputImage->SetSpacing( inputImage->GetSpacing() );
        outputImage->FillBuffer(0);

        // calculate histogram and scale to the selected quantiles
        ImageRegionConstIterator<TInputImage> inputIterator( inputImage, inputImage->GetLargestPossibleRegion() );
        ImageRegionIterator<TInputImage> outputIterator( outputImage, outputImage->GetLargestPossibleRegion() );
        inputIterator.GoToBegin();
        unsigned int numPixels = (inputImage->GetLargestPossibleRegion().GetSize()[0]*
                                  inputImage->GetLargestPossibleRegion().GetSize()[1]*
                                  inputImage->GetLargestPossibleRegion().GetSize()[2]);
        double sumOfIntensities = 0.0;

        // fill the histogram
        while (inputIterator.IsAtEnd() == false)
        {
            sumOfIntensities += inputIterator.Get();
            ++inputIterator;
        }

        double threshold = sumOfIntensities / (float)numPixels;

        // fill the histogram
        inputIterator.GoToBegin();
        outputIterator.GoToBegin();
        while (inputIterator.IsAtEnd() == false)
        {
            if (inputIterator.Get() >= threshold)
                outputIterator.Set( inputIterator.Get() );

            ++inputIterator;
            ++outputIterator;
        }

        // show new calculated threshold
        Logger::GetInstance()->WriteLine( "+ Using mean based threshold, which is set to: " + QString::number(threshold) );
    }
    else
    {
        typedef itk::OtsuThresholdImageFilter<TInputImage, TInputImage> OtsuThresholdImageFilterType;
        typename OtsuThresholdImageFilterType::Pointer otsuThreshold = OtsuThresholdImageFilterType::New();
        otsuThreshold->SetReleaseDataFlag( false );
        otsuThreshold->SetInput( inputImage );
        otsuThreshold->SetInsideValue( 0 ); //1
        otsuThreshold->SetOutsideValue( 1 ); //0

        itkTryCatch( otsuThreshold->Update(), "Error: Update OtsuTheshold in ThresholdImageFilter failed!" );
        outputImage = otsuThreshold->GetOutput();

		typename TInputImage::PixelType threshold = otsuThreshold->GetThreshold();
    }

	ImageWrapper *outputWrapper = new ImageWrapper();
    outputWrapper->SetImage<TInputImage>( outputImage );
    mOutputImages.append( outputWrapper );

    // update the process widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

} // namespace XPIWIT
