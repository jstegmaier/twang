/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// namespace header
#include "../../Base/Management/ImageWrapper.h"
#include "CastImageFilterWidget.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkImageSeriesReader.h"
#include "itkNumericSeriesFileNames.h"
#include "itkRescaleIntensityImageFilter.h"
#include "itkImageRegionIterator.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkIntensityWindowingImageFilter.h"
#include "itkMultiplyImageFilter.h"

// qt header
#include <QtCore/QUrl>

// system header
#include <math.h>

using namespace XPIWIT;


namespace XPIWIT
{

// the default constructor
template <class TImageType>
ImageReaderWidget<TImageType>::ImageReaderWidget() : ProcessObjectBase()
{
    // set type
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_READER);
	this->mObjectType->SetNumberTypes(1);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);
    this->mName = "ImageReader";
    this->mDescription = "Reads image from disk";

    // get process object settings pointer
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // add settings
    processObjectSettings->AddSetting( "UseSeriesReader", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Combine separate 2D images to one 3D stack" );
    processObjectSettings->AddSetting( "SeriesMinIndex", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Start index for the series reader" );
    processObjectSettings->AddSetting( "SeriesMaxIndex", "499", ProcessObjectSetting::SETTINGVALUETYPE_INT, "End index for the series reader" );
    processObjectSettings->AddSetting( "SpacingX", "1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Original spacing in the first dimension" );
    processObjectSettings->AddSetting( "SpacingY", "1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Original spacing in the second dimension"  );
    processObjectSettings->AddSetting( "SpacingZ", "1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Original spacing in the third dimension"  );
    processObjectSettings->AddSetting( "FuseRotations", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN );
    processObjectSettings->AddSetting( "FuseTranslationX", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE );
    processObjectSettings->AddSetting( "FuseTranslationY", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE );
    processObjectSettings->AddSetting( "FuseTranslationZ", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE );
	processObjectSettings->AddSetting( "InputMaximumValue", "65535", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template <class TImageType>
ImageReaderWidget<TImageType>::~ImageReaderWidget()
{
}



// update the reader
template <class TImageType>
void ImageReaderWidget<TImageType>::Update()
{
    // start timer and get the process object settings object
    ProcessObjectBase::StartTimer();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get the widget settings
    float spacingParameter[3];
    const int maxThreads		= processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    spacingParameter[0]			= processObjectSettings->GetSettingValue( "SpacingX" ).toFloat();
    spacingParameter[1]			= processObjectSettings->GetSettingValue( "SpacingY" ).toFloat();
    spacingParameter[2]			= processObjectSettings->GetSettingValue( "SpacingZ" ).toFloat();
	const float inputMaximumValue = processObjectSettings->GetSettingValue( "InputMaximumValue" ).toFloat();

    // declare the reader
	typedef itk::Image<float, TImageType::ImageDimension> TReaderType;
	typename TReaderType::Pointer tempImage;
    typename ImageFileReader<TReaderType>::Pointer fileReader;
	
	// setup the reader
    fileReader = ImageFileReader<TReaderType>::New();
	fileReader->SetFileName( this->mPath.toStdString() );
    fileReader->SetReleaseDataFlag( true );
	fileReader->SetNumberOfThreads( maxThreads );
	itkTryCatch( fileReader->Update(), "Exception Caught: ImageReaderWidget -> Update function of the reader." );

	// perform a windowing of the value range to match the 
	typedef itk::IntensityWindowingImageFilter<TReaderType, TReaderType> IntensityWindowingImageFilterType;
	typename IntensityWindowingImageFilterType::Pointer intensityWindowingFilter = IntensityWindowingImageFilterType::New();
	intensityWindowingFilter->SetInput( fileReader->GetOutput() );
	intensityWindowingFilter->SetWindowMinimum( 0 );
	intensityWindowingFilter->SetWindowMaximum( inputMaximumValue );

    // set input range
    if( typeid( typename TImageType::PixelType ) == typeid( float ) || typeid( typename TImageType::PixelType ) == typeid( double ) )
	{
        // floating point types
        intensityWindowingFilter->SetOutputMinimum( itk::NumericTraits< typename TImageType::PixelType >::ZeroValue() );
        intensityWindowingFilter->SetOutputMaximum( itk::NumericTraits< typename TImageType::PixelType >::OneValue() );
    }
	else
	{
        // integer types
        intensityWindowingFilter->SetOutputMinimum( itk::NumericTraits< typename TImageType::PixelType >::min() );
        intensityWindowingFilter->SetOutputMaximum( itk::NumericTraits< typename TImageType::PixelType >::max() );
    }

	// update the intensity conversion
	itkTryCatch( intensityWindowingFilter->Update(), "Exception Caught: ImageReaderWidget -> Update function of the intensity conversion." );

	// cast image to internal data type
	ImageWrapper *imageWrapper = new ImageWrapper();
	imageWrapper->SetImage<TReaderType>( intensityWindowingFilter->GetOutput() );
	typename TImageType::Pointer resultImage = imageWrapper->GetImage<TImageType>();

    // set the spacing along x,y,z axes
    typename TImageType::SpacingType spacing;
    for( int i=0; i<TImageType::ImageDimension; i++ )
        spacing[i] = spacingParameter[i];
    resultImage->SetSpacing( spacingParameter );
	
    // store image in output list
    ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TImageType>( resultImage );
    mOutputImages.append( outputImage );

    // log the performance
    ProcessObjectBase::LogPerformance();

	// update the process object
    ProcessObjectBase::Update();
}

} // namespace XPIWIT
