/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// project header
#include "../../../Core/Utilities/Logger.h"

// qt header
#include <QFile>


namespace XPIWIT
{

// the default constructor
MetaReaderWidget::MetaReaderWidget() : ProcessObjectBase()
{
    // set type
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_READER);
	this->mObjectType->SetNumberTypes(0);
    this->mObjectType->SetNumberImageInputs(0);
    this->mObjectType->SetNumberImageOutputs(0);

    this->mObjectType->SetNumberMetaInputs(1);
	this->mObjectType->AppendMetaInputType("Variable");
    this->mObjectType->SetNumberMetaOutputs(1);
	this->mObjectType->AppendMetaOutputType("Variable");
    this->mName = "MetaReader";
	this->mDescription = "Reads csv from disk";

    // get process object settings pointer
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "Separator", ";", ProcessObjectSetting::SETTINGVALUETYPE_STRING, "Defines the used separator" );
	processObjectSettings->AddSetting( "Delimitor", ".", ProcessObjectSetting::SETTINGVALUETYPE_STRING, "Defines the used delimitor" );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
MetaReaderWidget::~MetaReaderWidget()
{
}


// update the reader
void MetaReaderWidget::Update()
{
    // start timer and get the process object settings object
    ProcessObjectBase::StartTimer();
	ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

	const QString separator = processObjectSettings->GetSettingValue("Separator");
    const QString delimitor = processObjectSettings->GetSettingValue("Delimitor");

	QFile csvFile( "" ); // this->mImagePath
	if( csvFile.open( QIODevice::ReadOnly | QIODevice::Text ) )
	{
		MetaDataFilter* metaOutput = this->mMetaOutputs.at(0);
		metaOutput->mIsMultiDimensional = false;							// one line per image

		// get postfix from file path
		QString postfix = "";	//this->mImagePath;
		postfix = postfix.split("_").last();	// get postfix
		postfix = postfix.split(".").first();	// delete file ending
		metaOutput->mPostfix = postfix;

		// read header line
		QString line = csvFile.readLine();
		line.replace("\n", "");
		metaOutput->mTitle = line.split( separator );

		// read data
		line = csvFile.readLine();
		line.replace("\n", "");

		QStringList currentLineStrings = line.split( separator );
		QList<float> currentLine;

		for (int i=0; i<currentLineStrings.length(); ++i)
			currentLine.append( currentLineStrings.at(i).toFloat() );

		metaOutput->mData.append( currentLine );
		
		while( !csvFile.atEnd() )
		{
			// if more than one switch MultiDimensional flag to true
			metaOutput->mIsMultiDimensional = true;		
			line = csvFile.readLine();
			line.replace("\n", "");

			currentLineStrings = line.split( separator );
			currentLine.clear();
		
			for (int i=0; i<currentLineStrings.length(); ++i)
				currentLine.append( currentLineStrings.at(i).toFloat() );

			metaOutput->mData.append( currentLine );
		}

		csvFile.close();
	}
	else
	{
		// error occured
	}

    // log the performance
    ProcessObjectBase::LogPerformance();
}

} // namespace XPIWIT
