/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// project header
#include "../../../Core/Utilities/Logger.h"
#include "../../Base/Management/ImageWrapper.h"

// itk header
#include "itkIntensityWindowingImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkCastImageFilter.h"
#include "itkNumericTraits.h"


// XPIWIT namespace
namespace XPIWIT {

// the default constructor
template< class TInputImage, class TOutputImage >
CastImageFilterWidget< TInputImage, TOutputImage >::CastImageFilterWidget() : ProcessObjectBase()
{
    // set the widget type
    this->mName = "CastImageFilter";
    this->mDescription = "Cast the input image to the output image and resacles the intensity.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(2);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(2);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage, class TOutputImage >
CastImageFilterWidget< TInputImage, TOutputImage >::~CastImageFilterWidget()
{
}


// the update function
template< class TInputImage, class TOutputImage >
void CastImageFilterWidget< TInputImage, TOutputImage >::Update()
{
    // nothing to do here
    if( typeid( typename TInputImage::PixelType ) == typeid( typename TOutputImage::PixelType ) ){
		mOutputImages.append( mInputImages.at(0) );
        return;
	}

	// start the timer
    ProcessObjectBase::StartTimer();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    Logger::GetInstance()->WriteLine( "- Cast image: (" + QString::fromStdString( typeid( typename TInputImage::PixelType ).name() ) + " -> " + QString::fromStdString( typeid( typename TOutputImage::PixelType ).name() ) + ")" );
    
    // get the parameter values
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const bool rescaleImage = mInputImages.at(0)->GetRescaleFlag();

	// get the input image
	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
	inputImage->SetReleaseDataFlag( true );

	// specify the maximum minimum calculator
	typename itk::MinimumMaximumImageCalculator<TInputImage>::Pointer minMaxInput = itk::MinimumMaximumImageCalculator<TInputImage>::New();
    minMaxInput->SetImage( inputImage );
    minMaxInput->Compute();

	// write intensity transform to the console
	Logger::GetInstance()->WriteToAll( QString("Rescale flag: ") + QString::number( rescaleImage ) );
	Logger::GetInstance()->WriteToAll( QString("Input - minimum: ") + QString::number( minMaxInput->GetMinimum() ) + QString(" maximum: ") + QString::number( minMaxInput->GetMaximum() ) );

    // TODO: cases for intensity and labled images
    // cast integral data types if max fits
    //if( typeid( TInputImage::PixelType  ) != typeid( float )  &&
    //    typeid( TInputImage::PixelType  ) != typeid( double ) &&
    //    typeid( TOutputImage::PixelType ) != typeid( float )  &&
    //    typeid( TOutputImage::PixelType ) != typeid( double ) ){

	if( !rescaleImage )
	{
		// calculate the minimum and maximum (TODO: why again here??)
		typename itk::MinimumMaximumImageCalculator<TInputImage>::Pointer minMaxCalc = itk::MinimumMaximumImageCalculator<TInputImage>::New();
        minMaxCalc->SetImage( inputImage );
        minMaxCalc->Compute();
        typename TInputImage::PixelType minimum = minMaxCalc->GetMinimum();
        typename TInputImage::PixelType maximum = minMaxCalc->GetMaximum();

        // is it possible to cast without data loss
        if( itk::NumericTraits< typename TOutputImage::PixelType >::min() <= minimum &&
            itk::NumericTraits< typename TOutputImage::PixelType >::max() >= maximum )
		{
			Logger::GetInstance()->WriteLine( "- Possible Data Loss by the Type Cast!");
		}

		// write cast ranges to log
		Logger::GetInstance()->WriteLine( "- Cast image Without Rescale: [" + QString::number( minimum ) + ", " + QString::number( maximum ) + "] -> [" + QString::number( minimum ) + ", " + QString::number( maximum ) + "]");

		// perform the cast
        typename itk::CastImageFilter<TInputImage, TOutputImage>::Pointer castFilter = itk::CastImageFilter<TInputImage, TOutputImage>::New();
        castFilter->SetInput( inputImage );
		castFilter->SetReleaseDataFlag( true );

        try
        {
            castFilter->Update();
            ImageWrapper *outputImage = new ImageWrapper();
			outputImage->SetRescaleFlag( rescaleImage );
            outputImage->SetImage<TOutputImage>( castFilter->GetOutput() );
            mOutputImages.append( outputImage );

            // log the performance
            ProcessObjectBase::LogPerformance();
            return;
        }
        catch( itk::ExceptionObject & err )
        {
            Logger::GetInstance()->WriteLine( "ExceptionObject caught !" );
            Logger::GetInstance()->WriteException( err );
            return;
        }
    }

    // rescale the intensity to 16Bit with minimum of 0 and maximum of 65535
    typename itk::IntensityWindowingImageFilter<TInputImage, TOutputImage>::Pointer rescaleIntensity = itk::IntensityWindowingImageFilter<TInputImage, TOutputImage>::New();
    rescaleIntensity->SetInput( inputImage );
	rescaleIntensity->SetReleaseDataFlag( true );

    // set input range
    if( typeid( typename TInputImage::PixelType ) == typeid( float ) ||
            typeid( typename TInputImage::PixelType ) == typeid( double ) )
	{
        // floating point types
        rescaleIntensity->SetWindowMinimum( itk::NumericTraits< typename TInputImage::PixelType >::ZeroValue() );
        rescaleIntensity->SetWindowMaximum( itk::NumericTraits< typename TInputImage::PixelType >::OneValue() );
    }
	else
	{
        // integer types
        rescaleIntensity->SetWindowMinimum( itk::NumericTraits< typename TInputImage::PixelType >::min() );
        rescaleIntensity->SetWindowMaximum( itk::NumericTraits< typename TInputImage::PixelType >::max() );
    }

    // set output range
    if( typeid( typename TOutputImage::PixelType ) == typeid( float ) ||
            typeid( typename TOutputImage::PixelType ) == typeid( double ) )
	{
        // floating point types
        rescaleIntensity->SetOutputMinimum( itk::NumericTraits< typename TOutputImage::PixelType >::ZeroValue() );
        rescaleIntensity->SetOutputMaximum( itk::NumericTraits< typename TOutputImage::PixelType >::OneValue() );
    }
	else
	{
        // integer types
        rescaleIntensity->SetOutputMinimum( itk::NumericTraits< typename TOutputImage::PixelType >::min() );
        rescaleIntensity->SetOutputMaximum( itk::NumericTraits< typename TOutputImage::PixelType >::max() );
    }

    try
    {
		Logger::GetInstance()->WriteLine( "- Windowing image: [" + QString::number( rescaleIntensity->GetWindowMinimum() ) + ", " + QString::number( rescaleIntensity->GetWindowMaximum() ) + "] -> [" + QString::number( rescaleIntensity->GetOutputMinimum() ) + ", " + QString::number( rescaleIntensity->GetOutputMaximum() ) + "]");
        rescaleIntensity->Update();

        ImageWrapper* outputImage = new ImageWrapper();
        outputImage->SetImage<TOutputImage>( rescaleIntensity->GetOutput() );
        mOutputImages.append( outputImage );
    }
    catch( itk::ExceptionObject & err )
    {
        Logger::GetInstance()->WriteLine( "ExceptionObject caught !" );
        Logger::GetInstance()->WriteException( err );
        return;
    }

    // update the process widget
    //ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance( false );
}

} // namespace XPIWIT
