/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

#ifndef __XPIWIT_IMAGEREADERWIDGET_H
#define __XPIWIT_IMAGEREADERWIDGET_H

// namespace header
#include "../../Base/Management/ProcessObjectBase.h"

// itk header
#include "itkImageFileReader.h"
#include "itkExtractImageFilter.h"

// qt header
#include <QtCore/QStringList>

using namespace itk;


namespace XPIWIT
{

/**
 *	@class ImageReaderWidget
 *	The base class for image to image filter widgets.
 */
template <class TInputImage>
class ImageReaderWidget : public ProcessObjectBase
{
    public:

        /**
         * The constructor.
         */
        ImageReaderWidget();

        /**
         * The destructor.
         */
        virtual ~ImageReaderWidget();

        /**
         * Updates the reader process object
         */
        void Update();

};

} // namespace XPIWIT

#include "ImageReaderWidget.txx"

#endif
