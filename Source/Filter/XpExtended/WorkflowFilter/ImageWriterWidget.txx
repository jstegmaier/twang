/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// namespace header
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkImageFileWriter.h"


namespace XPIWIT
{

// the default constructor
ImageWriterWidget::ImageWriterWidget() : ProcessObjectBase()
{
    // set type
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_WRITER);
	this->mObjectType->SetNumberTypes(0);
    this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);

    this->mObjectType->SetNumberImageOutputs(0);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);
    this->mName = "ImageWriter";
    this->mDescription = "Writes image to disk";

    // get process object settings pointer
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // add settings
    processObjectSettings->AddSetting( "Precision", "16", ProcessObjectSetting::SETTINGVALUETYPE_INT, "set precision in bit (8/16 Bit)" );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
ImageWriterWidget::~ImageWriterWidget()
{
}


// update the reader
void ImageWriterWidget::Update()
{
    // start timer and get the process object settings object
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get the widget settings
    const int maxThreads		= processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
    const int precision         = processObjectSettings->GetSettingValue( "Precision" ).toInt();

    ImageWrapper *imageWrapper = this->mInputImages.at(0);
    QPair< ProcessObjectType::DataType, int > imageType = imageWrapper->GetImageType();

    if( imageType.second == 2 ){
        if( precision == 16 )
            WriteImage< Image2UShort >();
        else
            WriteImage< Image2UChar >();
    }

    if( imageType.second == 3 ){
        if( precision == 16 )
            WriteImage< Image3UShort >();
        else
            WriteImage< Image3UChar >();
    }

    // log the performance
    ProcessObjectBase::LogPerformance();
}


// function to write the image
template <class TImageType>
void ImageWriterWidget::WriteImage()
{
    ImageWrapper *imageWrapper = this->mInputImages.at(0);

    typename TImageType::Pointer image = imageWrapper->GetImage<TImageType>();
    ProcessObjectBase::StartTimer();


    typename itk::ImageFileWriter< TImageType >::Pointer writer = itk::ImageFileWriter< TImageType >::New();
    writer->SetReleaseDataFlag( false );
	writer->SetFileName( this->mPath.toStdString() );
    writer->SetInput( image );
    try
    {
        writer->Update();
    }
    catch( itk::ExceptionObject & err )
    {
        Logger::GetInstance()->WriteLine( "ExceptionObject caught !" );
        Logger::GetInstance()->WriteException( err );
        return;
    }
}

} // namespace XPIWIT
