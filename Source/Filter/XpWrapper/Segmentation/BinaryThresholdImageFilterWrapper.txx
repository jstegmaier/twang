/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkBinaryThresholdImageFilter.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
BinaryThresholdImageFilterWrapper<TInputImage>::BinaryThresholdImageFilterWrapper() : ProcessObjectBase()
{
    // set the widget type
    this->mName = "BinaryThresholdImageFilter";
    this->mDescription = "Creates a binary image ";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
    this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "LowerThreshold", "0.03", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Defines the lower threshold" );
    processObjectSettings->AddSetting( "UpperThreshold", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Defines the upper threshold" );
    processObjectSettings->AddSetting( "OutsideValue", "0.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Defines the value outside of the thresholds" );
    processObjectSettings->AddSetting( "InsideValue", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Defines the value within the thresholds" );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
BinaryThresholdImageFilterWrapper<TInputImage>::~BinaryThresholdImageFilterWrapper()
{
}


// the update function
template< class TInputImage >
void BinaryThresholdImageFilterWrapper<TInputImage>::Update()
{
    // start timer and get the process object settings pointer
    ProcessObjectBase::StartTimer();
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();

	typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();

    // adjust filter settings
    typedef itk::BinaryThresholdImageFilter<TInputImage, TInputImage> BinaryThresholdFilter;
    typename BinaryThresholdFilter::Pointer filter = BinaryThresholdFilter::New();
    filter->SetInput( inputImage );
    filter->SetNumberOfThreads( maxThreads );
    filter->SetLowerThreshold( processObjectSettings->GetSettingValue( "LowerThreshold" ).toDouble() );
    filter->SetUpperThreshold( processObjectSettings->GetSettingValue( "UpperThreshold" ).toDouble() );
    filter->SetOutsideValue( processObjectSettings->GetSettingValue( "OutsideValue" ).toDouble() );
    filter->SetInsideValue( processObjectSettings->GetSettingValue( "InsideValue" ).toDouble() );

    // update the filter
    itkTryCatch( filter->Update(), "Error: Updating BinaryThresholdImageFilter failed." );
    
	ImageWrapper *outputImage = new ImageWrapper();
    outputImage->SetImage<TInputImage>( filter->GetOutput() );
	//outputImage->SetRescaleFlag( false );
    mOutputImages.append( outputImage );

    // update the process widget
    ProcessObjectBase::Update();

    // log the performance
    ProcessObjectBase::LogPerformance();
}

} // namespace XPIWIT
