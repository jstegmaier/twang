/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// namespace header
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkMorphologicalWatershedFromMarkersImageFilter.h"


namespace XPIWIT
{

// the default constructor
template < class TImageType >
MorphologicalWatershedFromMarkersImageWrapper< TImageType >::MorphologicalWatershedFromMarkersImageWrapper() : ProcessObjectBase()
{
	this->mName = "MorphologicalWatershedFromMarkersImageFilter";
	this->mDescription = "Calculates the watershed transform of the input image based on a marker image.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(2);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "MarkWatershedLine", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled, watershed lines are highlighted by zero values." );
	processObjectSettings->AddSetting( "FullyConnected", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If enabled 8-neighborhood (2D) or 27-neighborhood (3D) is used." );

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TImageType >
MorphologicalWatershedFromMarkersImageWrapper< TImageType >::~MorphologicalWatershedFromMarkersImageWrapper()
{
}


// the update function
template < class TImageType >
void MorphologicalWatershedFromMarkersImageWrapper< TImageType >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const int markWatershedLine = processObjectSettings->GetSettingValue( "MarkWatershedLine" ).toInt() > 0;
	const int fullyConnected = processObjectSettings->GetSettingValue( "FullyConnected" ).toInt() > 0;
	
	// get input image and allocate the marker image
	typedef TImageType LabelImageType;
	typename TImageType::Pointer inputImage = mInputImages.at(0)->template GetImage<TImageType>();
	typename TImageType::SpacingType spacing = inputImage->GetSpacing();

	typename LabelImageType::Pointer markerImage = mInputImages.at(1)->template GetImage<TImageType>();
/*	markerImage->SetRegions( inputImage->GetLargestPossibleRegion() );
	markerImage->SetSpacing( spacing );
	markerImage->Allocate();
	markerImage->FillBuffer( 0 );

	// get the number of seed points
	MetaDataFilter* seedPoints = this->mMetaInputs.at(0);
	unsigned int numSeedPoints = seedPoints->mData.length();
	
    for(int i=0; i<numSeedPoints; ++i)
	{
        QList<float> line = seedPoints->mData.at(i);

        typename TImageType::IndexType index;
        for ( int j = 0; j < TImageType::ImageDimension; ++j )
            index[j] = int( 0.5+line.at(j+2)/spacing[j] );

        markerImage->SetPixel( index, i+1 );
    }

    // print the spacing of the investigated image
    Logger::GetInstance()->WriteLine( "+ Seed locations were transformed to image space with the following spacing: [" +
        QString::number(spacing[0]) + ", " +  QString::number(spacing[1]) + ", " +  QString::number(spacing[2]) + "]" );
	*/

	// start processing
	ProcessObjectBase::StartTimer();
	
	// setup the filter
	typedef itk::MorphologicalWatershedFromMarkersImageFilter<TImageType, LabelImageType> WatershedFromMarkersFilterType;
	typename WatershedFromMarkersFilterType::Pointer watershedFromMarkersFilter = WatershedFromMarkersFilterType::New();
	watershedFromMarkersFilter->SetInput1( inputImage );
	watershedFromMarkersFilter->SetInput2( markerImage );
	watershedFromMarkersFilter->SetFullyConnected( fullyConnected );
	watershedFromMarkersFilter->SetMarkWatershedLine( markWatershedLine );
	watershedFromMarkersFilter->SetReleaseDataFlag( true );

	// update the filter
	itkTryCatch(watershedFromMarkersFilter->Update(), "Error: MorphologicalWatershedFromMarkersImageWrapper Update Function.");

	// set the output
	ImageWrapper *outputImage = new ImageWrapper();
	outputImage->SetImage<TImageType>( watershedFromMarkersFilter->GetOutput() );
	outputImage->SetRescaleFlag( true );
	mOutputImages.append( outputImage );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

} // namespace XPIWIT

