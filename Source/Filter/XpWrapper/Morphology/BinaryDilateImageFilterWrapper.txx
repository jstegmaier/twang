/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// namespace header
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkBinaryDilateImageFilter.h"
#include "itkBinaryBallStructuringElement.h"


namespace XPIWIT
{

// the default constructor
template < class TImageType1 >
BinaryDilateImageFilterWrapper< TImageType1 >::BinaryDilateImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = "BinaryDilateImageFilter";
	this->mDescription = "Binary dilation. Expand binary regions.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "KernelRadius", "1", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Radius of the kernel." );

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TImageType1 >
BinaryDilateImageFilterWrapper< TImageType1 >::~BinaryDilateImageFilterWrapper()
{
}


// the update function
template < class TImageType1 >
void BinaryDilateImageFilterWrapper< TImageType1 >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const int radius = processObjectSettings->GetSettingValue( "KernelRadius" ).toInt();
	
	// get images
	typename TImageType1::Pointer inputImage = mInputImages.at(0)->template GetImage<TImageType1>();
	
	// start processing
	ProcessObjectBase::StartTimer();
	
	// create kernel
	typedef itk::BinaryBallStructuringElement< typename TImageType1::PixelType, TImageType1::ImageDimension> StructuringElementType;
	StructuringElementType structuringElement;
	structuringElement.SetRadius( radius );
	structuringElement.CreateStructuringElement();

	// setup the filter
	typedef itk::BinaryDilateImageFilter<TImageType1, TImageType1, StructuringElementType> FilterType;
	typename FilterType::Pointer filter = FilterType::New();
	filter->SetInput( inputImage );
	filter->SetKernel( structuringElement );
	filter->SetForegroundValue(1);
	filter->SetBackgroundValue( 0 );
	filter->SetReleaseDataFlag( true );
	
	itkTryCatch(filter->Update(), "Error: BinaryDilateImageFilterWrapper Update Function.");
	
	ImageWrapper *outputImage = new ImageWrapper();
	outputImage->SetImage<TImageType1>( filter->GetOutput() );
	outputImage->SetRescaleFlag( false );
	mOutputImages.append( outputImage );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

} // namespace XPIWIT

