/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// project header
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkIntensityWindowingImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"
#include "itkNumericTraits.h"


namespace XPIWIT
{

// the default constructor
template < class TImageType1 >
RescaleIntensityImageFilterWrapper< TImageType1 >::RescaleIntensityImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = "RescaleIntensityImageFilter";
	this->mDescription = "Rescales the image from min to max for integer types and from 0 to 1 for float types";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TImageType1 >
RescaleIntensityImageFilterWrapper< TImageType1 >::~RescaleIntensityImageFilterWrapper()
{
}


// the update function
template < class TImageType1 >
void RescaleIntensityImageFilterWrapper< TImageType1 >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	
	// get images
	typename TImageType1::Pointer inputImage = mInputImages.at(0)->template GetImage<TImageType1>();
	
	// start processing
	ProcessObjectBase::StartTimer();

	typedef itk::MinimumMaximumImageCalculator<TImageType1> MinMaxCalculatorType;
	typename MinMaxCalculatorType::Pointer minMaxCalc = MinMaxCalculatorType::New();
    minMaxCalc->SetImage( inputImage );
    minMaxCalc->Compute();

	typename TImageType1::PixelType minimum = minMaxCalc->GetMinimum();
    typename TImageType1::PixelType maximum = minMaxCalc->GetMaximum();

	typedef itk::IntensityWindowingImageFilter<TImageType1, TImageType1> IntensityFilterType;
	typename IntensityFilterType::Pointer intensityFilter = IntensityFilterType::New();
	intensityFilter->SetInput( inputImage );
	intensityFilter->SetReleaseDataFlag( true );
	intensityFilter->SetWindowMinimum( minimum );
    intensityFilter->SetWindowMaximum( maximum );

	if( typeid( typename TImageType1::PixelType ) == typeid( float ) ||
		typeid( typename TImageType1::PixelType ) == typeid( double ) )
	{
		// float type
		intensityFilter->SetOutputMinimum( itk::NumericTraits< typename TImageType1::PixelType >::ZeroValue() );
        intensityFilter->SetOutputMaximum( itk::NumericTraits< typename TImageType1::PixelType >::OneValue() );
	}
	else
	{
		// integral type
		intensityFilter->SetOutputMinimum( itk::NumericTraits< typename TImageType1::PixelType >::min() );
        intensityFilter->SetOutputMaximum( itk::NumericTraits< typename TImageType1::PixelType >::max() );
	}
	
	itkTryCatch(intensityFilter->Update(), "Error: RescaleIntensityImageFilterWrapper Update Function.");
	
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetImage<TImageType1>( intensityFilter->GetOutput() );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

} // namespace XPIWIT

