/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// project header
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkMultiplyImageFilter.h"


namespace XPIWIT
{

// the default constructor
template < class TImageType1 >
MultiplyImageFilterWrapper< TImageType1 >::MultiplyImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = "MultiplyImageFilter";
	this->mDescription = "Pixel-wise multiplication of two images.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(2);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "Constant", "0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "If non-zero, this is used instead of the second input image for multiplication." );

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TImageType1 >
MultiplyImageFilterWrapper< TImageType1 >::~MultiplyImageFilterWrapper()
{
}


// the update function
template < class TImageType1 >
void MultiplyImageFilterWrapper< TImageType1 >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const float constant = processObjectSettings->GetSettingValue( "Constant" ).toFloat();
	
	// get images
	typename TImageType1::Pointer inputImage1 = mInputImages.at(0)->template GetImage<TImageType1>();
	typename TImageType1::Pointer inputImage2 = mInputImages.at(1)->template GetImage<TImageType1>();
	
	// start processing
	ProcessObjectBase::StartTimer();
	
	// setup the filter
	typedef itk::MultiplyImageFilter<TImageType1, TImageType1> FilterType;
	typename FilterType::Pointer filter = FilterType::New();

	if (constant == 0)
	{
		filter->SetInput1( inputImage1 );
		filter->SetInput2( inputImage2 );
		filter->SetReleaseDataFlag( true );
	}
	else
	{
		filter->SetInput1( inputImage1 );
		filter->SetConstant( constant );
		filter->SetReleaseDataFlag( true );
	}

	itkTryCatch(filter->Update(), "Error: MultiplyImageFilterWrapper Update Function.");
	
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetImage<TImageType1>( filter->GetOutput() );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

} // namespace XPIWIT

