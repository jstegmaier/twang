/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// project header
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkInvertIntensityImageFilter.h"
#include "itkMinimumMaximumImageCalculator.h"


namespace XPIWIT
{

// the default constructor
template < class TImageType1 >
InvertIntensityImageFilterWrapper< TImageType1 >::InvertIntensityImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = "InvertIntensityImageFilter";
	this->mDescription = "Invert the intensity of an image.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "Maximum", "-1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Maximum, -1 for automatic" );

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TImageType1 >
InvertIntensityImageFilterWrapper< TImageType1 >::~InvertIntensityImageFilterWrapper()
{
}


// the update function
template < class TImageType1 >
void InvertIntensityImageFilterWrapper< TImageType1 >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	float maximum = processObjectSettings->GetSettingValue( "Maximum" ).toDouble();
	
	// get images
	typename TImageType1::Pointer inputImage = mInputImages.at(0)->template GetImage<TImageType1>();
	
	// start processing
	ProcessObjectBase::StartTimer();
	
	if( maximum < 0 )
	{
		if( typeid( typename TImageType1::PixelType ) == typeid( float ) ||
		typeid( typename TImageType1::PixelType ) == typeid( double ) )
		{
			maximum = 1;
		}
		else
		{
			maximum = itk::NumericTraits< typename TImageType1::PixelType >::max();
		}
	}

	// setup the filter
	typedef itk::InvertIntensityImageFilter<TImageType1, TImageType1> FilterType;
	typename FilterType::Pointer filter = FilterType::New();
	filter->SetInput( inputImage );
	filter->SetMaximum( maximum );
	filter->SetReleaseDataFlag( true );
	
	itkTryCatch(filter->Update(), "Error: InvertIntensityImageFilterWrapper Update Function.");
	
	ImageWrapper *outputWrapper = new ImageWrapper();
	outputWrapper->SetImage<TImageType1>( filter->GetOutput() );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

} // namespace XPIWIT

