/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// namespace header
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkHessianToObjectnessMeasureImageFilter.h"
#include "itkHessianRecursiveGaussianImageFilter.h"


namespace XPIWIT
{
	
// the default constructor
template < class TImageType >
HessianToObjectnessMeasureImageFilterWrapper< TImageType >::HessianToObjectnessMeasureImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = "HessianToObjectnessMeasureImageFilter";
	this->mDescription = "Uses the hessian eigenvalues to enhance specific structures in the image.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "Sigma", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Variance used by the Hessian calculation." );
	processObjectSettings->AddSetting( "Alpha", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Alpha parameter for the objectness filter." );
	processObjectSettings->AddSetting( "Beta", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Beta parameter for the objectness filter." );
	processObjectSettings->AddSetting( "Gamma", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Gamma parameter for the objectness filter." );
	processObjectSettings->AddSetting( "ScaleObjectnessMeasure", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Scales the object measure if set on." );
	processObjectSettings->AddSetting( "ObjectDimension", "0", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Set the dimension of the objects about to be emphasized." );
	processObjectSettings->AddSetting( "BrightObject", "0", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Gamma parameter for the objectness filter." );
	
	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TImageType >
HessianToObjectnessMeasureImageFilterWrapper< TImageType >::~HessianToObjectnessMeasureImageFilterWrapper()
{
}


// the update function
template < class TImageType >
void HessianToObjectnessMeasureImageFilterWrapper< TImageType >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const float sigma = processObjectSettings->GetSettingValue( "Sigma" ).toFloat();
	const float alpha = processObjectSettings->GetSettingValue( "Alpha" ).toFloat();
	const float beta = processObjectSettings->GetSettingValue( "Beta" ).toFloat();
	const float gamma = processObjectSettings->GetSettingValue( "Gamma" ).toFloat();
	const int objectDimension = processObjectSettings->GetSettingValue( "ObjectDimension" ).toInt();
	const bool scaleObjectnessMeasure = processObjectSettings->GetSettingValue( "ScaleObjectnessMeasure" ).toInt() > 0;
	const bool brightObject = processObjectSettings->GetSettingValue( "BrightObject" ).toInt() > 0;
	
	// get images
	typename TImageType::Pointer inputImage = mInputImages.at(0)->template GetImage<TImageType>();
		
	// start processing
	ProcessObjectBase::StartTimer();
	
	// setup the filter
	typedef itk::HessianRecursiveGaussianImageFilter<TImageType> HessianFilterType;
	typedef itk::HessianToObjectnessMeasureImageFilter< typename HessianFilterType::OutputImageType, TImageType> ObjectnessMeasureFilterType;

	typename HessianFilterType::Pointer hessianFilter = HessianFilterType::New();
	hessianFilter->SetInput( inputImage );
	hessianFilter->SetNumberOfThreads( maxThreads );
	hessianFilter->SetReleaseDataFlag( true );
	hessianFilter->SetSigma( sigma );
	
	typename ObjectnessMeasureFilterType::Pointer objectnessFilter = ObjectnessMeasureFilterType::New();
	objectnessFilter->SetInput( hessianFilter->GetOutput() );
	objectnessFilter->SetAlpha( alpha );
	objectnessFilter->SetBeta( beta );
	objectnessFilter->SetGamma( gamma );
	objectnessFilter->SetObjectDimension( objectDimension );
	objectnessFilter->SetBrightObject( brightObject );
	objectnessFilter->SetNumberOfThreads( maxThreads );
	objectnessFilter->SetScaleObjectnessMeasure( scaleObjectnessMeasure );
	itkTryCatch(objectnessFilter->Update(), "Error: HessianToObjectnessMeasureImageFilterWrapper Update Function.");
	
	ImageWrapper *outputImage = new ImageWrapper();
	outputImage->SetImage<TImageType>( objectnessFilter->GetOutput() );
	mOutputImages.append( outputImage );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

} // namespace XPIWIT

