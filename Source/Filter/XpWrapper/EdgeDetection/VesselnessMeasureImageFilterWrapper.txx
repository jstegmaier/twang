/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// namespace header
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkHessian3DToVesselnessMeasureImageFilter.h"
#include "itkHessianRecursiveGaussianImageFilter.h"


namespace XPIWIT
{

// the default constructor
template < class TImageType >
VesselnessMeasureImageFilterWrapper< TImageType >::VesselnessMeasureImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = "VesselnessMesasureImageFilter";
	this->mDescription = "Uses the hessian eigenvalues to enhance vessel like structures in the image.";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "Sigma", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Variance used by the Hessian calculation." );
	processObjectSettings->AddSetting( "Alpha1", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Alpha1 parameter for the vesselness filter." );
	processObjectSettings->AddSetting( "Alpha2", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Alpha2 parameter for the vesselness filter." );
	
	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TImageType >
VesselnessMeasureImageFilterWrapper< TImageType >::~VesselnessMeasureImageFilterWrapper()
{
}


// the update function
template < class TImageType >
void VesselnessMeasureImageFilterWrapper< TImageType >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const float sigma = processObjectSettings->GetSettingValue( "Sigma" ).toInt();
	const float alpha1 = processObjectSettings->GetSettingValue( "Alpha1" ).toInt();
	const float alpha2 = processObjectSettings->GetSettingValue( "Alpha2" ).toInt();
	
	// get images
	typename TImageType::Pointer inputImage = mInputImages.at(0)->template GetImage<TImageType>();
		
	// start processing
	ProcessObjectBase::StartTimer();
	
	// setup the filter
	typedef itk::HessianRecursiveGaussianImageFilter<TImageType> HessianFilterType;
	typedef itk::Hessian3DToVesselnessMeasureImageFilter<float> VesselnessMeasureFilterType;

	typename HessianFilterType::Pointer hessianFilter = HessianFilterType::New();
	hessianFilter->SetInput( inputImage );
	hessianFilter->SetReleaseDataFlag( true );
	hessianFilter->SetSigma( sigma );
	
	typename VesselnessMeasureFilterType::Pointer vesselnessFilter = VesselnessMeasureFilterType::New();
	vesselnessFilter->SetInput( hessianFilter->GetOutput() );
	vesselnessFilter->SetAlpha1( alpha1 );
	vesselnessFilter->SetAlpha2( alpha2 );
	itkTryCatch(vesselnessFilter->Update(), "Error: VesselnessMeasureImageFilterWrapper Update Function.");
	
	ImageWrapper *outputImage = new ImageWrapper();
	outputImage->SetImage<TImageType>( vesselnessFilter->GetOutput() );
	mOutputImages.append( outputImage );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

} // namespace XPIWIT

