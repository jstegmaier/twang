/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// namespace header
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkBinaryImageToLabelMapFilter.h"
#include "itkLabelMapToLabelImageFilter.h"


namespace XPIWIT
{

// the default constructor
template < class TImageType1 >
BinaryImageToLabelImageWrapper< TImageType1 >::BinaryImageToLabelImageWrapper() : ProcessObjectBase()
{
	this->mName = "BinaryImageToLabelImage";
	this->mDescription = "Transforms binary image to a label map and creates the label image";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(0);

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	processObjectSettings->AddSetting( "FullyConnected", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "If turned on, 8/26 neighborhood is used, else 4/6 neighborhood is used for 2D/3D images, respectively." );
	processObjectSettings->AddSetting( "InputForegroundValue", "1", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The input foreground value. Usually this should be set to 1." );
	processObjectSettings->AddSetting( "OutputBackgroundValue", "0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "The output background value. Usually this should be set to 0." );

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TImageType1 >
BinaryImageToLabelImageWrapper< TImageType1 >::~BinaryImageToLabelImageWrapper()
{
}


// the update function
template < class TImageType1 >
void BinaryImageToLabelImageWrapper< TImageType1 >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const bool fullyConnected = processObjectSettings->GetSettingValue( "FullyConnected" ).toInt() > 0;
	const float foregroundValue = processObjectSettings->GetSettingValue( "InputForegroundValue" ).toFloat();
	const float backgroundValue = processObjectSettings->GetSettingValue( "OutputBackgroundValue" ).toFloat();
	
	// get images
	typename TImageType1::Pointer inputImage = mInputImages.at(0)->template GetImage<TImageType1>();
	
	// start processing
	ProcessObjectBase::StartTimer();
	
	// setup the filter
	typedef itk::BinaryImageToLabelMapFilter< TImageType1 > BinaryImageToLabelMapFilterType;
	typename BinaryImageToLabelMapFilterType::Pointer binaryImageToLabelMapFilter = BinaryImageToLabelMapFilterType::New();
	binaryImageToLabelMapFilter->SetInput( inputImage );
	binaryImageToLabelMapFilter->SetReleaseDataFlag( true );
	binaryImageToLabelMapFilter->SetInputForegroundValue( foregroundValue );
	binaryImageToLabelMapFilter->SetOutputBackgroundValue( backgroundValue );
	binaryImageToLabelMapFilter->SetFullyConnected( fullyConnected );
	itkTryCatch( binaryImageToLabelMapFilter->Update(), "Error: BinaryImageToLabelMapFilter update.");
 
	// convert the label mapt into a label image
	typedef itk::Image< unsigned short, TImageType1::ImageDimension >  LabelType;
	typedef itk::LabelMapToLabelImageFilter< typename BinaryImageToLabelMapFilterType::OutputImageType, LabelType > LabelMapToLabelImageFilterType;
	typename LabelMapToLabelImageFilterType::Pointer labelMapToLabelImageFilter = LabelMapToLabelImageFilterType::New();
	labelMapToLabelImageFilter->SetInput( binaryImageToLabelMapFilter->GetOutput() );
	labelMapToLabelImageFilter->SetReleaseDataFlag( true );
	itkTryCatch( labelMapToLabelImageFilter->Update(), "Error: LabelMapToLabelImageFilter update.");
	
	// set the output
	ImageWrapper* outputWrapper = new ImageWrapper();
	outputWrapper->SetImage< LabelType >( labelMapToLabelImageFilter->GetOutput() );
	outputWrapper->SetRescaleFlag( false );
	mOutputImages.append( outputWrapper );
	
	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

} // namespace XPIWIT

