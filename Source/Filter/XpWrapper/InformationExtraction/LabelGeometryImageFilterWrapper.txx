/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// namespace header
#include "../../Base/Management/ImageWrapper.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkLabelMapToLabelImageFilter.h"
#include "itkLabelGeometryImageFilter.h"

using namespace itk;


namespace XPIWIT
{

// the default constructor
template < class TImageType1 >
LabelGeometryImageFilterWrapper< TImageType1 >::LabelGeometryImageFilterWrapper() : ProcessObjectBase()
{
	this->mName = "LabelGeometryImageFilter";
	this->mDescription = "Extract geometry information of labeled image";

	// set the filter type and I/O settings
	this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
	this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(0);
	this->mObjectType->SetNumberMetaInputs(0);
	this->mObjectType->SetNumberMetaOutputs(1);
	this->mObjectType->AppendMetaOutputType("GeometryProperties");

	// add settings (types: double, int, string, bool)
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

	// initialize the widget
	ProcessObjectBase::Init();
}


// the destructor
template < class TImageType1 >
LabelGeometryImageFilterWrapper< TImageType1 >::~LabelGeometryImageFilterWrapper()
{
}


// the update function
template < class TImageType1 >
void LabelGeometryImageFilterWrapper< TImageType1 >::Update()
{
	ProcessObjectBase::PrepareInputs();
	ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
	
	// get parameters
	const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	
	// get images
	typedef itk::Image< unsigned short, TImageType1::ImageDimension >  LabelType;
	typename LabelType::Pointer inputImage = mInputImages.at(0)->template GetImage<LabelType>();
	
	// start processing
	ProcessObjectBase::StartTimer();
	
	// setup the filter
	typedef itk::LabelGeometryImageFilter<LabelType, LabelType> LabelGeometryImageFilterType;
    typename LabelGeometryImageFilterType::Pointer labelGeometryFilter =  LabelGeometryImageFilterType::New();
	labelGeometryFilter->SetInput( inputImage );
    labelGeometryFilter->SetCalculateOrientedBoundingBox( true );
    labelGeometryFilter->SetCalculateOrientedIntensityRegions( false );
    labelGeometryFilter->SetCalculateOrientedLabelRegions( false );
    labelGeometryFilter->SetCalculatePixelIndices( false );
    labelGeometryFilter->SetReleaseDataFlag( true );
	
	itkTryCatch(labelGeometryFilter->Update(), "Error: LabelGeometryImageFilterWrapper Update Function.");

	// Create the meta data Object
	MetaDataFilter* metaOutput = this->mMetaOutputs.at(0);
    metaOutput->mIsMultiDimensional = true;
	metaOutput->mPostfix = "RegionProps";

	QStringList metaDescription;                    QStringList metaType;
    metaDescription << "id";						metaType << "int";
	metaDescription << "size";						metaType << "int";
	metaDescription << "xpos";						metaType << "int";	// centroid
	metaDescription << "ypos";						metaType << "int";
	if( TImageType1::ImageDimension >= 3 )
		metaDescription << "zpos";					metaType << "int";

	metaDescription << "bbOriginX";					metaType << "int";	// bounding box origin
	metaDescription << "bbOriginY";					metaType << "int";
	if( TImageType1::ImageDimension >= 3 )
		metaDescription << "bbOriginZ";				metaType << "int";
	metaDescription << "bbSizeX";					metaType << "int";	// bounding box size
	metaDescription << "bbSizeY";					metaType << "int";
	if( TImageType1::ImageDimension >= 3 )
		metaDescription << "bbSizeZ";				metaType << "int";

	metaDescription << "xweighted";					metaType << "int";	// weighted centroid
	metaDescription << "yweighted";					metaType << "int";
	if( TImageType1::ImageDimension >= 3 )
		metaDescription << "zweighted";				metaType << "int";
	metaDescription << "integrated intensity";		metaType << "int";
	//metaDescription << "axes length";				metaType << "int";
	metaDescription << "major axis length";			metaType << "int";
	metaDescription << "minor axis length";			metaType << "int";
	metaDescription << "eccentricity";				metaType << "float";
	metaDescription << "elongation";				metaType << "float";
	metaDescription << "orientation";				metaType << "float";

	// write to meta object
	metaOutput->mTitle = metaDescription;
	metaOutput->mType = metaType;

	// get the labels
    typename LabelGeometryImageFilterType::LabelsType allLabels = labelGeometryFilter->GetLabels();
    typename LabelGeometryImageFilterType::LabelsType::iterator allLabelsIt;
    unsigned int currentKeyPointID = 0;
    // iterate trough the labels and extract nucleus information
    for( allLabelsIt = allLabels.begin(); allLabelsIt != allLabels.end(); allLabelsIt++ )
    {
        ++currentKeyPointID;
        typename LabelGeometryImageFilterType::LabelPixelType labelValue = *allLabelsIt;

		QList<float> currentData;
		currentData << currentKeyPointID;											// id
		currentData << labelGeometryFilter->GetVolume(labelValue);					// size
		currentData << labelGeometryFilter->GetCentroid(labelValue)[0];				// xpos			// centroid
		currentData << labelGeometryFilter->GetCentroid(labelValue)[1];				// ypos
		if( TImageType1::ImageDimension >= 3 )
			currentData << labelGeometryFilter->GetCentroid(labelValue)[2];			// zpos
		
		// bounding box origin
		currentData << labelGeometryFilter->GetBoundingBox(labelValue)[0];			
		currentData << labelGeometryFilter->GetBoundingBox(labelValue)[2];
		if( TImageType1::ImageDimension >= 3 )
			currentData << labelGeometryFilter->GetBoundingBox(labelValue)[4];

		// bounding box size
		currentData << labelGeometryFilter->GetBoundingBoxSize(labelValue)[0];		
		currentData << labelGeometryFilter->GetBoundingBoxSize(labelValue)[1];
		if( TImageType1::ImageDimension >= 3 )
			currentData << labelGeometryFilter->GetBoundingBoxSize(labelValue)[2];

		currentData << labelGeometryFilter->GetWeightedCentroid(labelValue)[0];		// xweighted	// weighted centroid
		currentData << labelGeometryFilter->GetWeightedCentroid(labelValue)[1];		// yweighted
		if( TImageType1::ImageDimension >= 3 )
			currentData << labelGeometryFilter->GetWeightedCentroid(labelValue)[2];	// zweighted
		currentData << labelGeometryFilter->GetIntegratedIntensity(labelValue);		// integrated intensity
		// avarege: labelGeometryFilter->GetIntegratedIntensity(labelValue) / labelGeometryFilter->GetVolume(labelValue)
		//currentData << labelGeometryFilter->GetAxesLength(labelValue) );				// axes length
		currentData << labelGeometryFilter->GetMajorAxisLength(labelValue);			// major axis length
		currentData << labelGeometryFilter->GetMinorAxisLength(labelValue);			// minor axis length
		currentData << labelGeometryFilter->GetEccentricity(labelValue);				// eccentricity
		currentData << labelGeometryFilter->GetElongation(labelValue);				// elongation
		currentData << labelGeometryFilter->GetOrientation(labelValue);				// orientation

		metaOutput->mData.append( currentData );
    }

	// log performance and write results
	ProcessObjectBase::LogPerformance();
	ProcessObjectBase::Update();
}

} // namespace XPIWIT

