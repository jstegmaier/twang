/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// project header
#include "../../Base/Management/ImageWrapper.h"
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkDiscreteGaussianImageFilter.h"


namespace XPIWIT
{

// the default constructor
template< class TInputImage >
DiscreteGaussianImageFilterWrapper<TInputImage>::DiscreteGaussianImageFilterWrapper() : ProcessObjectBase()
{
    // set the filter name
    this->mName = "DiscreteGaussianImageFilter";
    this->mDescription = "Gaussian smothing filter. ";
    this->mDescription += "Filters the image with a gaussian kernel defined by variance.";

    // set the filter type and I/O settings
    this->mObjectType->SetFilterType(ProcessObjectType::FILTERTYPE_FILTER);
	this->mObjectType->SetNumberTypes(1);
	this->mObjectType->SetNumberImageInputs(1);
    this->mObjectType->AppendImageInputType(1);
	this->mObjectType->SetNumberImageOutputs(1);
	this->mObjectType->AppendImageOutputType(1);
    this->mObjectType->SetNumberMetaInputs(0);
    this->mObjectType->SetNumberMetaOutputs(0);

    // add settings
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;
    processObjectSettings->AddSetting( "Variance", "1.0", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Variance of the gaussian kernel." );
    processObjectSettings->AddSetting( "MaximumError", "0.01", ProcessObjectSetting::SETTINGVALUETYPE_DOUBLE, "Maximum error of the gaussian function approximation." );
    processObjectSettings->AddSetting( "MaximumKernelWidth", "32", ProcessObjectSetting::SETTINGVALUETYPE_INT, "Maximum kernel size in pixel." );
    processObjectSettings->AddSetting( "UseImageSpacing", "1", ProcessObjectSetting::SETTINGVALUETYPE_BOOLEAN, "Use the real spacing for the gaussian kernel creation."  );

    // initialize the widget
    ProcessObjectBase::Init();
}


// the destructor
template< class TInputImage >
DiscreteGaussianImageFilterWrapper<TInputImage>::~DiscreteGaussianImageFilterWrapper()
{
}


// the update function
template< class TInputImage >
void DiscreteGaussianImageFilterWrapper<TInputImage>::Update()
{
    ProcessObjectBase::PrepareInputs();
    ProcessObjectSettings* processObjectSettings = this->mProcessObjectSettings;

    // get parameters
    const int maxThreads = processObjectSettings->GetSettingValue( "MaxThreads" ).toInt();
	const float variance = processObjectSettings->GetSettingValue( "Variance" ).toDouble();

    // get images
    typename TInputImage::Pointer inputImage = mInputImages.at(0)->template GetImage<TInputImage>();
    ProcessObjectBase::StartTimer();

    // setup the gaussian filter
    typedef itk::DiscreteGaussianImageFilter<TInputImage, TInputImage> GaussianFilter;
    typename GaussianFilter::Pointer filter = GaussianFilter::New();
    filter->SetInput( inputImage );
    filter->SetReleaseDataFlag( true );
    filter->SetVariance( variance );
    filter->SetMaximumError( processObjectSettings->GetSettingValue( "MaximumError" ).toDouble() );
    filter->SetMaximumKernelWidth( processObjectSettings->GetSettingValue( "MaximumKernelWidth" ).toInt() );
    filter->SetUseImageSpacing( processObjectSettings->GetSettingValue( "UseImageSpacing" ).toInt() );
    filter->SetNumberOfThreads( maxThreads );

    itkTryCatch(filter->Update(), "Error: DiscreteGaussianImageFilterWrapper Update Function.");

    ImageWrapper *outputWrapper = new ImageWrapper();
    outputWrapper->SetImage<TInputImage>( filter->GetOutput() );
    mOutputImages.append( outputWrapper );

    // log performance and write results
    ProcessObjectBase::LogPerformance();
    ProcessObjectBase::Update();
}

} // namespace XPIWIT

