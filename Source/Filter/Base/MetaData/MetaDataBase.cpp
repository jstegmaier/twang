/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// file header
#include "MetaDataBase.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// qt header
#include <QtCore/QMutex>


namespace XPIWIT
{

MetaDataBase *MetaDataBase::mInstance = 0;

// returns the singleton instance
MetaDataBase *MetaDataBase::GetInstance()
{
    static QMutex mutex;
    if (!mInstance)
    {
        mutex.lock();

        if (!mInstance)
            mInstance = new MetaDataBase();

        mutex.unlock();
    }
    return mInstance;
}


// delete the instance
void MetaDataBase::DeleteInstance()
{
    static QMutex mutex;
    mutex.lock();

    delete mInstance;
    mInstance = 0;

    mutex.unlock();
}


// the default constructor
MetaDataBase::MetaDataBase()
{

}


// default constructor
MetaDataBase::~MetaDataBase()
{
    qDeleteAll( mMetaDataImages );
    mMetaDataImages.clear();
}


// function to set teh image
void MetaDataBase::SetImage(QString imageId, MetaDataImage *imageMeta)
{
    mImageIds.append( imageId );
    mMetaDataImages.append( imageMeta );
}


// function to retrieve the meta data
MetaDataImage *MetaDataBase::GetImageMeta(QString imageId)
{
    if( mImageIds.contains( imageId ) )
	{
        return mMetaDataImages.at( mImageIds.indexOf( imageId ) );
	}
	else
	{
		MetaDataImage *metaDataImage = new MetaDataImage();
		SetImage( imageId, metaDataImage );
		return metaDataImage;
	}
}


// function to retrieve the meta data by index
MetaDataImage *MetaDataBase::GetImageMeta(int index)
{
	if( mMetaDataImages.length() > index )
		return mMetaDataImages.at( index );
	return NULL;
}

}// XPIWIT namespace
