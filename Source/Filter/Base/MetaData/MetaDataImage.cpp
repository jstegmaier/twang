/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// file header
#include "MetaDataImage.h"


namespace XPIWIT
{

// default constructor
MetaDataImage::MetaDataImage()
{
	mFilterIds.clear();
	mMetaDataFilters.clear();
}


// default destructor
MetaDataImage::~MetaDataImage()
{
    qDeleteAll( mMetaDataFilters );
    mMetaDataFilters.clear();
}


// function to set the meta data
void MetaDataImage::SetMetaData(QString filterId, MetaDataFilter *metaDataFilter)
{
    mFilterIds.append( filterId );
    mMetaDataFilters.append( metaDataFilter );
}


// function to create the meta output object
MetaDataFilter *MetaDataImage::CreateMetaOutput(QString filterId)
{
    MetaDataFilter *temp = new MetaDataFilter();
    SetMetaData( filterId, temp );
    return temp;
}


// function to get the meta data
MetaDataFilter *MetaDataImage::GetMetaData(QString filterId)
{
    if( mFilterIds.contains( filterId ) )
        return mMetaDataFilters.at( mFilterIds.indexOf( filterId ) );

    return NULL;
}

}// XPIWIT namespace
