/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// file header
#include "MetaDataFilter.h"

// qt header
#include<QtCore/QString>
#include<QtCore/QFile>


namespace XPIWIT
{

// default constructor
MetaDataFilter::MetaDataFilter()
{
    mFilterName = "";
    mFilterId = "";
    mPostfix = "";

    mWriteMetaData = true;
    mIsMultiDimensional = false;
}


// default destructor
MetaDataFilter::~MetaDataFilter()
{
}


// function to retrieve the feature index by a given name
const int MetaDataFilter::GetFeatureIndex(const QString& featureName)
{
	// iterate over all feature names and return if a match is found
	for (int i=0; i<mTitle.length(); ++i)
		if (mTitle[i] == featureName)
			return i;

	return -1;
}


// writes the meta data to a csv file
void MetaDataFilter::WriteFilter( QString path, bool useHeader, QString separator, QString delimiter )
{
    // create file name and set the output path
    path += "_"  + mPostfix + ".csv";
	mOutputPath = path;

	// open the output file for writing
    QFile csvFile( path );
    csvFile.open( QIODevice::WriteOnly );

    // check if feature identifiers should be written to disk
    if( useHeader )
	{
        QString temp = "";
        for( int i = 0; i < mTitle.length(); i++ )
		{
			// add separator - space?
            if( i > 0 )
                temp = separator;
            temp += mTitle.at( i );
            csvFile.write( temp.toLatin1() );
        }
        csvFile.write("\n");
    }

	// iterate over all features and write them to the file
    for( int i = 0; i < mData.length(); i++ )
	{
        for( int j = 0; j < mData.at(i).length(); j++ )
		{
            QString temp = "";
            QString value = "";

			// add separator - space?
            if( j > 0 )
                temp = separator;

			if( !mType.isEmpty() && mType.at(j).compare( "float" ) == 0 )
			{
				// set the value and change delimiter for float variables
				value = QString::number(mData.at(i).at(j));
				value = value.replace( ".", delimiter );
			}
			else
			{
				// directly write the number for integer data types
				value = QString::number(int(mData.at(i).at(j)));
			}

			// write feature
            temp += value;
            csvFile.write( temp.toLatin1() );
        }

		// next line
        csvFile.write("\n");
    }

	// close the output file
    csvFile.close();
}

} // XPIWIT namespace
