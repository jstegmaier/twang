/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// file header
#include "ProcessObjectType.h"

// project header
#include "../../../Core/Utilities/Logger.h"

// itk header
#include "itkProcessObject.h"


namespace XPIWIT
{

// the default constructor
ProcessObjectType::ProcessObjectType()
{
    ProcessObjectType(ProcessObjectType::FILTERTYPE_UNSPECIFIED, 0, 0, 0, 0);
}


// more advanced constructor
ProcessObjectType::ProcessObjectType(FilterType filterType, int numImageIn, int numImageOut, int numMetaIn, int numMetaOut)
{
    QPair<DataType, int> undefType(ProcessObjectType::DATATYPE_UNDEFINED, 0);
    ProcessObjectType(filterType, numImageIn, numImageOut, numMetaIn, numMetaOut, undefType);
}


// more advanced constructor
ProcessObjectType::ProcessObjectType(FilterType filterType, int numImageIn, int numImageOut, int numMetaIn, int numMetaOut, QPair<DataType, int> inputType)
{
    mFilterType = filterType;

    mNumberImageInputs = numImageIn;
    mNumberImageOutputs = numImageOut;
    mNumberMetaInputs = numMetaIn;
    mNumberMetaOutputs = numMetaOut;
    //SetImageType(0, inputType);
}


// more advanced constructor
ProcessObjectType::ProcessObjectType(FilterType filterType, int numImageIn, int numImageOut, int numMetaIn, int numMetaOut, QPair<DataType, int> inputType, QPair<DataType, int> outputType)
{
    mFilterType = filterType;

    mNumberImageInputs = numImageIn;
    mNumberImageOutputs = numImageOut;
    mNumberMetaInputs = numMetaIn;
    mNumberMetaOutputs = numMetaOut;
    //SetImageType(0, inputType);
    //SetImageType(1, outputType);
}


// the destructor
ProcessObjectType::~ProcessObjectType()
{
}


// function to specify the image type
void ProcessObjectType::SetImageType(int num, DataType dataType, int dimension)
{
    SetImageType(num, QPair<DataType, int>(dataType, dimension));
}


// function to specify the image type
void ProcessObjectType::SetImageType(int num, QPair<DataType, int> imageType)
{
    if(num == 0){
        mImageTypes.clear();
        for(int i = 0; i < 6; i++)
            mImageTypes.append(imageType);
    }else{
        mImageTypes.replace(num, imageType);
    }
}

} // namespace XPIWIT
