/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// itk header
#include "itkImageDuplicator.h"
#include "ImageWrapper.h"
#include "../../XpExtended/WorkflowFilter/CastImageFilterWidget.h"


namespace XPIWIT
{

// function to get a pointer to the image
template< class TImageType >
typename TImageType::Pointer ImageWrapper::GetImage()
{
    // cast image
    typename TImageType::Pointer image = SelectTypeAndCastImage<TImageType>();

    return image;
}


// function to perform the type cast
template< class TImageType >
typename TImageType::Pointer ImageWrapper::SelectTypeAndCastImage()
{
    ProcessObjectType::DataType type = mImagePointerManager.GetImageType().first;

    if( type == ProcessObjectType::DATATYPE_CHAR )
        return CastImage< itk::Image< char, TImageType::ImageDimension>, TImageType >();

    if( type == ProcessObjectType::DATATYPE_SHORT )
        return CastImage< itk::Image< short, TImageType::ImageDimension>, TImageType >();

    if( type == ProcessObjectType::DATATYPE_INT )
        return CastImage< itk::Image< int, TImageType::ImageDimension>, TImageType >();

    if( type == ProcessObjectType::DATATYPE_LONG )
        return CastImage< itk::Image< long, TImageType::ImageDimension>, TImageType >();

    if( type == ProcessObjectType::DATATYPE_UCHAR )
        return CastImage< itk::Image< unsigned char, TImageType::ImageDimension>, TImageType >();

    if( type == ProcessObjectType::DATATYPE_USHORT )
        return CastImage< itk::Image< unsigned short, TImageType::ImageDimension>, TImageType >();

    if( type == ProcessObjectType::DATATYPE_UINT )
        return CastImage< itk::Image< unsigned int, TImageType::ImageDimension>, TImageType >();

    if( type == ProcessObjectType::DATATYPE_ULONG )
        return CastImage< itk::Image< unsigned long, TImageType::ImageDimension>, TImageType >();

    if( type == ProcessObjectType::DATATYPE_FLOAT )
        return CastImage< itk::Image< float, TImageType::ImageDimension>, TImageType >();

    if( type == ProcessObjectType::DATATYPE_DOUBLE )
        return CastImage< itk::Image< double, TImageType::ImageDimension>, TImageType >();

    return NULL;
}


// function to perform a type cast
template< class TInputImage, class TOutputImage >
typename TOutputImage::Pointer ImageWrapper::CastImage()
{
    typename TInputImage::Pointer inputImage = NULL;
    typename TOutputImage::Pointer outputImage = NULL;

    if( mCounter > 0 || !mReleaseData )
	{
        mImagePointerManager.GetPointer( &inputImage );

        typename itk::ImageDuplicator<TInputImage>::Pointer duplicator = itk::ImageDuplicator<TInputImage>::New();
        duplicator->SetInputImage( inputImage );
        duplicator->Update();
        typename TInputImage::Pointer inputImage = duplicator->GetOutput();

        ImagePointerManager tempImagePointerManager;
        tempImagePointerManager.SetPointer( inputImage );

        if( typeid( TInputImage ) == typeid( TOutputImage ) )
		{
            tempImagePointerManager.GetPointer( &outputImage );
        }
		else
		{
            ImageWrapper *imageWrapper = new ImageWrapper();
            imageWrapper->SetImage<TInputImage>( inputImage );
            imageWrapper->SetReleaseData( true );
			imageWrapper->SetRescaleFlag( GetRescaleFlag() );
            outputImage = imageWrapper->GetImage<TOutputImage>();
        }
    }
	else
	{
        // check if types are equal
        if( typeid( TInputImage ) == typeid( TOutputImage ) )
		{
            mImagePointerManager.GetPointer( &outputImage );
        }
		else
		{
            // cast image
            CastImageFilterWidget< TInputImage, TOutputImage  > castImageFilter;
            castImageFilter.SetInputImage( this, 0 );
            castImageFilter.Update();
            outputImage = castImageFilter.GetOutputImage( 0 )->template GetImage<TOutputImage>();
        }
    }

    return outputImage;
}

} // namespace XPIWIT
