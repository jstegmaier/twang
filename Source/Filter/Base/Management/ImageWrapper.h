/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

#ifndef IMAGEWRAPPER_H
#define IMAGEWRAPPER_H

// namespace header
#include "ImagePointerManager.h"


namespace XPIWIT
{

/**
 * stores one specific settings.
 */
class ImageWrapper
{
public:
    ImageWrapper() { mReleaseData = true; 
					 mRescaleImageFlag = true;
					 mCounter = 0; }

    void SetReleaseData( bool releaseData ) { mReleaseData = releaseData; }
    bool GetReleaseData() { return mReleaseData; }

    ImagePointerManager GetPointerManager() { return mImagePointerManager; }
	void ReleaseImage() { mImagePointerManager.DeleteCurrentPointer(); }

    template< class TImageType >
    typename TImageType::Pointer GetImage();

    template< class TImageType >
    void SetImage( typename TImageType::Pointer imagePointer, int counter = 0 ) { mImagePointerManager.SetPointer( imagePointer ); mCounter = counter; }

    QPair< ProcessObjectType::DataType, int > GetImageType() { return mImagePointerManager.GetImageType(); }
    int GetImageDimension() { return mImagePointerManager.GetImageType().second; }
    ProcessObjectType::DataType GetImageDataType() { return mImagePointerManager.GetImageType().first; }

	void SetCounter( int counter ) { mCounter = counter; }
	int GetCounter() { return mCounter; }
	bool decrementCounter() { mCounter -= 1; return mCounter > 0; }

	void SetRescaleFlag( bool flag ) { mRescaleImageFlag = flag; }
	bool GetRescaleFlag() { return mRescaleImageFlag; }

private:

    bool mReleaseData;
    ImagePointerManager mImagePointerManager;

    template< class TImageType >
    typename TImageType::Pointer SelectTypeAndCastImage();

    template< class TInputImage, class TOutputImage >
    typename TOutputImage::Pointer CastImage();

	int mCounter;
	bool mRescaleImageFlag;
};

} // namespace XPIWIT

#include "ImageWrapper.txx"

#endif // IMAGEWRAPPER_H
