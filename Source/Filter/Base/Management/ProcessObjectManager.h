/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

#ifndef PROCESSOBJECTMANAGER_H
#define PROCESSOBJECTMANAGER_H

// include required headers
#include <QtCore/QObject>
#include <QtCore/QUrl>
#include <QtCore/QSettings>
#include <QtCore/QDateTime>
#include <QtCore/QMutex>
#include <QtCore/QPair>
#include <QtCore/QString>
#include "../../../Core/Utilities/Logger.h"


namespace XPIWIT
{
	// forward declaration of the base process object
	class ProcessObjectBase;

/**
 *	@class ProcessObjectList
 *	Singleton
 *  Holds a list of all implemented Filters
 */
class ProcessObjectManager : public QObject
{
	Q_OBJECT
    public:

        /**
         * get singleton instance
         */
        static ProcessObjectManager* GetInstance();

        /**
         * delete singleton instance
         */
        static void DeleteInstance();

        /* ------------------------------------ */
        /* --------productive functions-------- */
        /* ------------------------------------ */

        QList<ProcessObjectBase*> GetProcessObjectList();
        ProcessObjectBase* GetProcessObjectInstance(QString name, QStringList types, QList<int> dimensions);

    private:
        /**
         * Singleton Instance
         */
        static ProcessObjectManager* mInstance;

        /**
         * The constructor.
         * @param parent The parent of the main window. Usually should be NULL.
         */
        ProcessObjectManager();

        /**
         * The destructor.
         */
        virtual ~ProcessObjectManager();

        void Init();

		bool StringCompare( QString, QString );

        /**
         * List of all available Filter
         */
        QList<ProcessObjectBase *> mProcessObjectWidgetList;
    protected:
};

} // namespace XPIWIT

#endif // PROCESSOBJECTMANAGER_H
