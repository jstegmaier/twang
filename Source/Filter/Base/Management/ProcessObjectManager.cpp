/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// include required headers
#include "ProcessObjectManager.h"


// ------------------------------------------------- //
// List of all Filters (TODO: extend for new filter) //
// ------------------------------------------------- //

// -------------------------------------------------- //
// ------------- XpWrapper -------------------------- //
// -------------------------------------------------- //

// ------------- Blur ------------------------------- //
#include "../../XpWrapper/Blur/DiscreteGaussianImageFilterWrapper.h"
#include "../../XpWrapper/Blur/MedianImageFilterWrapper.h"

// ------------- EdgeDetection --------------------- //

// ------------- ImageTransformation --------------- //
#include "../../XpWrapper/ImageTransformation/SignedMaurerDistanceMapImageFilterWrapper.h"
#include "../../XpWrapper/ImageTransformation/InvertIntensityImageFilterWrapper.h"
#include "../../XpWrapper/ImageTransformation/RescaleIntensityImageFilterWrapper.h"
#include "../../XpWrapper/ImageTransformation/SubtractImageFilterWrapper.h"
#include "../../XpWrapper/ImageTransformation/MultiplyImageFilterWrapper.h"
#include "../../XpWrapper/ImageTransformation/AddImageFilterWrapper.h"

// ------------- InformationExtraction ------------- //
#include "../../XpWrapper/InformationExtraction/BinaryImageToLabelImageWrapper.h"
#include "../../XpWrapper/InformationExtraction/LabelGeometryImageFilterWrapper.h"

// ------------- Morphology ------------------------ //
#include "../../XpWrapper/Morphology/BinaryDilateImageFilterWrapper.h"
#include "../../XpWrapper/Morphology/MorphologicalWatershedFromMarkersImageWrapper.h"

// ------------- Noise ----------------------- //

// ------------- Segmentation ----------------------- //
#include "../../XpWrapper/Segmentation/BinaryThresholdImageFilterWrapper.h"
#include "../../XpWrapper/Segmentation/OtsuThresholdImageFilterWrapper.h"

// -------------------------------------------------- //
// ------------- XpExtended ------------------------- //
// -------------------------------------------------- //

// ------------- ACME ------------------------------- //

// ------------- Blur ------------------------------- //

// ------------------------------------------------- //
// ------------- ImageTransformation --------------- //
#include "../../XpExtended/ImageTransformation/ResizeFilterWidget.h"

// ------------------------------------------------- //
// ------------- InformationExtraction-------------- //
#include "../../XpExtended/InformationExtraction/ExtractLocalExtremaFilterWidget.h"
#include "../../XpExtended/InformationExtraction/ExtractRegionPropsFilterWidget.h"
#include "../../XpExtended/InformationExtraction/LoGScaleSpaceMaximumProjectionFilterWidget.h"
#include "../../XpExtended/InformationExtraction/ExtractSeedBasedIntensityWindowFilterWidget.h"

// ------------------------------------------------- //
// ---------------- Morphology --------------------- //

// ------------------------------------------------- //
// ---------------- Segmentation ------------------- //
#include "../../XpExtended/Segmentation/EdgeMapFilterWidget.h"
#include "../../XpExtended/Segmentation/LabelImageFilterWidget.h"
#include "../../XpExtended/Segmentation/ThresholdFilterWidget.h"
#include "../../XpExtended/Segmentation/TwangSegmentationWidget.h"
#include "../../XpExtended/Segmentation/LevelSetSegmentationWidget.h"

// ------------------------------------------------- //
// ------------------ Workflow --------------------- //
#include "../../XpExtended/WorkflowFilter/ImageReaderWidget.h"
#include "../../XpExtended/WorkflowFilter/MetaReaderWidget.h"
#include "../../XpExtended/WorkflowFilter/CastImageFilterWidget.h"

// ------------------------------------------------- //


namespace XPIWIT {

	ProcessObjectManager* ProcessObjectManager::mInstance = 0;

	// the default constructor
	ProcessObjectManager *ProcessObjectManager::GetInstance()
	{
		static QMutex mutex;
		if (!mInstance)
		{
			mutex.lock();
			if (!mInstance)
				mInstance = new ProcessObjectManager();
			mutex.unlock();
		}

		return mInstance;
	}

	void ProcessObjectManager::DeleteInstance()
	{
		static QMutex mutex;
		mutex.lock();
		delete mInstance;
		mInstance = 0;
		mutex.unlock();
	}

	ProcessObjectManager::ProcessObjectManager() : QObject()
	{
		Init();
	}

	ProcessObjectManager::~ProcessObjectManager()
	{
		qDeleteAll( mProcessObjectWidgetList );
		mProcessObjectWidgetList.clear();
	}

	void ProcessObjectManager::Init()
	{
		qDeleteAll( mProcessObjectWidgetList );
		mProcessObjectWidgetList.clear();

		// ------------------------------------------------- //
		// List of all Filters (TODO: extend for new filter) //
		// ------------------------------------------------- //

		// ------------------------------------------------- //
		// ------------- XpWrapper ------------------------- //
		// ------------------------------------------------- //

		// ------------- Blur ------------------------------ //
		mProcessObjectWidgetList.append( new DiscreteGaussianImageFilterWrapper< Image3Float >() );
		mProcessObjectWidgetList.append( new MedianImageFilterWrapper< Image3Float >() );

		// ------------- EdgeDetection --------------------- //
		
		// ------------- ImageTransformation --------------- //#
		mProcessObjectWidgetList.append( new SignedMaurerDistanceMapImageFilterWrapper< Image3Float >() );
		mProcessObjectWidgetList.append( new InvertIntensityImageFilterWrapper< Image3Float >() );
		mProcessObjectWidgetList.append( new RescaleIntensityImageFilterWrapper< Image3Float >() );
		mProcessObjectWidgetList.append( new SubtractImageFilterWrapper< Image3Float >() );
		mProcessObjectWidgetList.append( new MultiplyImageFilterWrapper< Image3Float >() );
		mProcessObjectWidgetList.append( new AddImageFilterWrapper< Image3Float >() );

		// ------------- InformationExtraction ------------- //
		mProcessObjectWidgetList.append( new BinaryImageToLabelImageWrapper< Image3Float >() );
		mProcessObjectWidgetList.append( new LabelGeometryImageFilterWrapper< Image3Float >() );
				
		// ------------- Morphology ------------------------ //
		mProcessObjectWidgetList.append( new BinaryDilateImageFilterWrapper< Image3Float >() );
		mProcessObjectWidgetList.append( new MorphologicalWatershedFromMarkersImageWrapper< Image3Float >() );

		// ------------- Noise ---------------------- //

		// ------------- Segmentation ---------------------- //
		mProcessObjectWidgetList.append( new BinaryThresholdImageFilterWrapper< Image3Float >() );
		mProcessObjectWidgetList.append( new OtsuThresholdImageFilterWrapper< Image3Float >() );
		mProcessObjectWidgetList.append( new LevelSetSegmentationWidget< Image3Float >() );

		// ------------------------------------------------- //
		// ------------- XpExtended ------------------------ //
		// ------------------------------------------------- //

		// ------------- ACME ------------------------------ //

		// ------------- Blur ------------------------------ //

		// ------------------------------------------------ //
		// ------------- ImageTransformation -------------- //
		mProcessObjectWidgetList.append( new ResizeFilterWidget< Image3Float >() );
				
		// ------------------------------------------------ //
		// ------------- InformationExtraction------------- //
		mProcessObjectWidgetList.append( new ExtractLocalExtremaFilterWidget< Image3Float, Image3UChar >() );
		mProcessObjectWidgetList.append( new ExtractRegionPropsFilterWidget< Image3Float >() );
		mProcessObjectWidgetList.append( new LoGScaleSpaceMaximumProjectionFilterWidget< Image3Float, Image3UChar >() );
		mProcessObjectWidgetList.append( new ExtractSeedBasedIntensityWindowFilterWidget< Image3Float >() );

		// ------------------------------------------------- //
		// ---------------- Morphology --------------------- //

		// ------------------------------------------------ //
		// ---------------- Segmentation ------------------ //
		mProcessObjectWidgetList.append( new EdgeMapFilterWidget< Image3Float >() );
		mProcessObjectWidgetList.append( new ThresholdFilterWidget< Image3Float >() );
		mProcessObjectWidgetList.append( new TwangSegmentationWidget< Image3Float >() );

		// ------------------------------------------------ //
		// ------------------ Workflow -------------------- //
		mProcessObjectWidgetList.append( new ImageReaderWidget< Image3Float >() );
		mProcessObjectWidgetList.append( new MetaReaderWidget() );
		mProcessObjectWidgetList.append( new CastImageFilterWidget< Image3Float, Image3Float >() );

		// ------------------------------------------------ //
	}

	QList<ProcessObjectBase*> ProcessObjectManager::GetProcessObjectList()
	{
		//return float3DList.getProcessObjectList();
		return mProcessObjectWidgetList;

	}

	bool ProcessObjectManager::StringCompare( QString str1, QString str2 ){

		if( str1.toLower().compare( str2.toLower() ) == 0)
			return true;

		return false;
	}

	ProcessObjectBase *ProcessObjectManager::GetProcessObjectInstance( QString name, QStringList types, QList<int> dimensions )
	{
		ProcessObjectBase *processObjectBase = NULL;

		if( dimensions.isEmpty() ){
			Logger::GetInstance()->WriteToAll("! Filter: " + name + " can not be instantiated, dimension information is missing.\n");
			return processObjectBase;
		}


		// ------------------------------------------------- //
		// ------------------ XpWrapper -------------------- //
		// ------------------------------------------------- //

		// ------------------ Blur ------------------------- //
		if( StringCompare( name, "DiscreteGaussianImageFilter" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new DiscreteGaussianImageFilterWrapper<Image2UShort>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new DiscreteGaussianImageFilterWrapper<Image3UShort>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new DiscreteGaussianImageFilterWrapper<Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new DiscreteGaussianImageFilterWrapper<Image3Float>();
				}
			}else{
				// default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new DiscreteGaussianImageFilterWrapper<Image2Float>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new DiscreteGaussianImageFilterWrapper<Image3Float>();
			}
		}
		if( StringCompare( name, "MedianImageFilter" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new MedianImageFilterWrapper<Image2UShort>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new MedianImageFilterWrapper<Image3UShort>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new MedianImageFilterWrapper<Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new MedianImageFilterWrapper<Image3Float>();
				}
			}else{
				// default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new MedianImageFilterWrapper<Image2Float>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new MedianImageFilterWrapper<Image3Float>();
			}
		}


		// ------------- EdgeDetection --------------------- //


		// ------------- ImageTransformation --------------- //
		if( StringCompare( name, "SignedMaurerDistanceMapImageFilter" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new SignedMaurerDistanceMapImageFilterWrapper<Image2UShort>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new SignedMaurerDistanceMapImageFilterWrapper<Image3UShort>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new SignedMaurerDistanceMapImageFilterWrapper<Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new SignedMaurerDistanceMapImageFilterWrapper<Image3Float>();
				}
			}else{
				// default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new SignedMaurerDistanceMapImageFilterWrapper<Image2Float>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new SignedMaurerDistanceMapImageFilterWrapper<Image3Float>();
			}
		}
		if( StringCompare( name, "InvertIntensityImageFilter" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new InvertIntensityImageFilterWrapper<Image2UShort>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new InvertIntensityImageFilterWrapper<Image3UShort>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new InvertIntensityImageFilterWrapper<Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new InvertIntensityImageFilterWrapper<Image3Float>();
				}
			}else{
				// default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new InvertIntensityImageFilterWrapper<Image2Float>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new InvertIntensityImageFilterWrapper<Image3Float>();
			}
		}

		if( StringCompare( name, "RescaleIntensityImageFilter" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new RescaleIntensityImageFilterWrapper<Image2UShort>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new RescaleIntensityImageFilterWrapper<Image3UShort>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new RescaleIntensityImageFilterWrapper<Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new RescaleIntensityImageFilterWrapper<Image3Float>();
				}
			}else{
				// default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new RescaleIntensityImageFilterWrapper<Image2Float>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new RescaleIntensityImageFilterWrapper<Image3Float>();
			}
		}
		if( StringCompare( name, "SubtractImageFilter" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new SubtractImageFilterWrapper<Image2UShort>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new SubtractImageFilterWrapper<Image3UShort>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new SubtractImageFilterWrapper<Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new SubtractImageFilterWrapper<Image3Float>();
				}
			}else{
				//default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new SubtractImageFilterWrapper<Image2Float>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new SubtractImageFilterWrapper<Image3Float>();
			}
		}
		if( StringCompare( name, "MultiplyImageFilter" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new MultiplyImageFilterWrapper<Image2UShort>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new MultiplyImageFilterWrapper<Image3UShort>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new MultiplyImageFilterWrapper<Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new MultiplyImageFilterWrapper<Image3Float>();
				}
			}else{
				//default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new MultiplyImageFilterWrapper<Image2Float>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new MultiplyImageFilterWrapper<Image3Float>();
			}
		}
		if( StringCompare( name, "AddImageFilter" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new AddImageFilterWrapper<Image2UShort>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new AddImageFilterWrapper<Image3UShort>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new AddImageFilterWrapper<Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new AddImageFilterWrapper<Image3Float>();
				}
			}else{
				//default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new AddImageFilterWrapper<Image2Float>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new AddImageFilterWrapper<Image3Float>();
			}
		}

		// ------------- InformationExtraction ------------- //
		if( StringCompare( name, "BinaryImageToLabelImage" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new BinaryImageToLabelImageWrapper<Image2UShort>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new BinaryImageToLabelImageWrapper<Image3UShort>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new BinaryImageToLabelImageWrapper<Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new BinaryImageToLabelImageWrapper<Image3Float>();
				}
			}else{
				// default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new BinaryImageToLabelImageWrapper<Image2Float>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new BinaryImageToLabelImageWrapper<Image3Float>();
			}
		}


		// ------------- Morphology ------------------------ //
		if( StringCompare( name, "BinaryDilateImageFilter" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new BinaryDilateImageFilterWrapper<Image2UShort>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new BinaryDilateImageFilterWrapper<Image3UShort>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new BinaryDilateImageFilterWrapper<Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new BinaryDilateImageFilterWrapper<Image3Float>();
				}
			}else{
				// default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new BinaryDilateImageFilterWrapper<Image2Float>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new BinaryDilateImageFilterWrapper<Image3Float>();
			}
		}

		// ------------- Segmentation ---------------------- //
		if( StringCompare( name, "BinaryThresholdImageFilter" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new BinaryThresholdImageFilterWrapper<Image2UShort>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new BinaryThresholdImageFilterWrapper<Image3UShort>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new BinaryThresholdImageFilterWrapper<Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new BinaryThresholdImageFilterWrapper<Image3Float>();
				}
			}else{
				// default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new BinaryThresholdImageFilterWrapper<Image2Float>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new BinaryThresholdImageFilterWrapper<Image3Float>();
			}
		}

		if( StringCompare( name, "OtsuThresholdImageFilter" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new OtsuThresholdImageFilterWrapper<Image2UShort>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new OtsuThresholdImageFilterWrapper<Image3UShort>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new OtsuThresholdImageFilterWrapper<Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new OtsuThresholdImageFilterWrapper<Image3Float>();
				}
			}else{
				// default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new OtsuThresholdImageFilterWrapper<Image2Float>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new OtsuThresholdImageFilterWrapper<Image3Float>();
			}
		}
		if( StringCompare( name, "LevelSetSegmentationFilter" ) ){
			if( types.length() > 0 && dimensions.length() > 0 ){

				//if( StringCompare( types.at( 0 ), "ushort" ) ){
				//	if( dimensions.at( 0 ) == 2 )
				//		processObjectBase = new LevelSetSegmentationWidget<Image2UShort>();
				//	if( dimensions.at( 0 ) == 3 )
				//		processObjectBase = new LevelSetSegmentationWidget<Image3UShort>();
				//}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new LevelSetSegmentationWidget<Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new LevelSetSegmentationWidget<Image3Float>();
				}
			}else{
				// default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new LevelSetSegmentationWidget<Image2Float>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new LevelSetSegmentationWidget<Image3Float>();
			}
		}

		// ------------- NOISE ------------------------------ //


		// ------------------------------------------------- //
		// ------------------ XpExtended ------------------- //
		// ------------------------------------------------- //

		// ------------- ACME ------------------------------ //


		// ------------- Blur ------------------------------ //
	


		// ------------- Image Transformation ------------- //

		if( StringCompare( name, "ResizeFilter" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new ResizeFilterWidget<Image2UShort>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new ResizeFilterWidget<Image3UShort>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new ResizeFilterWidget<Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new ResizeFilterWidget<Image3Float>();
				}
			}else{
				// default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new ResizeFilterWidget<Image2Float>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new ResizeFilterWidget<Image3Float>();
			}
		}


		// ------------- Information Extraction ------------ //

		if( StringCompare( name, "ExtractLocalExtremaFilter" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new ExtractLocalExtremaFilterWidget<Image2UShort, Image2UChar>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new ExtractLocalExtremaFilterWidget<Image3UShort, Image3UChar>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new ExtractLocalExtremaFilterWidget<Image2Float, Image2UChar>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new ExtractLocalExtremaFilterWidget<Image3Float, Image3UChar>();
				}
			}else{
				// default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new ExtractLocalExtremaFilterWidget<Image2Float, Image2UChar>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new ExtractLocalExtremaFilterWidget<Image3Float, Image3UChar>();
			}
		}
		if( StringCompare( name, "ExtractRegionPropsFilter" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new ExtractRegionPropsFilterWidget<Image2UShort>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new ExtractRegionPropsFilterWidget<Image3UShort>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new ExtractRegionPropsFilterWidget<Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new ExtractRegionPropsFilterWidget<Image3Float>();
				}
			}else{
				// default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new ExtractRegionPropsFilterWidget<Image2Float>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new ExtractRegionPropsFilterWidget<Image3Float>();
			}
		}
		if( StringCompare( name, "ExtractSeedBasedIntensityWindowFilter" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new ExtractSeedBasedIntensityWindowFilterWidget<Image2UShort>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new ExtractSeedBasedIntensityWindowFilterWidget<Image3UShort>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new ExtractSeedBasedIntensityWindowFilterWidget<Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new ExtractSeedBasedIntensityWindowFilterWidget<Image3Float>();
				}
			}else{
				// default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new ExtractSeedBasedIntensityWindowFilterWidget<Image2Float>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new ExtractSeedBasedIntensityWindowFilterWidget<Image3Float>();
			}
		}
		if( StringCompare( name, "LoGScaleSpaceMaximumProjectionFilter" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new LoGScaleSpaceMaximumProjectionFilterWidget<Image2UShort, Image2UChar>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new LoGScaleSpaceMaximumProjectionFilterWidget<Image3UShort, Image3UChar>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new LoGScaleSpaceMaximumProjectionFilterWidget<Image2Float, Image2UChar>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new LoGScaleSpaceMaximumProjectionFilterWidget<Image3Float, Image3UChar>();
				}
			}else{
				// default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new LoGScaleSpaceMaximumProjectionFilterWidget<Image2Float, Image2UChar>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new LoGScaleSpaceMaximumProjectionFilterWidget<Image3Float, Image3UChar>();
			}
		}

		// ---------------- Morphology -------------------- //

		if( StringCompare( name, "MorphologicalWatershedFromMarkersImageFilter" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new MorphologicalWatershedFromMarkersImageWrapper<Image2UShort>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new MorphologicalWatershedFromMarkersImageWrapper<Image3UShort>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new MorphologicalWatershedFromMarkersImageWrapper<Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new MorphologicalWatershedFromMarkersImageWrapper<Image3Float>();
				}
			}else{
				// default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new MorphologicalWatershedFromMarkersImageWrapper<Image2Float>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new MorphologicalWatershedFromMarkersImageWrapper<Image3Float>();
			}
		}

		// ---------------- Segmentation ------------------ //

		if( StringCompare( name, "ThresholdFilter" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new ThresholdFilterWidget<Image2UShort>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new ThresholdFilterWidget<Image3UShort>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new ThresholdFilterWidget<Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new ThresholdFilterWidget<Image3Float>();
				}
			}else{
				// default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new ThresholdFilterWidget<Image2Float>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new ThresholdFilterWidget<Image3Float>();
			}
		}
		if( StringCompare( name, "TwangSegmentationFilter" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new TwangSegmentationWidget<Image2UShort>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new TwangSegmentationWidget<Image3UShort>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new TwangSegmentationWidget<Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new TwangSegmentationWidget<Image3Float>();
				}
			}else{
				// default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new TwangSegmentationWidget<Image2Float>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new TwangSegmentationWidget<Image3Float>();
			}
		}
		if( StringCompare( name, "EdgeMapFilter" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new EdgeMapFilterWidget<Image2UShort>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new EdgeMapFilterWidget<Image3UShort>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new EdgeMapFilterWidget<Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new EdgeMapFilterWidget<Image3Float>();
				}
			}else{
				// default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new EdgeMapFilterWidget<Image2Float>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new EdgeMapFilterWidget<Image3Float>();
			}
		}

		// ------------------ Workflow --------------------- //
		if( StringCompare( name, "ImageReader" ) ){
			if( types.length() > 0 ){
				// explicit type
				if( StringCompare( types.at( 0 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new ImageReaderWidget<Image2UShort>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new ImageReaderWidget<Image3UShort>();
				}

				if( StringCompare( types.at( 0 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new ImageReaderWidget<Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new ImageReaderWidget<Image3Float>();
				}
			}else{
				// default
				if( dimensions.at( 0 ) == 2 )
					processObjectBase = new ImageReaderWidget<Image2Float>();
				if( dimensions.at( 0 ) == 3 )
					processObjectBase = new ImageReaderWidget<Image3Float>();
			}
		}
		if( StringCompare( name, "MetaReader" ) ){
			processObjectBase = new MetaReaderWidget();
		}
		if( StringCompare( name, "CastImageFilter" ) ){
			if( types.length() > 1 && dimensions.length() > 1 ){

				if( StringCompare( types.at( 0 ), "ushort" ) && StringCompare( types.at( 1 ), "float" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new CastImageFilterWidget<Image2UShort, Image2Float>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new CastImageFilterWidget<Image3UShort, Image3Float>();
				}

				if( StringCompare( types.at( 0 ), "float" ) && StringCompare( types.at( 1 ), "ushort" ) ){
					if( dimensions.at( 0 ) == 2 )
						processObjectBase = new CastImageFilterWidget<Image2Float, Image2UShort>();
					if( dimensions.at( 0 ) == 3 )
						processObjectBase = new CastImageFilterWidget<Image3Float, Image3UShort>();
				}
			}
		}

		if( processObjectBase == NULL )
		{
			Logger::GetInstance()->WriteToAll("! Filter: " + name + " can not be instantiated and will be skipped.\n");
			Logger::GetInstance()->WriteToAll("- Update of ProcessObjectManager may solve this error.\n");
		}
		return processObjectBase;
	}

} // namespace XPIWIT
