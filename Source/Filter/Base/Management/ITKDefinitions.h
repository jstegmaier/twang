/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

#ifndef ITKDEFINITIONS_H
#define ITKDEFINITIONS_H

// itk header
#include "itkPoint.h"
#include "itkImage.h"
#include "itkMatrix.h"

// preprocessor macro for a try catch construct
#define itkTryCatch(expression, description)	                \
    try															\
    {															\
        expression;												\
    }															\
    catch( itk::ExceptionObject & err )							\
    {															\
        std::cout << "ExceptionObject caught in: " << description << std::endl;	\
        std::cout << err << std::endl;							\
        return;													\
    }															\

// preprocessor macro for a try catch construct
#define itkTryCatchWithReturn(expression, description)	        \
    try															\
    {															\
        expression;												\
    }															\
    catch( itk::ExceptionObject & err )							\
    {															\
        std::cout << "ExceptionObject caught in: " << description << std::endl;	\
        std::cout << err << std::endl;							\
        return NULL;											\
    }															\

// preprocessor macro for a try catch construct
#define itkTryCatchWithContinue(expression, description)	    \
    try															\
    {															\
        expression;												\
    }															\
    catch( itk::ExceptionObject & err )							\
    {															\
        std::cout << "ExceptionObject caught in: " << description << std::endl;	\
        std::cout << err << std::endl;							\
        continue;												\
    }

namespace XPIWIT
{
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // ITK Pixel Types.
    typedef char                                PixelChar;
    typedef short                               PixelShort;
    typedef int                                 PixelInt;
    typedef long                                PixelLong;
    typedef unsigned char                       PixelUChar;
    typedef unsigned short                      PixelUShort;
    typedef unsigned int                        PixelUInt;
    typedef unsigned long                       PixelULong;
    typedef float                               PixelFloat;
    typedef double                              PixelDouble;

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // ITK point types for 2D
    typedef itk::Point<char, 2>                 Point2Char;
    typedef itk::Point<short, 2>                Point2Short;
    typedef itk::Point<int, 2>                  Point2Int;
    typedef itk::Point<long, 2>                 Point2Long;
    typedef itk::Point<unsigned char, 2>        Point2UChar;
    typedef itk::Point<unsigned short, 2>       Point2UShort;
    typedef itk::Point<unsigned int, 2>         Point2UInt;
    typedef itk::Point<unsigned long, 2>        Point2ULong;
    typedef itk::Point<float, 2>                Point2Float;
    typedef itk::Point<double, 2>               Point2Double;

    // ITK point types for 3D
    typedef itk::Point<char, 3>                 Point3Char;
    typedef itk::Point<short, 3>                Point3Short;
    typedef itk::Point<int, 3>                  Point3Int;
    typedef itk::Point<long, 3>                 Point3Long;
    typedef itk::Point<unsigned char, 3>        Point3UChar;
    typedef itk::Point<unsigned short, 3>       Point3UShort;
    typedef itk::Point<unsigned int, 3>         Point3UInt;
    typedef itk::Point<unsigned long, 3>        Point3ULong;
    typedef itk::Point<float, 3>                Point3Float;
    typedef itk::Point<double, 3>               Point3Double;

    // ITK point types for 4D
    typedef itk::Point<char, 4>                 Point4Char;
    typedef itk::Point<short, 4>                Point4Short;
    typedef itk::Point<int, 4>                  Point4Int;
    typedef itk::Point<long, 4>                 Point4Long;
    typedef itk::Point<unsigned char, 4>        Point4UChar;
    typedef itk::Point<unsigned short, 4>       Point4UShort;
    typedef itk::Point<unsigned int, 4>         Point4UInt;
    typedef itk::Point<unsigned long, 4>        Point4ULong;
    typedef itk::Point<float, 4>                Point4Float;
    typedef itk::Point<double, 4>               Point4Double;

    // ITK matrix types for 3x3
    typedef itk::Matrix<char, 3, 3>				Matrix3x3Char;
    typedef itk::Matrix<short, 3, 3>			Matrix3x3Short;
    typedef itk::Matrix<int, 3, 3>				Matrix3x3Int;
    typedef itk::Matrix<unsigned char, 3, 3>	Matrix3x3UChar;
    typedef itk::Matrix<unsigned short, 3, 3>	Matrix3x3UShort;
    typedef itk::Matrix<unsigned int, 3, 3>		Matrix3x3UInt;
    typedef itk::Matrix<float, 3, 3>			Matrix3x3Float;
    typedef itk::Matrix<double, 3, 3>			Matrix3x3Double;


    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // ITK image types for 2D
    typedef itk::Image<PixelChar, 2>            Image2Char;
    typedef itk::Image<PixelShort, 2>           Image2Short;
    typedef itk::Image<PixelInt, 2>             Image2Int;
    typedef itk::Image<PixelLong, 2>            Image2Long;
    typedef itk::Image<PixelUChar, 2>           Image2UChar;
    typedef itk::Image<PixelUShort, 2>          Image2UShort;
    typedef itk::Image<PixelUInt, 2>            Image2UInt;
    typedef itk::Image<PixelULong, 2>           Image2ULong;
    typedef itk::Image<PixelFloat, 2>           Image2Float;
    typedef itk::Image<PixelDouble, 2>          Image2Double;

    // ITK image types for 3D
    typedef itk::Image<PixelChar, 3>            Image3Char;
    typedef itk::Image<PixelShort, 3>           Image3Short;
    typedef itk::Image<PixelInt, 3>             Image3Int;
    typedef itk::Image<PixelLong, 3>            Image3Long;
    typedef itk::Image<PixelUChar, 3>           Image3UChar;
    typedef itk::Image<PixelUShort, 3>          Image3UShort;
    typedef itk::Image<PixelUInt, 3>            Image3UInt;
    typedef itk::Image<PixelULong, 3>           Image3ULong;
    typedef itk::Image<PixelFloat, 3>           Image3Float;
    typedef itk::Image<PixelDouble, 3>          Image3Double;

    // ITK image types for 4D
    typedef itk::Image<PixelChar, 4>            Image4Char;
    typedef itk::Image<PixelShort, 4>           Image4Short;
    typedef itk::Image<PixelInt, 4>             Image4Int;
    typedef itk::Image<PixelLong, 4>            Image4Long;
    typedef itk::Image<PixelUChar, 4>           Image4UChar;
    typedef itk::Image<PixelUShort, 4>          Image4UShort;
    typedef itk::Image<PixelUInt, 4>            Image4UInt;
    typedef itk::Image<PixelULong, 4>           Image4ULong;
    typedef itk::Image<PixelFloat, 4>           Image4Float;
    typedef itk::Image<PixelDouble, 4>          Image4Double;

    // ITK matrix image types for 3x3
    typedef itk::Image<Matrix3x3Char, 3>        ImageMatrix3Char;
    typedef itk::Image<Matrix3x3Short, 3>       ImageMatrix3Short;
    typedef itk::Image<Matrix3x3Int, 3>         ImageMatrix3Int;
    typedef itk::Image<Matrix3x3UChar, 3>       ImageMatrix3UChar;
    typedef itk::Image<Matrix3x3UShort, 3>      ImageMatrix3UShort;
    typedef itk::Image<Matrix3x3UInt, 3>        ImageMatrix3UInt;
    typedef itk::Image<Matrix3x3Float, 3>       ImageMatrix3Float;
    typedef itk::Image<Matrix3x3Double, 3>      ImageMatrix3Double;

} // namespace XPIWIT

#endif
