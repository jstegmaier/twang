/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

#ifndef PROCESSOBJECTBASE_H
#define PROCESSOBJECTBASE_H

// namespace header
#include "ProcessObjectSettings.h"
#include "ProcessObjectType.h"
#include "../MetaData/MetaDataBase.h"
#include "../MetaData/MetaDataImage.h"
#include "../MetaData/MetaDataFilter.h"

// project header
#include "../../../Core/XML/AbstractFilter.h"
#include "../../../Core/CMD/CMDPipelineArguments.h"

// itk header
//#include "ITKDefinitions.h"

// qt header
#include <QtCore/QObject>
#include <QtCore/QUrl>
#include <QtCore/QSettings>
#include <QtCore/QDateTime>
#include <QtCore/QStringList>
#include <QtCore/QList>

// system header

namespace XPIWIT
{
   class ImageWrapper;
/**
 *	@class ProcessObjectBase
 *	The base class for image to image filter wrapper.
 */
class ProcessObjectBase : public QObject
{
    public:

        /**
         * The constructor.
         */
        ProcessObjectBase();


        /**
         * The destructor.
         */
        virtual ~ProcessObjectBase();


        /**
         * Function to init the main window. All widgets, menus etc. are created within this function.
         */
        void Init();


        /**
         * Function to set the pointer of the input iamges
         */
        virtual void PrepareInputs();


        /**
         * Update the filter.
         */
        virtual void Update();


        /**
         * write result image to disk
         */
        void WriteResult( ImageWrapper*, int );


        /**
         * Function to get the name of the filter.
         * @param Returns the name of the current filter.
         */
        const QString& GetName()							{ return mName; }


        /**
         * Function to get the name of the filter.
         * @param Returns the name of the current filter.
         */
        const QString& GetDescription() 					{ return mDescription; }


        /**
         * Function to set an image
         * @param Select the number if object has more than one output
         */
        virtual void SetInputImage(ImageWrapper *imageWrapper, int num);


        /**
         * Function to get a processed image
         * @param Select the number if object has more than one output
         */
        virtual ImageWrapper *GetOutputImage(int num);


        /**
         * Function to get the type of the filter.
         * @param Returns the type of the current filter.
         */
        ProcessObjectType *GetType()					{ return mObjectType; }


        /**
         * Function to set a specific setting.
         * @param
         */
        void SetSetting(const QString& name, const QString& value) { mProcessObjectSettings->SetSettingValue( name, value); }

        /**
         * Function to get the process object settings.
         * @return The process object settings variable.
         */
        ProcessObjectSettings* GetProcessObjectSettings()		{ return mProcessObjectSettings; }


        /**
         * Function to set the dirty flag, i.e. to force update call and get valid output.
         * @param dirtyFlag The new dirty flag value.
         */
        void SetDirtyFlag(bool dirtyFlag)						{ mDirtyFlag = dirtyFlag; }


        /**
         * Function to mark the last filter of the pipeline
         */
        void SetIsLast() { mIsLastFilter = true; }


		/**
		 * Function to release the outputs.
		 */
		void ReleaseOutputs();


        /**
         * Function to start a timer upon filter execution
         */
        void StartTimer();


        /**
         * Function to log the performance, i.e. the running time of the filter.
         */
        void LogPerformance(bool logSettings = true);


        /**
         * Function to log the settings of the filter.
         */
        void LogSettings();


        /**
         * @brief GetAbstractFilter returns the abstract filter object
         */
        AbstractFilter* GetAbstractFilter() { return mAbstractFilter; }


        /**
         * @brief SetAbstractFilter sets the abstract filter object
         */
        void SetAbstractFilter(AbstractFilter* abstractFilter);


		/**
         * @brief GetAbstractFilter returns the abstract filter object
         */
        CMDPipelineArguments *GetCMDPipelineArguments() { return mCMDPipelineArguments; }


        /**
         * @brief SetAbstractFilter sets the abstract filter object
         */
        void SetCMDPipelineArguments(CMDPipelineArguments *cmdPipelineArguments) { mCMDPipelineArguments = cmdPipelineArguments; }


		/**
		 * @brief SetReaderPath sets the path for image and meta reader
		 */
		void SetReaderPath(QString path)	{ mPath = path; }


		/**
		 * @brief CreateOutputPath creates the output path
		 */
		static QString CreateOutputPath(CMDPipelineArguments*, AbstractFilter*);

    protected:
        ProcessObjectSettings* mProcessObjectSettings;  // data structure for the complete process object settings
        QString mName;                                  // the name of the process object
        QString mDescription;                           // the description of the process object
        ProcessObjectType *mObjectType;                 // information for template typecasting

        qint64 mStartTime;                              // start time point for processing time estimations
        bool mDirtyFlag;                                // flag to identify that an update of the output is necessary
        bool mIsLastFilter;                             // flag for the last filter in pipeline

        QList< ImageWrapper *> mInputImages;            // list of input images
        QList< ImageWrapper *> mOutputImages;           // list of output images

        QList< MetaDataFilter* > mMetaInputs;           // list of meta data inputs
        QList< MetaDataFilter* > mMetaOutputs;          // list of meta data outputs

        AbstractFilter* mAbstractFilter;                // xml definition of the filter
        CMDPipelineArguments *mCMDPipelineArguments;    // command line arguments for this pipeline run

		QString mPath;									// path for image and meta reader
};

} // namespace XPIWIT

#endif // PROCESSOBJECTBASE_H
