/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// file header
#include "ImagePointerManager.h"


namespace XPIWIT
{

ImagePointerManager::ImagePointerManager()
{
    SetAllToNull();
}

ImagePointerManager::~ImagePointerManager()
{
	SetAllToNull();
}

void ImagePointerManager::DeleteCurrentPointer()
{
	if (mImage3Float.IsNotNull())
		mImage3Float->ReleaseData();
}

void ImagePointerManager::SetAllToNull()
{
    mImageType.first = ProcessObjectType::DATATYPE_UNDEFINED;
    mImageType.second = 0;

    mImage2Char   = NULL;
    mImage2Short  = NULL;
    mImage2Int    = NULL;
    mImage2Long	  = NULL;
    mImage2UChar  = NULL;
	mImage2UShort = NULL;
    mImage2UInt   = NULL;
    mImage2ULong  = NULL;
    mImage2Float  = NULL;
    mImage2Double = NULL;

    mImage3Char   = NULL;
    mImage3Short  = NULL;
    mImage3Int    = NULL;
    mImage3Long	  = NULL;
    mImage3UChar  = NULL;
	mImage3UShort = NULL;
    mImage3UInt   = NULL;
    mImage3ULong  = NULL;	
	mImage3Float  = NULL;
    mImage3Double = NULL;
		
    mImage4Char   = NULL;
    mImage4Short  = NULL;
    mImage4Int    = NULL;
    mImage4Long   = NULL;
    mImage4UChar  = NULL;
	mImage4UShort = NULL;
    mImage4UInt   = NULL;
    mImage4ULong  = NULL;
    mImage4Float  = NULL;
    mImage4Double = NULL;
}

} // namespace XPIWIT
