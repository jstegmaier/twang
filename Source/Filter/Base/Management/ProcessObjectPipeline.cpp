/**
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images.
 * Copyright (C) 2013 J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle and R. Mikut
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * Please refer to the documentation for more information about the software
 * as well as for installation instructions.
 *
 * If you use this application for your scientific work, please cite the corresponding publication
 *
 * J. Stegmaier, J. C. Otte, A. Kobitski, A. Bartschat, A. Garcia, G. U. Nienhaus, U. Str�hle, R. Mikut,
 * Fast Segmentation of Stained Nuclei in Terabyte-Scale 3D+T Microscopy Images. 2013.
 *
 */

// file header
#include "ProcessObjectPipeline.h"

// namespace header
#include "ProcessObjectBase.h"
#include "ProcessObjectManager.h"
#include "ImageWrapper.h"
//#include "ProcessObjectList.h"

// project header
#include "../../../Core/XML/AbstractFilter.h"
#include "../../../Core/Utilities/Logger.h"

// itk header

// qt header
#include <QtCore/QString>
#include <QtCore/QFile>
#include <QtCore/QFileInfo>

// system header

namespace XPIWIT
{

// the constructor
ProcessObjectPipeline::ProcessObjectPipeline() 
{
}


// function to execute the specified abstract pipeline
void ProcessObjectPipeline::run( CMDPipelineArguments *cmdPipelineArguments, AbstractPipeline abstractPipeline )
{
	// get the logger instance
	Logger* logger = Logger::GetInstance();
	logger->SetUseSubfolder( cmdPipelineArguments->mUseSubFolder );

	// get the process object manager and the directories
	ProcessObjectManager* processObjectManager = ProcessObjectManager::GetInstance();
	QMap< QString, ProcessObjectBase* > processObjectMap;
	QString resultDir = cmdPipelineArguments->mStdOutputPath;
	QString defaultName = cmdPipelineArguments->mInputImagePaths.at( cmdPipelineArguments->mDefaultIndex );
	defaultName = defaultName.split( "/" ).last();

	// initialize the logger
	logger->InitLogFile( cmdPipelineArguments->mFileLoggingEnabled, resultDir, defaultName );

	// pipeline execution
	int numberOfFilter = abstractPipeline.GetNumFilter();

	// check if the last output already exists and quit the processing if true
	// TODO: replace with more elegant method, such as a method in the process objects directly
	QString lastOutputPath = ProcessObjectBase::CreateOutputPath( cmdPipelineArguments, abstractPipeline.GetFilter( numberOfFilter-1 ) );
	QFileInfo metaResultFile(lastOutputPath + QString("_KeyPoints.csv"));
	QFileInfo imageResultFile( lastOutputPath + QString("_Out1.tif") );
	if ((metaResultFile.exists() || imageResultFile.exists()) && cmdPipelineArguments->mSkipProcessingIfOutputExists == true)
	{
		Logger::GetInstance()->WriteToAll( "- Output file already exists, skipping processing. Disable --skipProcessingIfOutputExists to overwrite existing results." );
		return;
	}
	else
	{
		Logger::GetInstance()->WriteToAll( lastOutputPath + QString("_Out1.tif") +  QString(" does not exist yet.") );
	}

	// iterate over all filters and execute them
	for(int i=0; i<numberOfFilter; i++)
	{
		// get the abstract filter
		AbstractFilter* currentFilter = abstractPipeline.GetFilter( i );
		ProcessObjectBase *processObject = NULL;

		// handle readers separately as they need command line input
		if(currentFilter->IsReader() == true)
		{
			if(currentFilter->GetName().compare( "imagereader", Qt::CaseInsensitive ) == 0)
			{
				// ImageReader
				AbstractInput curInput = currentFilter->GetImageInputs().at( 0 );
				int id = curInput.mNumberRef;
				const QString type = "float";
				int index = cmdPipelineArguments->mInputImageId.indexOf( QString::number( id ) );
				QString path = cmdPipelineArguments->mInputImagePaths.at( index );
				int dimension = cmdPipelineArguments->mInputDimension.at( index ).toInt();

				processObject = processObjectManager->GetProcessObjectInstance( currentFilter->GetName(), QStringList() << type, QList< int >() << dimension );
				processObject->SetReaderPath( path );

				processObjectMap.insert( currentFilter->GetId(), processObject );
			}
			else
			{
				// MetaReader
				AbstractInput curInput = currentFilter->GetMetaInputs().at( 0 );
				int id = curInput.mNumberRef;
				int index = cmdPipelineArguments->mInputMetaId.indexOf( QString::number( id ) );
				QString path = cmdPipelineArguments->mInputMeta.at( index );

				processObject = processObjectManager->GetProcessObjectInstance( currentFilter->GetName(), QStringList() << "float", QList< int >() << 2 );
				processObject->SetReaderPath( path );

				processObjectMap.insert( currentFilter->GetId(), processObject );
			}
		}
		else
		{
			// Filter
			QStringList type;
			QList< int > dimension;

			int numberTypes = currentFilter->GetTypes().length();
			int numberInputs = currentFilter->GetImageInputs().length();

			for( int j = 0; j < numberTypes; j++ )
			{
				type << currentFilter->GetTypes().at( j );
			}

			for( int k = 0; k < numberInputs; k ++ )
			{
				// get the current dimension of the input
				ProcessObjectBase* curBase = processObjectMap.value( currentFilter->GetImageInputs().at( k ).mIdRef );
				if( curBase == NULL )
					throw QString("Error: Unknown reference id: " + currentFilter->GetImageInputs().at( k ).mIdRef + " in filter: " + currentFilter->GetId() );
					
				int outputNumber = currentFilter->GetImageInputs().at( k ).mNumberRef;
				if( outputNumber > curBase->GetType()->GetNumberImageOutputs() )
					throw QString("Error: Filter with id: " + curBase->GetAbstractFilter()->GetId() + " has no ouptut with number: " + QString::number( outputNumber ) );

				int dim = curBase->GetOutputImage( outputNumber - 1 )->GetImageDimension();
				dimension << dim;
			}

			processObject = ProcessObjectManager::GetInstance()->GetProcessObjectInstance( currentFilter->GetName(), QStringList() << type, QList< int >() << dimension );

			if( processObject == NULL )
				throw QString("Error: Filter can not be instantiated.");

			processObjectMap.insert( currentFilter->GetId(), processObject );

			// set inputs
			for( int j = 0; j < numberInputs; j++ )
			{
				ProcessObjectBase *curBase = processObjectMap.value( currentFilter->GetImageInputs().at( j ).mIdRef );
				int num = currentFilter->GetImageInputs().at( j ).mNumberRef - 1;
				ImageWrapper *curImageWrapper = curBase->GetOutputImage( num );
				curImageWrapper->decrementCounter();
				processObject->SetInputImage( curImageWrapper, j ); 
			}
		}

		// execute object
		processObject->SetAbstractFilter( currentFilter );
		processObject->SetCMDPipelineArguments( cmdPipelineArguments );

		if( i == (numberOfFilter-1))
			processObject->SetIsLast();

		try
		{
			processObject->Update();
		}
		catch(...)
		{
			throw QString("Error");
		}
	}

	// delete the intermediate filters
	QMap< QString, ProcessObjectBase* >::iterator mapIterator = processObjectMap.begin();
	for (mapIterator; mapIterator != processObjectMap.end(); ++mapIterator)
		mapIterator.value()->ReleaseOutputs();

	// write globale line
}

} // namespace XPIWIT
